<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($rawPathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($rawPathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    $ret = array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                    if (substr($pathinfo, -1) !== '/') {
                        return array_replace($ret, $this->redirect($rawPathinfo.'/', '_profiler_home'));
                    }

                    return $ret;
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        elseif (0 === strpos($pathinfo, '/category')) {
            // category_save
            if ('/category/save' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\CategoryCreateController::createAction',  '_route' => 'category_save',);
            }

            // category_list
            if ('/category/list' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\CategoryShowController::showAction',  '_route' => 'category_list',);
            }

            // category_update
            if ('/category/update' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\CategoryUpdateController::updateAction',  '_route' => 'category_update',);
            }

        }

        // homepage
        if ('' === $trimmedPathinfo) {
            $ret = array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
            if (substr($pathinfo, -1) !== '/') {
                return array_replace($ret, $this->redirect($rawPathinfo.'/', 'homepage'));
            }

            return $ret;
        }

        // login
        if ('/login' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\Login::login',  '_route' => 'login',);
        }

        // logout
        if ('/logout' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\LogoutController::indexAction',  '_route' => 'logout',);
        }

        if (0 === strpos($pathinfo, '/userlist')) {
            // app_login_userlist
            if ('/userlist' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\Login::userlistAction',  '_route' => 'app_login_userlist',);
            }

            // app_login_userlistpdf
            if ('/userlist_pdf' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\Login::userlistPDFAction',  '_route' => 'app_login_userlistpdf',);
            }

        }

        // user.edit
        if (0 === strpos($pathinfo, '/edituser') && preg_match('#^/edituser/(?P<id>[^/]++)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'user.edit')), array (  '_controller' => 'AppBundle\\Controller\\Login::editUserAction',));
        }

        if (0 === strpos($pathinfo, '/paymenttype')) {
            // payment_type_save
            if ('/paymenttype/save' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\PaymentTypeCreateController::createAction',  '_route' => 'payment_type_save',);
            }

            // payment_type_list
            if ('/paymenttype/list' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\PaymentTypeShowController::showAction',  '_route' => 'payment_type_list',);
            }

            // payment_type_update
            if ('/paymenttype/update' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\PaymentTypeUpdateController::updateAction',  '_route' => 'payment_type_update',);
            }

        }

        elseif (0 === strpos($pathinfo, '/se')) {
            if (0 === strpos($pathinfo, '/search')) {
                // search
                if ('/search' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\QRCodeScannerController::searchAction',  '_route' => 'search',);
                }

                // app_search_accountnumber
                if (preg_match('#^/search/(?P<accountnumber>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => 'app_search_accountnumber')), array (  '_controller' => 'AppBundle\\Controller\\SearchController::accountnumberAction',));
                }

            }

            elseif (0 === strpos($pathinfo, '/service')) {
                // service_save
                if ('/service/save' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\ServiceCreateController::createAction',  '_route' => 'service_save',);
                }

                if (0 === strpos($pathinfo, '/service_frequency')) {
                    // service_frequency_save
                    if ('/service_frequency/save' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\ServiceFrequencyCreateController::createAction',  '_route' => 'service_frequency_save',);
                    }

                    // service_frequency_list
                    if ('/service_frequency/list' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\ServiceFrequencyShowController::showAction',  '_route' => 'service_frequency_list',);
                    }

                    // service_frequency_update
                    if ('/service_frequency/update' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\ServiceFrequencyUpdateController::updateAction',  '_route' => 'service_frequency_update',);
                    }

                }

                elseif (0 === strpos($pathinfo, '/service_transaction')) {
                    // service_transaction_save
                    if ('/service_transaction/save' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\ServiceTransactionCreateController::createAction',  '_route' => 'service_transaction_save',);
                    }

                    // service_transaction_list
                    if ('/service_transaction/list' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\ServiceTransactionShowController::showAction',  '_route' => 'service_transaction_list',);
                    }

                    // service_transaction_update
                    if ('/service_transaction/update' === $pathinfo) {
                        return array (  '_controller' => 'AppBundle\\Controller\\ServiceTransactionUpdateController::updateAction',  '_route' => 'service_transaction_update',);
                    }

                }

                // service_list
                if ('/service/list' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\ServiceShowController::showAction',  '_route' => 'service_list',);
                }

                // service_update
                if ('/service/update' === $pathinfo) {
                    return array (  '_controller' => 'AppBundle\\Controller\\ServiceUpdateController::updateAction',  '_route' => 'service_update',);
                }

            }

            // send_mail
            if ('/send_mail' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\ServiceTransactionShowController::sendMail',  '_route' => 'send_mail',);
            }

        }

        elseif (0 === strpos($pathinfo, '/translation_map')) {
            // translation_map_save
            if ('/translation_map/save' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\TranslationMapCreateController::createAction',  '_route' => 'translation_map_save',);
            }

            // translation_map_list
            if ('/translation_map/list' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\TranslationMapShowController::showAction',  '_route' => 'translation_map_list',);
            }

            // translation_map_update
            if ('/translation_map/update' === $pathinfo) {
                return array (  '_controller' => 'AppBundle\\Controller\\TranslationMapUpdateController::updateAction',  '_route' => 'translation_map_update',);
            }

        }

        // user_registration
        if ('/register' === $pathinfo) {
            return array (  '_controller' => 'AppBundle\\Controller\\UserRegisterController::registerAction',  '_route' => 'user_registration',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}

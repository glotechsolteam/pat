<?php

/* service_transaction/list.html.twig */
class __TwigTemplate_e2ac6e94bc4eb22a5a691a8d6ea6a51fe1ba4fd6671e9c1226f99476bb975e17 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "service_transaction/list.html.twig", 1);
        $this->blocks = array(
            'service_transaction_list' => array($this, 'block_service_transaction_list'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c1fcf7afc344e2693e8f3c77b8dad449910f65d8be944a2c82a86ac8be3a8455 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c1fcf7afc344e2693e8f3c77b8dad449910f65d8be944a2c82a86ac8be3a8455->enter($__internal_c1fcf7afc344e2693e8f3c77b8dad449910f65d8be944a2c82a86ac8be3a8455_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "service_transaction/list.html.twig"));

        $__internal_8eaf9a52e241dc2d18b77797f45ad94eba7d386419e1d12cf80bca784c4bad6b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8eaf9a52e241dc2d18b77797f45ad94eba7d386419e1d12cf80bca784c4bad6b->enter($__internal_8eaf9a52e241dc2d18b77797f45ad94eba7d386419e1d12cf80bca784c4bad6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "service_transaction/list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c1fcf7afc344e2693e8f3c77b8dad449910f65d8be944a2c82a86ac8be3a8455->leave($__internal_c1fcf7afc344e2693e8f3c77b8dad449910f65d8be944a2c82a86ac8be3a8455_prof);

        
        $__internal_8eaf9a52e241dc2d18b77797f45ad94eba7d386419e1d12cf80bca784c4bad6b->leave($__internal_8eaf9a52e241dc2d18b77797f45ad94eba7d386419e1d12cf80bca784c4bad6b_prof);

    }

    // line 3
    public function block_service_transaction_list($context, array $blocks = array())
    {
        $__internal_2ef7d1038e271d335035ceb313462080c804e7103593b4e4646c6e3331f68635 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ef7d1038e271d335035ceb313462080c804e7103593b4e4646c6e3331f68635->enter($__internal_2ef7d1038e271d335035ceb313462080c804e7103593b4e4646c6e3331f68635_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_transaction_list"));

        $__internal_e4d901fe8c77f93c53b0d3f4cf3459b823021dac454e8d9d5eb85ed6471371e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e4d901fe8c77f93c53b0d3f4cf3459b823021dac454e8d9d5eb85ed6471371e1->enter($__internal_e4d901fe8c77f93c53b0d3f4cf3459b823021dac454e8d9d5eb85ed6471371e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_transaction_list"));

        // line 4
        echo "    <style>
        .paginator {
            margin-left: 10px;
        }
    </style>
    <script type=\"text/javascript\" src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/js/delete.js"), "html", null, true);
        echo "\"></script>
    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" rel=\"stylesheet\"/>
    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css\" rel=\"stylesheet\"/>
    <div class=\"row\">
        <div class=\"col-md-12\">
            ";
        // line 14
        echo twig_include($this->env, $context, "progress_bar.html.twig");
        echo "
            <h1>Service Transaction</h1>
            ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "flashes", array(0 => "notice"), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
            // line 17
            echo "                <div class=\"alert alert-warning\">
                    <strong>Warning!</strong>  ";
            // line 18
            echo twig_escape_filter($this->env, $context["message"], "html", null, true);
            echo "
                </div>

            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 22
        echo "            ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
            <table class=\"table\">
                <tr>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Service</th>
                    <th>Total Amount</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "dateStart", array()), 'widget');
        echo "
                    <td>";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "dateEnd", array()), 'widget');
        echo "
                    <td>";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "service", array()), 'widget');
        echo "
                    <td>";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute(($context["total_service_transaction"] ?? $this->getContext($context, "total_service_transaction")), "totalAmount", array()), "html", null, true);
        echo "
                    <td>";
        // line 36
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-success")));
        echo "
                </tr>
            </table>
            ";
        // line 39
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
            <table class=\"table table-striped table-hover\">
                <thead>
                    <tr>
                        <th>ID</th>
                        <!-- Pginaror Sortable Column-->
                        <th>";
        // line 45
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->sortable($this->env, ($context["service_transaction"] ?? $this->getContext($context, "service_transaction")), "Account Number", "accountNumber");
        echo "</th>
                        <th>";
        // line 46
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->sortable($this->env, ($context["service_transaction"] ?? $this->getContext($context, "service_transaction")), "Payment Type", "paymentType.name");
        echo "</th>
                        <th>";
        // line 47
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->sortable($this->env, ($context["service_transaction"] ?? $this->getContext($context, "service_transaction")), "User", "user.username");
        echo "</th>
                        <th>";
        // line 48
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->sortable($this->env, ($context["service_transaction"] ?? $this->getContext($context, "service_transaction")), "Service", "service.name");
        echo "</th>
                        <th>Paid</th>
                        <th>Status</th>
                        <th>";
        // line 51
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->sortable($this->env, ($context["service_transaction"] ?? $this->getContext($context, "service_transaction")), "Scan Count", "scanCount");
        echo "</th>
                        <th>";
        // line 52
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->sortable($this->env, ($context["service_transaction"] ?? $this->getContext($context, "service_transaction")), "Paid Date", "paidDate");
        echo "</th>
                    </tr>
                </thead>
                <tbody>
                    ";
        // line 56
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["service_transaction"] ?? $this->getContext($context, "service_transaction")));
        foreach ($context['_seq'] as $context["_key"] => $context["st"]) {
            // line 57
            echo "                        <tr data-id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["st"], "id", array()), "html", null, true);
            echo "\">
                            <td> ";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["st"], "id", array()), "html", null, true);
            echo " </td>
                            <td> ";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["st"], "accountNumber", array()), "html", null, true);
            echo " </td>
                            <td> ";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["st"], "paymentType", array()), "name", array()), "html", null, true);
            echo " | ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["st"], "paymentType", array()), "publicKey", array()), "html", null, true);
            echo " | ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["st"], "paymentType", array()), "publicKeyTwo", array()), "html", null, true);
            echo " </td>
                            <td> ";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["st"], "user", array()), "username", array()), "html", null, true);
            echo " </td>
                            <td> ";
            // line 62
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["st"], "service", array()), "name", array()), "html", null, true);
            echo "| ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["st"], "service", array()), "serviceFrequency", array()), "name", array()), "html", null, true);
            echo " </td>
                            <td> ";
            // line 63
            echo twig_escape_filter($this->env, $this->getAttribute($context["st"], "paid", array()), "html", null, true);
            echo " </td>
                            <td> ";
            // line 64
            echo twig_escape_filter($this->env, $this->getAttribute($context["st"], "status", array()), "html", null, true);
            echo " </td>
                            <td> ";
            // line 65
            echo twig_escape_filter($this->env, $this->getAttribute($context["st"], "scanCount", array()), "html", null, true);
            echo " </td>
                            <td> ";
            // line 66
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["st"], "paidDate", array()), "Y/m/d"), "html", null, true);
            echo " </td>
                            <td>
                                <a href=\"/service_transaction/update?id=";
            // line 68
            echo twig_escape_filter($this->env, $this->getAttribute($context["st"], "id", array()), "html", null, true);
            echo "\" class=\"btn btn-defualt btn-sm\">Edit</a>
                                ";
            // line 69
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["delete_form_ajax"] ?? $this->getContext($context, "delete_form_ajax")), 'form_start', array("attr" => array("id" => "form-delete")));
            echo "
                                ";
            // line 70
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["delete_form_ajax"] ?? $this->getContext($context, "delete_form_ajax")), 'widget');
            echo "
                                <input type=\"submit\" class=\"btn btn-danger btn-sm\" value=\"Delete\" >
                                <a href=\"/send_mail?user_id=";
            // line 72
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["st"], "user", array()), "id", array()), "html", null, true);
            echo "&account_number=";
            echo twig_escape_filter($this->env, $this->getAttribute($context["st"], "accountNumber", array()), "html", null, true);
            echo "&service_id=";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($context["st"], "service", array()), "id", array()), "html", null, true);
            echo "\" class=\"btn btn-info\">Send Mail</a>
                                ";
            // line 73
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["delete_form_ajax"] ?? $this->getContext($context, "delete_form_ajax")), 'form_end');
            echo "
                            </td>
                        </tr>
                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['st'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 77
        echo "                </tbody>
            </table>
            <div class=\"paginator\">
                ";
        // line 80
        echo $this->env->getExtension('Knp\Bundle\PaginatorBundle\Twig\Extension\PaginationExtension')->render($this->env, ($context["service_transaction"] ?? $this->getContext($context, "service_transaction")));
        echo "
            </div>
        </div>
    </div>
";
        
        $__internal_e4d901fe8c77f93c53b0d3f4cf3459b823021dac454e8d9d5eb85ed6471371e1->leave($__internal_e4d901fe8c77f93c53b0d3f4cf3459b823021dac454e8d9d5eb85ed6471371e1_prof);

        
        $__internal_2ef7d1038e271d335035ceb313462080c804e7103593b4e4646c6e3331f68635->leave($__internal_2ef7d1038e271d335035ceb313462080c804e7103593b4e4646c6e3331f68635_prof);

    }

    public function getTemplateName()
    {
        return "service_transaction/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  248 => 80,  243 => 77,  233 => 73,  225 => 72,  220 => 70,  216 => 69,  212 => 68,  207 => 66,  203 => 65,  199 => 64,  195 => 63,  189 => 62,  185 => 61,  177 => 60,  173 => 59,  169 => 58,  164 => 57,  160 => 56,  153 => 52,  149 => 51,  143 => 48,  139 => 47,  135 => 46,  131 => 45,  122 => 39,  116 => 36,  112 => 35,  108 => 34,  104 => 33,  100 => 32,  86 => 22,  76 => 18,  73 => 17,  69 => 16,  64 => 14,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block service_transaction_list %}
    <style>
        .paginator {
            margin-left: 10px;
        }
    </style>
    <script type=\"text/javascript\" src=\"{{ asset('bundles/js/delete.js') }}\"></script>
    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" rel=\"stylesheet\"/>
    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css\" rel=\"stylesheet\"/>
    <div class=\"row\">
        <div class=\"col-md-12\">
            {{ include('progress_bar.html.twig') }}
            <h1>Service Transaction</h1>
            {% for message in app.flashes('notice') %}
                <div class=\"alert alert-warning\">
                    <strong>Warning!</strong>  {{ message }}
                </div>

            {% endfor %}
            {{ form_start(form) }}
            <table class=\"table\">
                <tr>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Service</th>
                    <th>Total Amount</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>{{ form_widget(form.dateStart) }}
                    <td>{{ form_widget(form.dateEnd) }}
                    <td>{{ form_widget(form.service) }}
                    <td>{{total_service_transaction.totalAmount}}
                    <td>{{ form_widget(form.submit, { 'attr' : { 'class': 'btn btn-success' }}) }}
                </tr>
            </table>
            {{ form_end(form) }}
            <table class=\"table table-striped table-hover\">
                <thead>
                    <tr>
                        <th>ID</th>
                        <!-- Pginaror Sortable Column-->
                        <th>{{ knp_pagination_sortable(service_transaction, 'Account Number' , 'accountNumber') }}</th>
                        <th>{{ knp_pagination_sortable(service_transaction, 'Payment Type' , 'paymentType.name') }}</th>
                        <th>{{ knp_pagination_sortable(service_transaction, 'User' , 'user.username') }}</th>
                        <th>{{ knp_pagination_sortable(service_transaction, 'Service' , 'service.name') }}</th>
                        <th>Paid</th>
                        <th>Status</th>
                        <th>{{ knp_pagination_sortable(service_transaction, 'Scan Count' , 'scanCount') }}</th>
                        <th>{{ knp_pagination_sortable(service_transaction, 'Paid Date' , 'paidDate') }}</th>
                    </tr>
                </thead>
                <tbody>
                    {% for st in service_transaction %}
                        <tr data-id=\"{{ st.id  }}\">
                            <td> {{ st.id }} </td>
                            <td> {{ st.accountNumber }} </td>
                            <td> {{ st.paymentType.name}} | {{st.paymentType.publicKey }} | {{st.paymentType.publicKeyTwo }} </td>
                            <td> {{ st.user.username }} </td>
                            <td> {{ st.service.name }}| {{st.service.serviceFrequency.name}} </td>
                            <td> {{ st.paid }} </td>
                            <td> {{ st.status }} </td>
                            <td> {{ st.scanCount }} </td>
                            <td> {{ st.paidDate|date(\"Y/m/d\") }} </td>
                            <td>
                                <a href=\"/service_transaction/update?id={{ st.id }}\" class=\"btn btn-defualt btn-sm\">Edit</a>
                                {{ form_start(delete_form_ajax, { 'attr' : { 'id': 'form-delete' }}) }}
                                {{ form_widget(delete_form_ajax) }}
                                <input type=\"submit\" class=\"btn btn-danger btn-sm\" value=\"Delete\" >
                                <a href=\"/send_mail?user_id={{ st.user.id }}&account_number={{st.accountNumber}}&service_id={{ st.service.id }}\" class=\"btn btn-info\">Send Mail</a>
                                {{ form_end(delete_form_ajax) }}
                            </td>
                        </tr>
                    {% endfor %}
                </tbody>
            </table>
            <div class=\"paginator\">
                {{ knp_pagination_render(service_transaction) }}
            </div>
        </div>
    </div>
{% endblock %}

", "service_transaction/list.html.twig", "/var/www/pat/app/Resources/views/service_transaction/list.html.twig");
    }
}

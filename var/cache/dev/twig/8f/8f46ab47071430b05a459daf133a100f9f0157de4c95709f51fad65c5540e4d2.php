<?php

/* default/index.html.twig */
class __TwigTemplate_a5fad314a7fb808d9ed54e066b88d49cf589699d4ffd2fa81573c51faf4f64ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_41673a8b694f71a200ccee64f49ab6a5d43ec2d74bef0794b35cf8ccc08fb98b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_41673a8b694f71a200ccee64f49ab6a5d43ec2d74bef0794b35cf8ccc08fb98b->enter($__internal_41673a8b694f71a200ccee64f49ab6a5d43ec2d74bef0794b35cf8ccc08fb98b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $__internal_58cfc2466f4ace407c45ef2308f00c96bc0ed43057bb6748d68f78c701dfb44c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58cfc2466f4ace407c45ef2308f00c96bc0ed43057bb6748d68f78c701dfb44c->enter($__internal_58cfc2466f4ace407c45ef2308f00c96bc0ed43057bb6748d68f78c701dfb44c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_41673a8b694f71a200ccee64f49ab6a5d43ec2d74bef0794b35cf8ccc08fb98b->leave($__internal_41673a8b694f71a200ccee64f49ab6a5d43ec2d74bef0794b35cf8ccc08fb98b_prof);

        
        $__internal_58cfc2466f4ace407c45ef2308f00c96bc0ed43057bb6748d68f78c701dfb44c->leave($__internal_58cfc2466f4ace407c45ef2308f00c96bc0ed43057bb6748d68f78c701dfb44c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_094afcff18d7bc271fa8690da37f37eba4a7a62e9e1b03f5ea3837455b3d1dcd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_094afcff18d7bc271fa8690da37f37eba4a7a62e9e1b03f5ea3837455b3d1dcd->enter($__internal_094afcff18d7bc271fa8690da37f37eba4a7a62e9e1b03f5ea3837455b3d1dcd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_620085e91da5d1d2209565fb5298fc90b64547a05676ea1d30680c66fe5fde00 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_620085e91da5d1d2209565fb5298fc90b64547a05676ea1d30680c66fe5fde00->enter($__internal_620085e91da5d1d2209565fb5298fc90b64547a05676ea1d30680c66fe5fde00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
        <div id=\"container\">
            <div id=\"welcome\">
                <h1><span>Welcome to GTS Payment System </span></h1>
            </div>

            <div id=\"status\">
                <p>
                    <svg id=\"icon-status\" width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z\" fill=\"#759E1A\"/></svg>

                    Your application is now ready. You can start working on it at:
                    <code>";
        // line 15
        echo twig_escape_filter($this->env, ($context["base_dir"] ?? $this->getContext($context, "base_dir")), "html", null, true);
        echo "/</code>
                </p>
            </div>

            <div id=\"next\">

            </div>

        </div>

";
        
        $__internal_620085e91da5d1d2209565fb5298fc90b64547a05676ea1d30680c66fe5fde00->leave($__internal_620085e91da5d1d2209565fb5298fc90b64547a05676ea1d30680c66fe5fde00_prof);

        
        $__internal_094afcff18d7bc271fa8690da37f37eba4a7a62e9e1b03f5ea3837455b3d1dcd->leave($__internal_094afcff18d7bc271fa8690da37f37eba4a7a62e9e1b03f5ea3837455b3d1dcd_prof);

    }

    // line 27
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_397b20c82c92e0beb59c517d4e5bd1cb48ab2d3949d202fd42878161e2c72218 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_397b20c82c92e0beb59c517d4e5bd1cb48ab2d3949d202fd42878161e2c72218->enter($__internal_397b20c82c92e0beb59c517d4e5bd1cb48ab2d3949d202fd42878161e2c72218_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_06987e0e48d5e2ea9ea6b1722c28a57dc8c15a0a9a57b70d347861173ac2da8e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_06987e0e48d5e2ea9ea6b1722c28a57dc8c15a0a9a57b70d347861173ac2da8e->enter($__internal_06987e0e48d5e2ea9ea6b1722c28a57dc8c15a0a9a57b70d347861173ac2da8e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 28
        echo "<style>
    body { background: #F5F5F5; font: 18px/1.5 sans-serif; }
    h1, h2 { line-height: 1.2; margin: 0 0 .5em; }
    h1 { font-size: 36px; }
    h2 { font-size: 21px; margin-bottom: 1em; }
    p { margin: 0 0 1em 0; }
    a { color: #0000F0; }
    a:hover { text-decoration: none; }
    code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }
    #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }
    #container { padding: 2em; }
    #welcome, #status { margin-bottom: 2em; }
    #welcome h1 span { display: block; font-size: 75%; }
    #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }
    #icon-book { display: none; }

    @media (min-width: 768px) {
        #wrapper { width: 80%; margin: 2em auto; }
        #icon-book { display: inline-block; }
        #status a, #next a { display: block; }

        @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}
    }
</style>
";
        
        $__internal_06987e0e48d5e2ea9ea6b1722c28a57dc8c15a0a9a57b70d347861173ac2da8e->leave($__internal_06987e0e48d5e2ea9ea6b1722c28a57dc8c15a0a9a57b70d347861173ac2da8e_prof);

        
        $__internal_397b20c82c92e0beb59c517d4e5bd1cb48ab2d3949d202fd42878161e2c72218->leave($__internal_397b20c82c92e0beb59c517d4e5bd1cb48ab2d3949d202fd42878161e2c72218_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 28,  84 => 27,  63 => 15,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}

        <div id=\"container\">
            <div id=\"welcome\">
                <h1><span>Welcome to GTS Payment System </span></h1>
            </div>

            <div id=\"status\">
                <p>
                    <svg id=\"icon-status\" width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1671 566q0 40-28 68l-724 724-136 136q-28 28-68 28t-68-28l-136-136-362-362q-28-28-28-68t28-68l136-136q28-28 68-28t68 28l294 295 656-657q28-28 68-28t68 28l136 136q28 28 28 68z\" fill=\"#759E1A\"/></svg>

                    Your application is now ready. You can start working on it at:
                    <code>{{ base_dir }}/</code>
                </p>
            </div>

            <div id=\"next\">

            </div>

        </div>

{% endblock %}

{% block stylesheets %}
<style>
    body { background: #F5F5F5; font: 18px/1.5 sans-serif; }
    h1, h2 { line-height: 1.2; margin: 0 0 .5em; }
    h1 { font-size: 36px; }
    h2 { font-size: 21px; margin-bottom: 1em; }
    p { margin: 0 0 1em 0; }
    a { color: #0000F0; }
    a:hover { text-decoration: none; }
    code { background: #F5F5F5; max-width: 100px; padding: 2px 6px; word-wrap: break-word; }
    #wrapper { background: #FFF; margin: 1em auto; max-width: 800px; width: 95%; }
    #container { padding: 2em; }
    #welcome, #status { margin-bottom: 2em; }
    #welcome h1 span { display: block; font-size: 75%; }
    #icon-status, #icon-book { float: left; height: 64px; margin-right: 1em; margin-top: -4px; width: 64px; }
    #icon-book { display: none; }

    @media (min-width: 768px) {
        #wrapper { width: 80%; margin: 2em auto; }
        #icon-book { display: inline-block; }
        #status a, #next a { display: block; }

        @-webkit-keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        @keyframes fade-in { 0% { opacity: 0; } 100% { opacity: 1; } }
        .sf-toolbar { opacity: 0; -webkit-animation: fade-in 1s .2s forwards; animation: fade-in 1s .2s forwards;}
    }
</style>
{% endblock %}
", "default/index.html.twig", "/var/www/pat/app/Resources/views/default/index.html.twig");
    }
}

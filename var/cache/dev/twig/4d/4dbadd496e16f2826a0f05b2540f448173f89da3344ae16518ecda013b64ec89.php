<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_32ccaa003778514a3f8e9996b936430f83d00b6618f1941ba0dfde76b0fd1cd9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_019da6b562cd009d90122b0aec7ee6a6d3c08828d3e4af88dc1da2a2403de637 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_019da6b562cd009d90122b0aec7ee6a6d3c08828d3e4af88dc1da2a2403de637->enter($__internal_019da6b562cd009d90122b0aec7ee6a6d3c08828d3e4af88dc1da2a2403de637_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_bbfa18684ef7148d7feedc1fbc9b4f919797d09e2aad35275b33933b5d230c22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bbfa18684ef7148d7feedc1fbc9b4f919797d09e2aad35275b33933b5d230c22->enter($__internal_bbfa18684ef7148d7feedc1fbc9b4f919797d09e2aad35275b33933b5d230c22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_019da6b562cd009d90122b0aec7ee6a6d3c08828d3e4af88dc1da2a2403de637->leave($__internal_019da6b562cd009d90122b0aec7ee6a6d3c08828d3e4af88dc1da2a2403de637_prof);

        
        $__internal_bbfa18684ef7148d7feedc1fbc9b4f919797d09e2aad35275b33933b5d230c22->leave($__internal_bbfa18684ef7148d7feedc1fbc9b4f919797d09e2aad35275b33933b5d230c22_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_7c2e3b1405018fe72232b76544535a921a58a3e355615aff69f6b6681f5a0f5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c2e3b1405018fe72232b76544535a921a58a3e355615aff69f6b6681f5a0f5a->enter($__internal_7c2e3b1405018fe72232b76544535a921a58a3e355615aff69f6b6681f5a0f5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_92e0ee31e1834df9b43263f268ff7be0cd5cb4405eb08f200a8c71e97f93b17f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_92e0ee31e1834df9b43263f268ff7be0cd5cb4405eb08f200a8c71e97f93b17f->enter($__internal_92e0ee31e1834df9b43263f268ff7be0cd5cb4405eb08f200a8c71e97f93b17f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_92e0ee31e1834df9b43263f268ff7be0cd5cb4405eb08f200a8c71e97f93b17f->leave($__internal_92e0ee31e1834df9b43263f268ff7be0cd5cb4405eb08f200a8c71e97f93b17f_prof);

        
        $__internal_7c2e3b1405018fe72232b76544535a921a58a3e355615aff69f6b6681f5a0f5a->leave($__internal_7c2e3b1405018fe72232b76544535a921a58a3e355615aff69f6b6681f5a0f5a_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_5a2f722532c9b2bc033eafbfcae7217d1d7fae0c35ca58b670ec4081824c12b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a2f722532c9b2bc033eafbfcae7217d1d7fae0c35ca58b670ec4081824c12b2->enter($__internal_5a2f722532c9b2bc033eafbfcae7217d1d7fae0c35ca58b670ec4081824c12b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_78487fadc0734f45f0fc8c642be740aa31295be88c6ebd20be23841f1fd51923 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_78487fadc0734f45f0fc8c642be740aa31295be88c6ebd20be23841f1fd51923->enter($__internal_78487fadc0734f45f0fc8c642be740aa31295be88c6ebd20be23841f1fd51923_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_78487fadc0734f45f0fc8c642be740aa31295be88c6ebd20be23841f1fd51923->leave($__internal_78487fadc0734f45f0fc8c642be740aa31295be88c6ebd20be23841f1fd51923_prof);

        
        $__internal_5a2f722532c9b2bc033eafbfcae7217d1d7fae0c35ca58b670ec4081824c12b2->leave($__internal_5a2f722532c9b2bc033eafbfcae7217d1d7fae0c35ca58b670ec4081824c12b2_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_9b9dc460981c65bc5b4fd9debbb3841d9aaa9709df26a7f01452bb5bd8591f48 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b9dc460981c65bc5b4fd9debbb3841d9aaa9709df26a7f01452bb5bd8591f48->enter($__internal_9b9dc460981c65bc5b4fd9debbb3841d9aaa9709df26a7f01452bb5bd8591f48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_b54a0a95feabedebedfa67d271aeac86eb26e5f5a265afcce63b42a7bd1699ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b54a0a95feabedebedfa67d271aeac86eb26e5f5a265afcce63b42a7bd1699ef->enter($__internal_b54a0a95feabedebedfa67d271aeac86eb26e5f5a265afcce63b42a7bd1699ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_b54a0a95feabedebedfa67d271aeac86eb26e5f5a265afcce63b42a7bd1699ef->leave($__internal_b54a0a95feabedebedfa67d271aeac86eb26e5f5a265afcce63b42a7bd1699ef_prof);

        
        $__internal_9b9dc460981c65bc5b4fd9debbb3841d9aaa9709df26a7f01452bb5bd8591f48->leave($__internal_9b9dc460981c65bc5b4fd9debbb3841d9aaa9709df26a7f01452bb5bd8591f48_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/var/www/pat/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}

<?php

/* @KnpPaginator/Pagination/sortable_link.html.twig */
class __TwigTemplate_9ce996dbecc0015aa7c4dd71e8f85d469d344dfdc0880d04a5136157ac3a6d72 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3be61dbf32f5d055d6cf553b3ca2291639ed6d1e92d107741839bee2832a0cbe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3be61dbf32f5d055d6cf553b3ca2291639ed6d1e92d107741839bee2832a0cbe->enter($__internal_3be61dbf32f5d055d6cf553b3ca2291639ed6d1e92d107741839bee2832a0cbe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@KnpPaginator/Pagination/sortable_link.html.twig"));

        $__internal_b50c887c67d6632ccb71c154931da39b824192b54c4f222bbdb9a055077bbf60 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b50c887c67d6632ccb71c154931da39b824192b54c4f222bbdb9a055077bbf60->enter($__internal_b50c887c67d6632ccb71c154931da39b824192b54c4f222bbdb9a055077bbf60_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@KnpPaginator/Pagination/sortable_link.html.twig"));

        // line 1
        echo "<a";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["options"] ?? $this->getContext($context, "options")));
        foreach ($context['_seq'] as $context["attr"] => $context["value"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attr"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attr'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">";
        echo twig_escape_filter($this->env, ($context["title"] ?? $this->getContext($context, "title")), "html", null, true);
        echo "</a>
";
        
        $__internal_3be61dbf32f5d055d6cf553b3ca2291639ed6d1e92d107741839bee2832a0cbe->leave($__internal_3be61dbf32f5d055d6cf553b3ca2291639ed6d1e92d107741839bee2832a0cbe_prof);

        
        $__internal_b50c887c67d6632ccb71c154931da39b824192b54c4f222bbdb9a055077bbf60->leave($__internal_b50c887c67d6632ccb71c154931da39b824192b54c4f222bbdb9a055077bbf60_prof);

    }

    public function getTemplateName()
    {
        return "@KnpPaginator/Pagination/sortable_link.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<a{% for attr, value in options %} {{ attr }}=\"{{ value }}\"{% endfor %}>{{ title }}</a>
", "@KnpPaginator/Pagination/sortable_link.html.twig", "/var/www/pat/vendor/knplabs/knp-paginator-bundle/Resources/views/Pagination/sortable_link.html.twig");
    }
}

<?php

/* @WebProfiler/Profiler/open.html.twig */
class __TwigTemplate_0379b6e760b05a212edd50a6ba2583504a11033198e3c9d67d259e0b0d018c07 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "@WebProfiler/Profiler/open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e2dee32b7805078b1c8e9edeebd6fdfccd3d56da5b00c04befbf14649c376174 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e2dee32b7805078b1c8e9edeebd6fdfccd3d56da5b00c04befbf14649c376174->enter($__internal_e2dee32b7805078b1c8e9edeebd6fdfccd3d56da5b00c04befbf14649c376174_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $__internal_b244021c93b955ab3bba94abcfc867524a77bbaf81fc1543bace5f00a3a4b855 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b244021c93b955ab3bba94abcfc867524a77bbaf81fc1543bace5f00a3a4b855->enter($__internal_b244021c93b955ab3bba94abcfc867524a77bbaf81fc1543bace5f00a3a4b855_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e2dee32b7805078b1c8e9edeebd6fdfccd3d56da5b00c04befbf14649c376174->leave($__internal_e2dee32b7805078b1c8e9edeebd6fdfccd3d56da5b00c04befbf14649c376174_prof);

        
        $__internal_b244021c93b955ab3bba94abcfc867524a77bbaf81fc1543bace5f00a3a4b855->leave($__internal_b244021c93b955ab3bba94abcfc867524a77bbaf81fc1543bace5f00a3a4b855_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_1b76595f9acb62eba756a6299bcd37b7417389474478ad59e4db28db84a5a0c9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1b76595f9acb62eba756a6299bcd37b7417389474478ad59e4db28db84a5a0c9->enter($__internal_1b76595f9acb62eba756a6299bcd37b7417389474478ad59e4db28db84a5a0c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_c4a0375e8a544d425d7524a906a7152eca09a322553a5d3fc1998876ff14248d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4a0375e8a544d425d7524a906a7152eca09a322553a5d3fc1998876ff14248d->enter($__internal_c4a0375e8a544d425d7524a906a7152eca09a322553a5d3fc1998876ff14248d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_c4a0375e8a544d425d7524a906a7152eca09a322553a5d3fc1998876ff14248d->leave($__internal_c4a0375e8a544d425d7524a906a7152eca09a322553a5d3fc1998876ff14248d_prof);

        
        $__internal_1b76595f9acb62eba756a6299bcd37b7417389474478ad59e4db28db84a5a0c9->leave($__internal_1b76595f9acb62eba756a6299bcd37b7417389474478ad59e4db28db84a5a0c9_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_e6b6dd0c963798bcb570e9975b5135f52ddb7c9027454c65aa1e882caccb4b51 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6b6dd0c963798bcb570e9975b5135f52ddb7c9027454c65aa1e882caccb4b51->enter($__internal_e6b6dd0c963798bcb570e9975b5135f52ddb7c9027454c65aa1e882caccb4b51_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_06ce9f9f17c9bfc1ac2db166a8b3876f93d51b5e789a45ed48d47371df454eb0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_06ce9f9f17c9bfc1ac2db166a8b3876f93d51b5e789a45ed48d47371df454eb0->enter($__internal_06ce9f9f17c9bfc1ac2db166a8b3876f93d51b5e789a45ed48d47371df454eb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, ($context["file"] ?? $this->getContext($context, "file")), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, ($context["line"] ?? $this->getContext($context, "line")), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt(($context["filename"] ?? $this->getContext($context, "filename")), ($context["line"] ?? $this->getContext($context, "line")),  -1);
        echo "
</div>
";
        
        $__internal_06ce9f9f17c9bfc1ac2db166a8b3876f93d51b5e789a45ed48d47371df454eb0->leave($__internal_06ce9f9f17c9bfc1ac2db166a8b3876f93d51b5e789a45ed48d47371df454eb0_prof);

        
        $__internal_e6b6dd0c963798bcb570e9975b5135f52ddb7c9027454c65aa1e882caccb4b51->leave($__internal_e6b6dd0c963798bcb570e9975b5135f52ddb7c9027454c65aa1e882caccb4b51_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "@WebProfiler/Profiler/open.html.twig", "/var/www/pat/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}

<?php

/* login/userlist.html.twig */
class __TwigTemplate_52784f08dc79834823ffb6fcf8a9274e1b976b96f72f7bdf05037ff23285f8ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "login/userlist.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_143078df4304a8b31f741d434c213339b2816ae6704668e277640d84832e7e97 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_143078df4304a8b31f741d434c213339b2816ae6704668e277640d84832e7e97->enter($__internal_143078df4304a8b31f741d434c213339b2816ae6704668e277640d84832e7e97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "login/userlist.html.twig"));

        $__internal_2808dc5f19545f8bc8852224491e2b513565adadb27e97d46545f523dc7bc408 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2808dc5f19545f8bc8852224491e2b513565adadb27e97d46545f523dc7bc408->enter($__internal_2808dc5f19545f8bc8852224491e2b513565adadb27e97d46545f523dc7bc408_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "login/userlist.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_143078df4304a8b31f741d434c213339b2816ae6704668e277640d84832e7e97->leave($__internal_143078df4304a8b31f741d434c213339b2816ae6704668e277640d84832e7e97_prof);

        
        $__internal_2808dc5f19545f8bc8852224491e2b513565adadb27e97d46545f523dc7bc408->leave($__internal_2808dc5f19545f8bc8852224491e2b513565adadb27e97d46545f523dc7bc408_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_9601e014af5a77f643a876663ea9ad806af6aed9cf4a8fa4dd7c28d0f2f8a878 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9601e014af5a77f643a876663ea9ad806af6aed9cf4a8fa4dd7c28d0f2f8a878->enter($__internal_9601e014af5a77f643a876663ea9ad806af6aed9cf4a8fa4dd7c28d0f2f8a878_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_29a72b6d1c1346ba1512eadefc1b23fc4cfab5aa804aeb7d21809dad67d0e690 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29a72b6d1c1346ba1512eadefc1b23fc4cfab5aa804aeb7d21809dad67d0e690->enter($__internal_29a72b6d1c1346ba1512eadefc1b23fc4cfab5aa804aeb7d21809dad67d0e690_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    <link href=\"https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css\" rel=\"stylesheet\">
    <script src=\"https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js\"></script>
    <style>
.paginator {
    margin-left: 10px;
}
</style>
<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1>All Users</h1>
            <table class=\"table table-hover table-striped\">
                <tr>
                    <th>Username</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Image</th>
                    <th>Active</th>
                </tr>

                ";
        // line 26
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["users"] ?? $this->getContext($context, "users")));
        foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
            // line 27
            echo "                <tr onclick=\"document.location = '";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user.edit", array("id" => $this->getAttribute($context["user"], "id", array()))), "html", null, true);
            echo "'\" style=\"cursor: pointer;\">

                    <td>
                        ";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "username", array()), "html", null, true);
            echo "

                    </td>
                    <td> ";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "firstname", array()), "html", null, true);
            echo " </td>
                    <td> ";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "lastname", array()), "html", null, true);
            echo " </td>
                    <td> ";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "email", array()), "html", null, true);
            echo " </td>
                    <td><img class=\"img-responsive\" src=\"";
            // line 36
            echo twig_escape_filter($this->env, ($context["user_image_path"] ?? $this->getContext($context, "user_image_path")), "html", null, true);
            echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "ProfilePicture", array()), "html", null, true);
            echo "\" style=\"max-width:50px;\"></td>
                    <td>
                        ";
            // line 38
            if (($this->getAttribute($context["user"], "isactive", array()) == "1")) {
                // line 39
                echo "                            <input checked disabled data-toggle=\"toggle\" type=\"checkbox\">
                        ";
            } else {
                // line 41
                echo "                            <input disabled data-toggle=\"toggle\" type=\"checkbox\">
                        ";
            }
            // line 43
            echo "                    </td>

                </tr>
               ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        echo "            </table>
        </div>
    </div>
</div>
    
    
    
    
    
    
    

";
        
        $__internal_29a72b6d1c1346ba1512eadefc1b23fc4cfab5aa804aeb7d21809dad67d0e690->leave($__internal_29a72b6d1c1346ba1512eadefc1b23fc4cfab5aa804aeb7d21809dad67d0e690_prof);

        
        $__internal_9601e014af5a77f643a876663ea9ad806af6aed9cf4a8fa4dd7c28d0f2f8a878->leave($__internal_9601e014af5a77f643a876663ea9ad806af6aed9cf4a8fa4dd7c28d0f2f8a878_prof);

    }

    public function getTemplateName()
    {
        return "login/userlist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 47,  118 => 43,  114 => 41,  110 => 39,  108 => 38,  102 => 36,  98 => 35,  94 => 34,  90 => 33,  84 => 30,  77 => 27,  73 => 26,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}

    <link href=\"https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css\" rel=\"stylesheet\">
    <script src=\"https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js\"></script>
    <style>
.paginator {
    margin-left: 10px;
}
</style>
<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1>All Users</h1>
            <table class=\"table table-hover table-striped\">
                <tr>
                    <th>Username</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Email</th>
                    <th>Image</th>
                    <th>Active</th>
                </tr>

                {% for user in users %}
                <tr onclick=\"document.location = '{{path('user.edit', {id: user.id}) }}'\" style=\"cursor: pointer;\">

                    <td>
                        {{user.username}}

                    </td>
                    <td> {{ user.firstname }} </td>
                    <td> {{ user.lastname }} </td>
                    <td> {{ user.email }} </td>
                    <td><img class=\"img-responsive\" src=\"{{user_image_path}}{{ user.ProfilePicture }}\" style=\"max-width:50px;\"></td>
                    <td>
                        {% if user.isactive == \"1\" %}
                            <input checked disabled data-toggle=\"toggle\" type=\"checkbox\">
                        {% else %}
                            <input disabled data-toggle=\"toggle\" type=\"checkbox\">
                        {% endif %}
                    </td>

                </tr>
               {% endfor %}
            </table>
        </div>
    </div>
</div>
    
    
    
    
    
    
    

{% endblock %}
", "login/userlist.html.twig", "/var/www/pat/app/Resources/views/login/userlist.html.twig");
    }
}

<?php

/* base.html.twig */
class __TwigTemplate_c684b3384bd4334b4f6d2add3c23d263a6fea8e8da0f033c71de57d3392df686 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
            'login' => array($this, 'block_login'),
            'user_edit' => array($this, 'block_user_edit'),
            'register' => array($this, 'block_register'),
            'page_content' => array($this, 'block_page_content'),
            'service_list' => array($this, 'block_service_list'),
            'service_detail' => array($this, 'block_service_detail'),
            'service_save' => array($this, 'block_service_save'),
            'service_transaction_list' => array($this, 'block_service_transaction_list'),
            'service_transaction_detail' => array($this, 'block_service_transaction_detail'),
            'service_transaction_save' => array($this, 'block_service_transaction_save'),
            'user_list' => array($this, 'block_user_list'),
            'payment_type_list' => array($this, 'block_payment_type_list'),
            'payment_type_detail' => array($this, 'block_payment_type_detail'),
            'payment_type_save' => array($this, 'block_payment_type_save'),
            'service_frequency_list' => array($this, 'block_service_frequency_list'),
            'service_frequency_detail' => array($this, 'block_service_frequency_detail'),
            'service_frequency_save' => array($this, 'block_service_frequency_save'),
            'category_list' => array($this, 'block_category_list'),
            'category_detail' => array($this, 'block_category_detail'),
            'category_save' => array($this, 'block_category_save'),
            'translation_map_list' => array($this, 'block_translation_map_list'),
            'translation_map_detail' => array($this, 'block_translation_map_detail'),
            'translation_map_save' => array($this, 'block_translation_map_save'),
            'search_result' => array($this, 'block_search_result'),
            'scan' => array($this, 'block_scan'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0cd7304e20b9ef9a74fc6620422b7775da549db81ae7a22c0ba9235cad85c0e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0cd7304e20b9ef9a74fc6620422b7775da549db81ae7a22c0ba9235cad85c0e0->enter($__internal_0cd7304e20b9ef9a74fc6620422b7775da549db81ae7a22c0ba9235cad85c0e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_3ded6f4e916cffbccaf93078ba59f6f2e682d52c9c48144315c2d74cefd3863a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ded6f4e916cffbccaf93078ba59f6f2e682d52c9c48144315c2d74cefd3863a->enter($__internal_3ded6f4e916cffbccaf93078ba59f6f2e682d52c9c48144315c2d74cefd3863a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 7
        echo "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
    <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/css/style.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/css/login.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/css/footer.css"), "html", null, true);
        echo "\" />
    <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" />
</head>

<body>
    <div class=\"navbar navbar-inverse navbar-fixed-top\">
        <div class=\"container\">
            <div class=\"navbar-collapse collapse\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/\">Home</a></li>
                        ";
        // line 23
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_FULLY")) {
            // line 24
            echo "                            ";
            if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
                // line 25
                echo "
                        ";
            }
            // line 27
            echo "                        ";
            if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_CREATOR")) {
                // line 28
                echo "                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Category<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/category/list\">Category List</a></li>
                                    <li><a href=\"/category/save\">Create New Category</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Service<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/service/list\">Service List</a></li>
                                    <li><a href=\"/service/save\">Create New Service</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Service Transaction<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/service_transaction/list\">Service Transaction List</a></li>
                                    <li><a href=\"/service_transaction/save\">Create New Service Transaction</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Payment Type<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/paymenttype/list\">Payment Type List</a></li>
                                    <li><a href=\"/paymenttype/save\">Create Payment Type</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Service Frequency<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/service_frequency/list\">Service Frequency List</a></li>
                                    <li><a href=\"/service_frequency/save\">Create Service Frequency</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Translation Map<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/translation_map/list\">Translation Map List</a></li>
                                    <li><a href=\"/translation_map/save\">Create Translation Map</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Users<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/register\">Create New User</a></li>
                                    <li><a href=\"/userlist\">User List</a></li>
                                    <li><a href=\"/userlist_pdf\">User List In PDF</a></li>
                                </ul>
                            </li>
                        ";
            }
            // line 79
            echo "                        <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">&nbsp; <i class=\"fa fa-circle text-success\"></i> ";
            // line 80
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "username", array()), "html", null, true);
            echo "<b class=\"caret\"></b></a>
                            <ul class=\"dropdown-menu\">
                                <li><a href=\"";
            // line 82
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("user.edit", array("id" => $this->getAttribute($this->getAttribute(($context["app"] ?? $this->getContext($context, "app")), "user", array()), "id", array()))), "html", null, true);
            echo "\">Profile</a></li>
                                <li><a href=\"";
            // line 83
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\LogoutUrlExtension')->getLogoutPath(), "html", null, true);
            echo "\">Logout</a></li>
                            </ul>
                        </li>

                    ";
        } else {
            // line 88
            echo "                        <li><a href=\"/login\" >Sign in</a> </li>


                    ";
        }
        // line 92
        echo "                </ul>
            </div>
        </div>
    </div>
    <div style=\"margin-top:40px;\">
    ";
        // line 97
        $this->displayBlock('body', $context, $blocks);
        // line 98
        $this->displayBlock('javascripts', $context, $blocks);
        // line 99
        $this->displayBlock('login', $context, $blocks);
        // line 100
        $this->displayBlock('user_edit', $context, $blocks);
        // line 101
        $this->displayBlock('register', $context, $blocks);
        // line 102
        $this->displayBlock('page_content', $context, $blocks);
        // line 103
        $this->displayBlock('service_list', $context, $blocks);
        // line 104
        $this->displayBlock('service_detail', $context, $blocks);
        // line 105
        $this->displayBlock('service_save', $context, $blocks);
        // line 106
        $this->displayBlock('service_transaction_list', $context, $blocks);
        // line 107
        $this->displayBlock('service_transaction_detail', $context, $blocks);
        // line 108
        $this->displayBlock('service_transaction_save', $context, $blocks);
        // line 109
        $this->displayBlock('user_list', $context, $blocks);
        // line 110
        $this->displayBlock('payment_type_list', $context, $blocks);
        // line 111
        $this->displayBlock('payment_type_detail', $context, $blocks);
        // line 112
        $this->displayBlock('payment_type_save', $context, $blocks);
        // line 113
        $this->displayBlock('service_frequency_list', $context, $blocks);
        // line 114
        $this->displayBlock('service_frequency_detail', $context, $blocks);
        // line 115
        $this->displayBlock('service_frequency_save', $context, $blocks);
        // line 116
        $this->displayBlock('category_list', $context, $blocks);
        // line 117
        $this->displayBlock('category_detail', $context, $blocks);
        // line 118
        $this->displayBlock('category_save', $context, $blocks);
        // line 119
        $this->displayBlock('translation_map_list', $context, $blocks);
        // line 120
        $this->displayBlock('translation_map_detail', $context, $blocks);
        // line 121
        $this->displayBlock('translation_map_save', $context, $blocks);
        // line 122
        $this->displayBlock('search_result', $context, $blocks);
        // line 123
        $this->displayBlock('scan', $context, $blocks);
        // line 124
        echo "<footer class=\"footer-distributed\">

    <div class=\"footer-left\">

        <h3>GTS Payment<span>System</span></h3>

        <p class=\"footer-links\">

        </p>

        <p class=\"footer-company-name\">Global Technology Solutiond &copy; 2018</p>
    </div>

    <div class=\"footer-center\">

        <div>
            <i class=\"fa fa-map-marker\"></i>
            <p><span>110 Toktogul Street</span> Bishkek, Kyrgyzstan</p>
        </div>

        <div>
            <i class=\"fa fa-phone\"></i>
            <p>Office: 996 312 881 982</p>
            <p>Mob.  : 996 551 442 488 </p>
        </div>

        <div>
            <i class=\"fa fa-envelope\"></i>
            <p><a href=\"mailto:office@glotechsol.com\">office@glotechsol.com</a></p>
        </div>

    </div>

    <div class=\"footer-right\">

        <p class=\"footer-company-about\">
            <span>About GTS Payment Management Admin Tool</span>
            For World!
        </p>

        <div class=\"footer-icons\">
            <a href=\"#\"><i class=\"fa fa-facebook\"></i></a>
            <a href=\"#\"><i class=\"fa fa-twitter\"></i></a>
            <a href=\"#\"><i class=\"fa fa-linkedin\"></i></a>
            <a href=\"#\"><i class=\"fa fa-github\"></i></a>

        </div>

    </div>

</footer>
</body>
</html>
";
        
        $__internal_0cd7304e20b9ef9a74fc6620422b7775da549db81ae7a22c0ba9235cad85c0e0->leave($__internal_0cd7304e20b9ef9a74fc6620422b7775da549db81ae7a22c0ba9235cad85c0e0_prof);

        
        $__internal_3ded6f4e916cffbccaf93078ba59f6f2e682d52c9c48144315c2d74cefd3863a->leave($__internal_3ded6f4e916cffbccaf93078ba59f6f2e682d52c9c48144315c2d74cefd3863a_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_c2d5db46ef2e3b2bc8ff9f7c7602d37bca245807b7b0f0e7d9887015b0395e5a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c2d5db46ef2e3b2bc8ff9f7c7602d37bca245807b7b0f0e7d9887015b0395e5a->enter($__internal_c2d5db46ef2e3b2bc8ff9f7c7602d37bca245807b7b0f0e7d9887015b0395e5a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1db55765d6aa29720ff517fa8a75158c681d794f7c36fbd77cdd1453285a28df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1db55765d6aa29720ff517fa8a75158c681d794f7c36fbd77cdd1453285a28df->enter($__internal_1db55765d6aa29720ff517fa8a75158c681d794f7c36fbd77cdd1453285a28df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_1db55765d6aa29720ff517fa8a75158c681d794f7c36fbd77cdd1453285a28df->leave($__internal_1db55765d6aa29720ff517fa8a75158c681d794f7c36fbd77cdd1453285a28df_prof);

        
        $__internal_c2d5db46ef2e3b2bc8ff9f7c7602d37bca245807b7b0f0e7d9887015b0395e5a->leave($__internal_c2d5db46ef2e3b2bc8ff9f7c7602d37bca245807b7b0f0e7d9887015b0395e5a_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f3143e65509631600c692c2f7a1a33d10886abdfa090b0d3b5802b62d30a3c40 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f3143e65509631600c692c2f7a1a33d10886abdfa090b0d3b5802b62d30a3c40->enter($__internal_f3143e65509631600c692c2f7a1a33d10886abdfa090b0d3b5802b62d30a3c40_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_750ab77fe51be1adf62f8211d87eab6475377bac4e33131adf934b31895fe6f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_750ab77fe51be1adf62f8211d87eab6475377bac4e33131adf934b31895fe6f4->enter($__internal_750ab77fe51be1adf62f8211d87eab6475377bac4e33131adf934b31895fe6f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_750ab77fe51be1adf62f8211d87eab6475377bac4e33131adf934b31895fe6f4->leave($__internal_750ab77fe51be1adf62f8211d87eab6475377bac4e33131adf934b31895fe6f4_prof);

        
        $__internal_f3143e65509631600c692c2f7a1a33d10886abdfa090b0d3b5802b62d30a3c40->leave($__internal_f3143e65509631600c692c2f7a1a33d10886abdfa090b0d3b5802b62d30a3c40_prof);

    }

    // line 97
    public function block_body($context, array $blocks = array())
    {
        $__internal_794ec828bd48267bacedba3cee71d96421a63ac36cbcc40dfe90f71509e29eae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_794ec828bd48267bacedba3cee71d96421a63ac36cbcc40dfe90f71509e29eae->enter($__internal_794ec828bd48267bacedba3cee71d96421a63ac36cbcc40dfe90f71509e29eae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_edecbdcb6bcb55bb491837212c694be8427a3b675e3d32bfa2f103e288b67b3f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_edecbdcb6bcb55bb491837212c694be8427a3b675e3d32bfa2f103e288b67b3f->enter($__internal_edecbdcb6bcb55bb491837212c694be8427a3b675e3d32bfa2f103e288b67b3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_edecbdcb6bcb55bb491837212c694be8427a3b675e3d32bfa2f103e288b67b3f->leave($__internal_edecbdcb6bcb55bb491837212c694be8427a3b675e3d32bfa2f103e288b67b3f_prof);

        
        $__internal_794ec828bd48267bacedba3cee71d96421a63ac36cbcc40dfe90f71509e29eae->leave($__internal_794ec828bd48267bacedba3cee71d96421a63ac36cbcc40dfe90f71509e29eae_prof);

    }

    // line 98
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_3fe98c1e147487e688687f1c43d18c40e4f06aa0f7c9deb3cd231a5101f9d17a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3fe98c1e147487e688687f1c43d18c40e4f06aa0f7c9deb3cd231a5101f9d17a->enter($__internal_3fe98c1e147487e688687f1c43d18c40e4f06aa0f7c9deb3cd231a5101f9d17a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_d52cbdad221c7646ae87513939fd0dcd5e1bb5d3e2e23dbebdc5b385bd6c12a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d52cbdad221c7646ae87513939fd0dcd5e1bb5d3e2e23dbebdc5b385bd6c12a5->enter($__internal_d52cbdad221c7646ae87513939fd0dcd5e1bb5d3e2e23dbebdc5b385bd6c12a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_d52cbdad221c7646ae87513939fd0dcd5e1bb5d3e2e23dbebdc5b385bd6c12a5->leave($__internal_d52cbdad221c7646ae87513939fd0dcd5e1bb5d3e2e23dbebdc5b385bd6c12a5_prof);

        
        $__internal_3fe98c1e147487e688687f1c43d18c40e4f06aa0f7c9deb3cd231a5101f9d17a->leave($__internal_3fe98c1e147487e688687f1c43d18c40e4f06aa0f7c9deb3cd231a5101f9d17a_prof);

    }

    // line 99
    public function block_login($context, array $blocks = array())
    {
        $__internal_0bd9cd802fca8f86a420961b2228f2bdbce2ca6865cc05267dcda78cda7a0048 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0bd9cd802fca8f86a420961b2228f2bdbce2ca6865cc05267dcda78cda7a0048->enter($__internal_0bd9cd802fca8f86a420961b2228f2bdbce2ca6865cc05267dcda78cda7a0048_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "login"));

        $__internal_e28c8bc04de7b5bd6ea7a9761e7a40c33dea636e29363e3e6aa14a7bee33cd59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e28c8bc04de7b5bd6ea7a9761e7a40c33dea636e29363e3e6aa14a7bee33cd59->enter($__internal_e28c8bc04de7b5bd6ea7a9761e7a40c33dea636e29363e3e6aa14a7bee33cd59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "login"));

        
        $__internal_e28c8bc04de7b5bd6ea7a9761e7a40c33dea636e29363e3e6aa14a7bee33cd59->leave($__internal_e28c8bc04de7b5bd6ea7a9761e7a40c33dea636e29363e3e6aa14a7bee33cd59_prof);

        
        $__internal_0bd9cd802fca8f86a420961b2228f2bdbce2ca6865cc05267dcda78cda7a0048->leave($__internal_0bd9cd802fca8f86a420961b2228f2bdbce2ca6865cc05267dcda78cda7a0048_prof);

    }

    // line 100
    public function block_user_edit($context, array $blocks = array())
    {
        $__internal_7ccf2f9792f6d51b559461ddcfa98e9e08787113da5c3ab20b228cf9a6f7be73 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ccf2f9792f6d51b559461ddcfa98e9e08787113da5c3ab20b228cf9a6f7be73->enter($__internal_7ccf2f9792f6d51b559461ddcfa98e9e08787113da5c3ab20b228cf9a6f7be73_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_edit"));

        $__internal_1a22827f4795a2002bc3d48847265310af3fd7deb00345aa50a93b37c6fb937f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1a22827f4795a2002bc3d48847265310af3fd7deb00345aa50a93b37c6fb937f->enter($__internal_1a22827f4795a2002bc3d48847265310af3fd7deb00345aa50a93b37c6fb937f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_edit"));

        
        $__internal_1a22827f4795a2002bc3d48847265310af3fd7deb00345aa50a93b37c6fb937f->leave($__internal_1a22827f4795a2002bc3d48847265310af3fd7deb00345aa50a93b37c6fb937f_prof);

        
        $__internal_7ccf2f9792f6d51b559461ddcfa98e9e08787113da5c3ab20b228cf9a6f7be73->leave($__internal_7ccf2f9792f6d51b559461ddcfa98e9e08787113da5c3ab20b228cf9a6f7be73_prof);

    }

    // line 101
    public function block_register($context, array $blocks = array())
    {
        $__internal_79cce753304534c31d3bdeb7c3287a56cf90f9417ebdf2d01703808743b753e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79cce753304534c31d3bdeb7c3287a56cf90f9417ebdf2d01703808743b753e3->enter($__internal_79cce753304534c31d3bdeb7c3287a56cf90f9417ebdf2d01703808743b753e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "register"));

        $__internal_7c52c2205271612ccf29aca6d24c41f7285b5e4bc714b579526ae5249252ae83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c52c2205271612ccf29aca6d24c41f7285b5e4bc714b579526ae5249252ae83->enter($__internal_7c52c2205271612ccf29aca6d24c41f7285b5e4bc714b579526ae5249252ae83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "register"));

        
        $__internal_7c52c2205271612ccf29aca6d24c41f7285b5e4bc714b579526ae5249252ae83->leave($__internal_7c52c2205271612ccf29aca6d24c41f7285b5e4bc714b579526ae5249252ae83_prof);

        
        $__internal_79cce753304534c31d3bdeb7c3287a56cf90f9417ebdf2d01703808743b753e3->leave($__internal_79cce753304534c31d3bdeb7c3287a56cf90f9417ebdf2d01703808743b753e3_prof);

    }

    // line 102
    public function block_page_content($context, array $blocks = array())
    {
        $__internal_4a4e0c83afcc0a281b8a0fa7c12b89b16addf364a36766268c6dc3f6dff28613 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a4e0c83afcc0a281b8a0fa7c12b89b16addf364a36766268c6dc3f6dff28613->enter($__internal_4a4e0c83afcc0a281b8a0fa7c12b89b16addf364a36766268c6dc3f6dff28613_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_content"));

        $__internal_363a5a7ebf42506ce7523e7143ec5a18fc651ee92f0cf21075d6c6f88a867112 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_363a5a7ebf42506ce7523e7143ec5a18fc651ee92f0cf21075d6c6f88a867112->enter($__internal_363a5a7ebf42506ce7523e7143ec5a18fc651ee92f0cf21075d6c6f88a867112_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_content"));

        
        $__internal_363a5a7ebf42506ce7523e7143ec5a18fc651ee92f0cf21075d6c6f88a867112->leave($__internal_363a5a7ebf42506ce7523e7143ec5a18fc651ee92f0cf21075d6c6f88a867112_prof);

        
        $__internal_4a4e0c83afcc0a281b8a0fa7c12b89b16addf364a36766268c6dc3f6dff28613->leave($__internal_4a4e0c83afcc0a281b8a0fa7c12b89b16addf364a36766268c6dc3f6dff28613_prof);

    }

    // line 103
    public function block_service_list($context, array $blocks = array())
    {
        $__internal_5a270a860f71434e25d4c998233b98dfcf828a3994c4a0a41bd937733883940b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5a270a860f71434e25d4c998233b98dfcf828a3994c4a0a41bd937733883940b->enter($__internal_5a270a860f71434e25d4c998233b98dfcf828a3994c4a0a41bd937733883940b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_list"));

        $__internal_7095e423ae360f5b697a568494db190c94ee4f4f531acd79610854ec74629b89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7095e423ae360f5b697a568494db190c94ee4f4f531acd79610854ec74629b89->enter($__internal_7095e423ae360f5b697a568494db190c94ee4f4f531acd79610854ec74629b89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_list"));

        
        $__internal_7095e423ae360f5b697a568494db190c94ee4f4f531acd79610854ec74629b89->leave($__internal_7095e423ae360f5b697a568494db190c94ee4f4f531acd79610854ec74629b89_prof);

        
        $__internal_5a270a860f71434e25d4c998233b98dfcf828a3994c4a0a41bd937733883940b->leave($__internal_5a270a860f71434e25d4c998233b98dfcf828a3994c4a0a41bd937733883940b_prof);

    }

    // line 104
    public function block_service_detail($context, array $blocks = array())
    {
        $__internal_090677be7b1127db8ba287625b260cbc53757038916ff86b1bd8ce693e2c5175 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_090677be7b1127db8ba287625b260cbc53757038916ff86b1bd8ce693e2c5175->enter($__internal_090677be7b1127db8ba287625b260cbc53757038916ff86b1bd8ce693e2c5175_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_detail"));

        $__internal_d58637aadd26ab96559c6fa6454f7ac41101dee3b0a5041d39f611673c50b5e6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d58637aadd26ab96559c6fa6454f7ac41101dee3b0a5041d39f611673c50b5e6->enter($__internal_d58637aadd26ab96559c6fa6454f7ac41101dee3b0a5041d39f611673c50b5e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_detail"));

        
        $__internal_d58637aadd26ab96559c6fa6454f7ac41101dee3b0a5041d39f611673c50b5e6->leave($__internal_d58637aadd26ab96559c6fa6454f7ac41101dee3b0a5041d39f611673c50b5e6_prof);

        
        $__internal_090677be7b1127db8ba287625b260cbc53757038916ff86b1bd8ce693e2c5175->leave($__internal_090677be7b1127db8ba287625b260cbc53757038916ff86b1bd8ce693e2c5175_prof);

    }

    // line 105
    public function block_service_save($context, array $blocks = array())
    {
        $__internal_94b95e4642eeb3761cd3a312a38810247e853b10b5f9220e19f26822d49c26d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_94b95e4642eeb3761cd3a312a38810247e853b10b5f9220e19f26822d49c26d9->enter($__internal_94b95e4642eeb3761cd3a312a38810247e853b10b5f9220e19f26822d49c26d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_save"));

        $__internal_858e795f7eaf77781e602073a2b5888c770e587344e0e62e0c9c829b91703f03 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_858e795f7eaf77781e602073a2b5888c770e587344e0e62e0c9c829b91703f03->enter($__internal_858e795f7eaf77781e602073a2b5888c770e587344e0e62e0c9c829b91703f03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_save"));

        
        $__internal_858e795f7eaf77781e602073a2b5888c770e587344e0e62e0c9c829b91703f03->leave($__internal_858e795f7eaf77781e602073a2b5888c770e587344e0e62e0c9c829b91703f03_prof);

        
        $__internal_94b95e4642eeb3761cd3a312a38810247e853b10b5f9220e19f26822d49c26d9->leave($__internal_94b95e4642eeb3761cd3a312a38810247e853b10b5f9220e19f26822d49c26d9_prof);

    }

    // line 106
    public function block_service_transaction_list($context, array $blocks = array())
    {
        $__internal_16be44eec9d1868bffc1c2751c421c1158c01d13fb092761053e629bac5fb389 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_16be44eec9d1868bffc1c2751c421c1158c01d13fb092761053e629bac5fb389->enter($__internal_16be44eec9d1868bffc1c2751c421c1158c01d13fb092761053e629bac5fb389_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_transaction_list"));

        $__internal_29a5406ac500fc698902cfb7a16be5bc7008d3a65e3224973be752a540ab1879 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29a5406ac500fc698902cfb7a16be5bc7008d3a65e3224973be752a540ab1879->enter($__internal_29a5406ac500fc698902cfb7a16be5bc7008d3a65e3224973be752a540ab1879_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_transaction_list"));

        
        $__internal_29a5406ac500fc698902cfb7a16be5bc7008d3a65e3224973be752a540ab1879->leave($__internal_29a5406ac500fc698902cfb7a16be5bc7008d3a65e3224973be752a540ab1879_prof);

        
        $__internal_16be44eec9d1868bffc1c2751c421c1158c01d13fb092761053e629bac5fb389->leave($__internal_16be44eec9d1868bffc1c2751c421c1158c01d13fb092761053e629bac5fb389_prof);

    }

    // line 107
    public function block_service_transaction_detail($context, array $blocks = array())
    {
        $__internal_fd44a0766d4258f83bb4c35d948d4ac91e36c73c9442b4cf8eceedfd7e7eb3fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd44a0766d4258f83bb4c35d948d4ac91e36c73c9442b4cf8eceedfd7e7eb3fe->enter($__internal_fd44a0766d4258f83bb4c35d948d4ac91e36c73c9442b4cf8eceedfd7e7eb3fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_transaction_detail"));

        $__internal_256747042c4ea4612b80bac1ef5f2fc9aeae50f71d82ff283a7a5cfd67dd898e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_256747042c4ea4612b80bac1ef5f2fc9aeae50f71d82ff283a7a5cfd67dd898e->enter($__internal_256747042c4ea4612b80bac1ef5f2fc9aeae50f71d82ff283a7a5cfd67dd898e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_transaction_detail"));

        
        $__internal_256747042c4ea4612b80bac1ef5f2fc9aeae50f71d82ff283a7a5cfd67dd898e->leave($__internal_256747042c4ea4612b80bac1ef5f2fc9aeae50f71d82ff283a7a5cfd67dd898e_prof);

        
        $__internal_fd44a0766d4258f83bb4c35d948d4ac91e36c73c9442b4cf8eceedfd7e7eb3fe->leave($__internal_fd44a0766d4258f83bb4c35d948d4ac91e36c73c9442b4cf8eceedfd7e7eb3fe_prof);

    }

    // line 108
    public function block_service_transaction_save($context, array $blocks = array())
    {
        $__internal_9ac9a5de24649f88c5fd737262bdca898e4e3f42ec95e3c0077f83feea5aca62 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ac9a5de24649f88c5fd737262bdca898e4e3f42ec95e3c0077f83feea5aca62->enter($__internal_9ac9a5de24649f88c5fd737262bdca898e4e3f42ec95e3c0077f83feea5aca62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_transaction_save"));

        $__internal_1e26f0ac8cb1e48e2b8a6858b901589919f3b33148425ba8b580a5f9e0068d2c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1e26f0ac8cb1e48e2b8a6858b901589919f3b33148425ba8b580a5f9e0068d2c->enter($__internal_1e26f0ac8cb1e48e2b8a6858b901589919f3b33148425ba8b580a5f9e0068d2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_transaction_save"));

        
        $__internal_1e26f0ac8cb1e48e2b8a6858b901589919f3b33148425ba8b580a5f9e0068d2c->leave($__internal_1e26f0ac8cb1e48e2b8a6858b901589919f3b33148425ba8b580a5f9e0068d2c_prof);

        
        $__internal_9ac9a5de24649f88c5fd737262bdca898e4e3f42ec95e3c0077f83feea5aca62->leave($__internal_9ac9a5de24649f88c5fd737262bdca898e4e3f42ec95e3c0077f83feea5aca62_prof);

    }

    // line 109
    public function block_user_list($context, array $blocks = array())
    {
        $__internal_28f17e7c3c06b9e2fa98fc84a15a70dc3899fc1dd8bbf11d20843d6edd03c0ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_28f17e7c3c06b9e2fa98fc84a15a70dc3899fc1dd8bbf11d20843d6edd03c0ba->enter($__internal_28f17e7c3c06b9e2fa98fc84a15a70dc3899fc1dd8bbf11d20843d6edd03c0ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_list"));

        $__internal_19d1d3b2c8855ca1677843a748b4ef83c75487849f19918e2206bce1bee2ae16 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19d1d3b2c8855ca1677843a748b4ef83c75487849f19918e2206bce1bee2ae16->enter($__internal_19d1d3b2c8855ca1677843a748b4ef83c75487849f19918e2206bce1bee2ae16_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_list"));

        
        $__internal_19d1d3b2c8855ca1677843a748b4ef83c75487849f19918e2206bce1bee2ae16->leave($__internal_19d1d3b2c8855ca1677843a748b4ef83c75487849f19918e2206bce1bee2ae16_prof);

        
        $__internal_28f17e7c3c06b9e2fa98fc84a15a70dc3899fc1dd8bbf11d20843d6edd03c0ba->leave($__internal_28f17e7c3c06b9e2fa98fc84a15a70dc3899fc1dd8bbf11d20843d6edd03c0ba_prof);

    }

    // line 110
    public function block_payment_type_list($context, array $blocks = array())
    {
        $__internal_d9ac76315d8778e2a6b44634cc8d583e2b61f8be350b700dd71640039f6fe662 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d9ac76315d8778e2a6b44634cc8d583e2b61f8be350b700dd71640039f6fe662->enter($__internal_d9ac76315d8778e2a6b44634cc8d583e2b61f8be350b700dd71640039f6fe662_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "payment_type_list"));

        $__internal_d5c0c6cb7fd7124359c0f3340b6d60c0759dcedd3d61358d62237a6ded457b96 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5c0c6cb7fd7124359c0f3340b6d60c0759dcedd3d61358d62237a6ded457b96->enter($__internal_d5c0c6cb7fd7124359c0f3340b6d60c0759dcedd3d61358d62237a6ded457b96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "payment_type_list"));

        
        $__internal_d5c0c6cb7fd7124359c0f3340b6d60c0759dcedd3d61358d62237a6ded457b96->leave($__internal_d5c0c6cb7fd7124359c0f3340b6d60c0759dcedd3d61358d62237a6ded457b96_prof);

        
        $__internal_d9ac76315d8778e2a6b44634cc8d583e2b61f8be350b700dd71640039f6fe662->leave($__internal_d9ac76315d8778e2a6b44634cc8d583e2b61f8be350b700dd71640039f6fe662_prof);

    }

    // line 111
    public function block_payment_type_detail($context, array $blocks = array())
    {
        $__internal_6656a5533320457bee9c898c4db12c0a3bc3815468e1430f66e83da8c7e348eb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6656a5533320457bee9c898c4db12c0a3bc3815468e1430f66e83da8c7e348eb->enter($__internal_6656a5533320457bee9c898c4db12c0a3bc3815468e1430f66e83da8c7e348eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "payment_type_detail"));

        $__internal_a4f429802f0673043dc35478a40242209bec439bee186a95613e371184fd3d6f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a4f429802f0673043dc35478a40242209bec439bee186a95613e371184fd3d6f->enter($__internal_a4f429802f0673043dc35478a40242209bec439bee186a95613e371184fd3d6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "payment_type_detail"));

        
        $__internal_a4f429802f0673043dc35478a40242209bec439bee186a95613e371184fd3d6f->leave($__internal_a4f429802f0673043dc35478a40242209bec439bee186a95613e371184fd3d6f_prof);

        
        $__internal_6656a5533320457bee9c898c4db12c0a3bc3815468e1430f66e83da8c7e348eb->leave($__internal_6656a5533320457bee9c898c4db12c0a3bc3815468e1430f66e83da8c7e348eb_prof);

    }

    // line 112
    public function block_payment_type_save($context, array $blocks = array())
    {
        $__internal_1075067a3b91d271ffa1c72577b2dff47db803fe746795bf388ec5a83483a793 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1075067a3b91d271ffa1c72577b2dff47db803fe746795bf388ec5a83483a793->enter($__internal_1075067a3b91d271ffa1c72577b2dff47db803fe746795bf388ec5a83483a793_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "payment_type_save"));

        $__internal_f3ea415318e119bcf28f80423e7f925ae85b4b815bb0822787e6cb65980a0acd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3ea415318e119bcf28f80423e7f925ae85b4b815bb0822787e6cb65980a0acd->enter($__internal_f3ea415318e119bcf28f80423e7f925ae85b4b815bb0822787e6cb65980a0acd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "payment_type_save"));

        
        $__internal_f3ea415318e119bcf28f80423e7f925ae85b4b815bb0822787e6cb65980a0acd->leave($__internal_f3ea415318e119bcf28f80423e7f925ae85b4b815bb0822787e6cb65980a0acd_prof);

        
        $__internal_1075067a3b91d271ffa1c72577b2dff47db803fe746795bf388ec5a83483a793->leave($__internal_1075067a3b91d271ffa1c72577b2dff47db803fe746795bf388ec5a83483a793_prof);

    }

    // line 113
    public function block_service_frequency_list($context, array $blocks = array())
    {
        $__internal_4e4c990f1aca3d77bef0616982f80e7e3e76baa677c46a58b13457a95908651a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4e4c990f1aca3d77bef0616982f80e7e3e76baa677c46a58b13457a95908651a->enter($__internal_4e4c990f1aca3d77bef0616982f80e7e3e76baa677c46a58b13457a95908651a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_frequency_list"));

        $__internal_4f57bb6923257f69c3abd5a421d8fdc0aba82e3d770a0ee8300ff9611b17187e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f57bb6923257f69c3abd5a421d8fdc0aba82e3d770a0ee8300ff9611b17187e->enter($__internal_4f57bb6923257f69c3abd5a421d8fdc0aba82e3d770a0ee8300ff9611b17187e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_frequency_list"));

        
        $__internal_4f57bb6923257f69c3abd5a421d8fdc0aba82e3d770a0ee8300ff9611b17187e->leave($__internal_4f57bb6923257f69c3abd5a421d8fdc0aba82e3d770a0ee8300ff9611b17187e_prof);

        
        $__internal_4e4c990f1aca3d77bef0616982f80e7e3e76baa677c46a58b13457a95908651a->leave($__internal_4e4c990f1aca3d77bef0616982f80e7e3e76baa677c46a58b13457a95908651a_prof);

    }

    // line 114
    public function block_service_frequency_detail($context, array $blocks = array())
    {
        $__internal_7250832ed4da178a888a2c301686f566503b89762c232c2a282e4c8e548c414d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7250832ed4da178a888a2c301686f566503b89762c232c2a282e4c8e548c414d->enter($__internal_7250832ed4da178a888a2c301686f566503b89762c232c2a282e4c8e548c414d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_frequency_detail"));

        $__internal_ba4fb135c925c781d55d7b0ce48fbdf7e9f0521bfb96b986b30ecbe7dd2e42a1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba4fb135c925c781d55d7b0ce48fbdf7e9f0521bfb96b986b30ecbe7dd2e42a1->enter($__internal_ba4fb135c925c781d55d7b0ce48fbdf7e9f0521bfb96b986b30ecbe7dd2e42a1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_frequency_detail"));

        
        $__internal_ba4fb135c925c781d55d7b0ce48fbdf7e9f0521bfb96b986b30ecbe7dd2e42a1->leave($__internal_ba4fb135c925c781d55d7b0ce48fbdf7e9f0521bfb96b986b30ecbe7dd2e42a1_prof);

        
        $__internal_7250832ed4da178a888a2c301686f566503b89762c232c2a282e4c8e548c414d->leave($__internal_7250832ed4da178a888a2c301686f566503b89762c232c2a282e4c8e548c414d_prof);

    }

    // line 115
    public function block_service_frequency_save($context, array $blocks = array())
    {
        $__internal_dbb7c768cfdc533ced3a5de993875b1ec7f48ef5291f1343062b28f8e0a962b0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dbb7c768cfdc533ced3a5de993875b1ec7f48ef5291f1343062b28f8e0a962b0->enter($__internal_dbb7c768cfdc533ced3a5de993875b1ec7f48ef5291f1343062b28f8e0a962b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_frequency_save"));

        $__internal_ba01555f875981f353be75b0b8376127992be29e075ad9dda6fa7b3161f03484 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ba01555f875981f353be75b0b8376127992be29e075ad9dda6fa7b3161f03484->enter($__internal_ba01555f875981f353be75b0b8376127992be29e075ad9dda6fa7b3161f03484_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_frequency_save"));

        
        $__internal_ba01555f875981f353be75b0b8376127992be29e075ad9dda6fa7b3161f03484->leave($__internal_ba01555f875981f353be75b0b8376127992be29e075ad9dda6fa7b3161f03484_prof);

        
        $__internal_dbb7c768cfdc533ced3a5de993875b1ec7f48ef5291f1343062b28f8e0a962b0->leave($__internal_dbb7c768cfdc533ced3a5de993875b1ec7f48ef5291f1343062b28f8e0a962b0_prof);

    }

    // line 116
    public function block_category_list($context, array $blocks = array())
    {
        $__internal_db340ac63f62269a233df424e1fafadaa2ef2e709018eb6bb795005fe831d469 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_db340ac63f62269a233df424e1fafadaa2ef2e709018eb6bb795005fe831d469->enter($__internal_db340ac63f62269a233df424e1fafadaa2ef2e709018eb6bb795005fe831d469_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "category_list"));

        $__internal_07cad6aa121a4a554e59a6fc66de58dacff25f82bacf2475418aa8ac974947a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_07cad6aa121a4a554e59a6fc66de58dacff25f82bacf2475418aa8ac974947a3->enter($__internal_07cad6aa121a4a554e59a6fc66de58dacff25f82bacf2475418aa8ac974947a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "category_list"));

        
        $__internal_07cad6aa121a4a554e59a6fc66de58dacff25f82bacf2475418aa8ac974947a3->leave($__internal_07cad6aa121a4a554e59a6fc66de58dacff25f82bacf2475418aa8ac974947a3_prof);

        
        $__internal_db340ac63f62269a233df424e1fafadaa2ef2e709018eb6bb795005fe831d469->leave($__internal_db340ac63f62269a233df424e1fafadaa2ef2e709018eb6bb795005fe831d469_prof);

    }

    // line 117
    public function block_category_detail($context, array $blocks = array())
    {
        $__internal_edf6272d80c2c3c8a2a3d22556e2b87716d60eeaabb7d8c1b8f385e0409a4d99 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_edf6272d80c2c3c8a2a3d22556e2b87716d60eeaabb7d8c1b8f385e0409a4d99->enter($__internal_edf6272d80c2c3c8a2a3d22556e2b87716d60eeaabb7d8c1b8f385e0409a4d99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "category_detail"));

        $__internal_5ba0dfee63b8d3e92fbcfa692b2b32ccc20628c0f984ee434d9ce4e4fbc2568d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ba0dfee63b8d3e92fbcfa692b2b32ccc20628c0f984ee434d9ce4e4fbc2568d->enter($__internal_5ba0dfee63b8d3e92fbcfa692b2b32ccc20628c0f984ee434d9ce4e4fbc2568d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "category_detail"));

        
        $__internal_5ba0dfee63b8d3e92fbcfa692b2b32ccc20628c0f984ee434d9ce4e4fbc2568d->leave($__internal_5ba0dfee63b8d3e92fbcfa692b2b32ccc20628c0f984ee434d9ce4e4fbc2568d_prof);

        
        $__internal_edf6272d80c2c3c8a2a3d22556e2b87716d60eeaabb7d8c1b8f385e0409a4d99->leave($__internal_edf6272d80c2c3c8a2a3d22556e2b87716d60eeaabb7d8c1b8f385e0409a4d99_prof);

    }

    // line 118
    public function block_category_save($context, array $blocks = array())
    {
        $__internal_08e23375b098f24846380761cef145fae2c7ae4d639f9441bf500b85d07c3001 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08e23375b098f24846380761cef145fae2c7ae4d639f9441bf500b85d07c3001->enter($__internal_08e23375b098f24846380761cef145fae2c7ae4d639f9441bf500b85d07c3001_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "category_save"));

        $__internal_dd78f5721a827df8b7df92658f348b80ddf8d493093a28a96c1f0da1ba28637f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dd78f5721a827df8b7df92658f348b80ddf8d493093a28a96c1f0da1ba28637f->enter($__internal_dd78f5721a827df8b7df92658f348b80ddf8d493093a28a96c1f0da1ba28637f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "category_save"));

        
        $__internal_dd78f5721a827df8b7df92658f348b80ddf8d493093a28a96c1f0da1ba28637f->leave($__internal_dd78f5721a827df8b7df92658f348b80ddf8d493093a28a96c1f0da1ba28637f_prof);

        
        $__internal_08e23375b098f24846380761cef145fae2c7ae4d639f9441bf500b85d07c3001->leave($__internal_08e23375b098f24846380761cef145fae2c7ae4d639f9441bf500b85d07c3001_prof);

    }

    // line 119
    public function block_translation_map_list($context, array $blocks = array())
    {
        $__internal_816b2da52cce9f93589e9a04e63003d0faf7ec507336ebeb1628bb3f66fec374 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_816b2da52cce9f93589e9a04e63003d0faf7ec507336ebeb1628bb3f66fec374->enter($__internal_816b2da52cce9f93589e9a04e63003d0faf7ec507336ebeb1628bb3f66fec374_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "translation_map_list"));

        $__internal_29a7e357a44eca7801748407bf5ea1f0de3e37b076386ea5b89c12b7e2318785 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29a7e357a44eca7801748407bf5ea1f0de3e37b076386ea5b89c12b7e2318785->enter($__internal_29a7e357a44eca7801748407bf5ea1f0de3e37b076386ea5b89c12b7e2318785_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "translation_map_list"));

        
        $__internal_29a7e357a44eca7801748407bf5ea1f0de3e37b076386ea5b89c12b7e2318785->leave($__internal_29a7e357a44eca7801748407bf5ea1f0de3e37b076386ea5b89c12b7e2318785_prof);

        
        $__internal_816b2da52cce9f93589e9a04e63003d0faf7ec507336ebeb1628bb3f66fec374->leave($__internal_816b2da52cce9f93589e9a04e63003d0faf7ec507336ebeb1628bb3f66fec374_prof);

    }

    // line 120
    public function block_translation_map_detail($context, array $blocks = array())
    {
        $__internal_9c7f6115dd7cf86bfd4faf56e991dc1bf48e297b71ebcc2cab4ea777cad8fae1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c7f6115dd7cf86bfd4faf56e991dc1bf48e297b71ebcc2cab4ea777cad8fae1->enter($__internal_9c7f6115dd7cf86bfd4faf56e991dc1bf48e297b71ebcc2cab4ea777cad8fae1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "translation_map_detail"));

        $__internal_c0afaf801360502eb29b4c41753372ea99d613c0779a139e24c8b1a67a9e80e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c0afaf801360502eb29b4c41753372ea99d613c0779a139e24c8b1a67a9e80e2->enter($__internal_c0afaf801360502eb29b4c41753372ea99d613c0779a139e24c8b1a67a9e80e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "translation_map_detail"));

        
        $__internal_c0afaf801360502eb29b4c41753372ea99d613c0779a139e24c8b1a67a9e80e2->leave($__internal_c0afaf801360502eb29b4c41753372ea99d613c0779a139e24c8b1a67a9e80e2_prof);

        
        $__internal_9c7f6115dd7cf86bfd4faf56e991dc1bf48e297b71ebcc2cab4ea777cad8fae1->leave($__internal_9c7f6115dd7cf86bfd4faf56e991dc1bf48e297b71ebcc2cab4ea777cad8fae1_prof);

    }

    // line 121
    public function block_translation_map_save($context, array $blocks = array())
    {
        $__internal_cb5fcf09304ca8b691082f61505ac3687605a2ecc20738f6d74366057ff3a7fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cb5fcf09304ca8b691082f61505ac3687605a2ecc20738f6d74366057ff3a7fa->enter($__internal_cb5fcf09304ca8b691082f61505ac3687605a2ecc20738f6d74366057ff3a7fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "translation_map_save"));

        $__internal_236bb2b0e7b760e9a144b1d47b523957f740d63633b18c052df4e9c1bb7df617 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_236bb2b0e7b760e9a144b1d47b523957f740d63633b18c052df4e9c1bb7df617->enter($__internal_236bb2b0e7b760e9a144b1d47b523957f740d63633b18c052df4e9c1bb7df617_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "translation_map_save"));

        
        $__internal_236bb2b0e7b760e9a144b1d47b523957f740d63633b18c052df4e9c1bb7df617->leave($__internal_236bb2b0e7b760e9a144b1d47b523957f740d63633b18c052df4e9c1bb7df617_prof);

        
        $__internal_cb5fcf09304ca8b691082f61505ac3687605a2ecc20738f6d74366057ff3a7fa->leave($__internal_cb5fcf09304ca8b691082f61505ac3687605a2ecc20738f6d74366057ff3a7fa_prof);

    }

    // line 122
    public function block_search_result($context, array $blocks = array())
    {
        $__internal_05d02a6bc22d729c878c1440851992aeaf535bca89510fa4facf1ecdd260fae4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_05d02a6bc22d729c878c1440851992aeaf535bca89510fa4facf1ecdd260fae4->enter($__internal_05d02a6bc22d729c878c1440851992aeaf535bca89510fa4facf1ecdd260fae4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "search_result"));

        $__internal_3f70ae0ec09653fa0571f914be0120741e93d61ff8f81339e09364ef2551902c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f70ae0ec09653fa0571f914be0120741e93d61ff8f81339e09364ef2551902c->enter($__internal_3f70ae0ec09653fa0571f914be0120741e93d61ff8f81339e09364ef2551902c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "search_result"));

        
        $__internal_3f70ae0ec09653fa0571f914be0120741e93d61ff8f81339e09364ef2551902c->leave($__internal_3f70ae0ec09653fa0571f914be0120741e93d61ff8f81339e09364ef2551902c_prof);

        
        $__internal_05d02a6bc22d729c878c1440851992aeaf535bca89510fa4facf1ecdd260fae4->leave($__internal_05d02a6bc22d729c878c1440851992aeaf535bca89510fa4facf1ecdd260fae4_prof);

    }

    // line 123
    public function block_scan($context, array $blocks = array())
    {
        $__internal_4ad6e7ee6020e093ba8463db1666034043c85db1d2f4726ec6b86d6f95d3ec3f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4ad6e7ee6020e093ba8463db1666034043c85db1d2f4726ec6b86d6f95d3ec3f->enter($__internal_4ad6e7ee6020e093ba8463db1666034043c85db1d2f4726ec6b86d6f95d3ec3f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "scan"));

        $__internal_a7d1df487ff27bb8fda595a5940b6a6a37daaa82bf57c8a16638cf482804860a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7d1df487ff27bb8fda595a5940b6a6a37daaa82bf57c8a16638cf482804860a->enter($__internal_a7d1df487ff27bb8fda595a5940b6a6a37daaa82bf57c8a16638cf482804860a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "scan"));

        
        $__internal_a7d1df487ff27bb8fda595a5940b6a6a37daaa82bf57c8a16638cf482804860a->leave($__internal_a7d1df487ff27bb8fda595a5940b6a6a37daaa82bf57c8a16638cf482804860a_prof);

        
        $__internal_4ad6e7ee6020e093ba8463db1666034043c85db1d2f4726ec6b86d6f95d3ec3f->leave($__internal_4ad6e7ee6020e093ba8463db1666034043c85db1d2f4726ec6b86d6f95d3ec3f_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  788 => 123,  771 => 122,  754 => 121,  737 => 120,  720 => 119,  703 => 118,  686 => 117,  669 => 116,  652 => 115,  635 => 114,  618 => 113,  601 => 112,  584 => 111,  567 => 110,  550 => 109,  533 => 108,  516 => 107,  499 => 106,  482 => 105,  465 => 104,  448 => 103,  431 => 102,  414 => 101,  397 => 100,  380 => 99,  363 => 98,  346 => 97,  329 => 6,  311 => 5,  248 => 124,  246 => 123,  244 => 122,  242 => 121,  240 => 120,  238 => 119,  236 => 118,  234 => 117,  232 => 116,  230 => 115,  228 => 114,  226 => 113,  224 => 112,  222 => 111,  220 => 110,  218 => 109,  216 => 108,  214 => 107,  212 => 106,  210 => 105,  208 => 104,  206 => 103,  204 => 102,  202 => 101,  200 => 100,  198 => 99,  196 => 98,  194 => 97,  187 => 92,  181 => 88,  173 => 83,  169 => 82,  164 => 80,  161 => 79,  108 => 28,  105 => 27,  101 => 25,  98 => 24,  96 => 23,  83 => 13,  79 => 12,  75 => 11,  71 => 10,  66 => 7,  64 => 6,  60 => 5,  54 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
    {% block stylesheets %}{% endblock %}
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>
    <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('bundles/css/style.css') }}\" />
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('bundles/css/login.css') }}\" />
    <link rel=\"stylesheet\" href=\"{{ asset('bundles/css/footer.css') }}\" />
    <link rel=\"stylesheet\" href=\"http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" />
</head>

<body>
    <div class=\"navbar navbar-inverse navbar-fixed-top\">
        <div class=\"container\">
            <div class=\"navbar-collapse collapse\">
                <ul class=\"nav navbar-nav\">
                    <li class=\"active\"><a href=\"/\">Home</a></li>
                        {% if is_granted('IS_AUTHENTICATED_FULLY') %}
                            {% if is_granted('ROLE_ADMIN') %}

                        {% endif %}
                        {% if is_granted('ROLE_CREATOR') %}
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Category<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/category/list\">Category List</a></li>
                                    <li><a href=\"/category/save\">Create New Category</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Service<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/service/list\">Service List</a></li>
                                    <li><a href=\"/service/save\">Create New Service</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Service Transaction<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/service_transaction/list\">Service Transaction List</a></li>
                                    <li><a href=\"/service_transaction/save\">Create New Service Transaction</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Payment Type<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/paymenttype/list\">Payment Type List</a></li>
                                    <li><a href=\"/paymenttype/save\">Create Payment Type</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Service Frequency<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/service_frequency/list\">Service Frequency List</a></li>
                                    <li><a href=\"/service_frequency/save\">Create Service Frequency</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Translation Map<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/translation_map/list\">Translation Map List</a></li>
                                    <li><a href=\"/translation_map/save\">Create Translation Map</a></li>
                                </ul>
                            </li>
                            <li class=\"dropdown\">
                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">Users<b class=\"caret\"></b></a>
                                <ul class=\"dropdown-menu\">
                                    <li><a href=\"/register\">Create New User</a></li>
                                    <li><a href=\"/userlist\">User List</a></li>
                                    <li><a href=\"/userlist_pdf\">User List In PDF</a></li>
                                </ul>
                            </li>
                        {% endif %}
                        <li class=\"dropdown\">
                            <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">&nbsp; <i class=\"fa fa-circle text-success\"></i> {{ app.user.username }}<b class=\"caret\"></b></a>
                            <ul class=\"dropdown-menu\">
                                <li><a href=\"{{path('user.edit', {id: app.user.id}) }}\">Profile</a></li>
                                <li><a href=\"{{ logout_path() }}\">Logout</a></li>
                            </ul>
                        </li>

                    {% else %}
                        <li><a href=\"/login\" >Sign in</a> </li>


                    {% endif %}
                </ul>
            </div>
        </div>
    </div>
    <div style=\"margin-top:40px;\">
    {% block body %}{% endblock %}
{% block javascripts %}{% endblock %}
{% block login %}{% endblock %}
{% block user_edit %}{% endblock %}
{% block register %}{% endblock %}
{% block page_content %}{% endblock %}
{% block service_list %}{% endblock %}
{% block service_detail %}{% endblock %}
{% block service_save %}{% endblock %}
{% block service_transaction_list %}{% endblock %}
{% block service_transaction_detail %}{% endblock %}
{% block service_transaction_save %}{% endblock %}
{% block user_list %}{% endblock %}
{% block payment_type_list %}{% endblock %}
{% block payment_type_detail %}{% endblock %}
{% block payment_type_save %}{% endblock %}
{% block service_frequency_list %}{% endblock %}
{% block service_frequency_detail %}{% endblock %}
{% block service_frequency_save %}{% endblock %}
{% block category_list %}{% endblock %}
{% block category_detail %}{% endblock %}
{% block category_save %}{% endblock %}
{% block translation_map_list %}{% endblock %}
{% block translation_map_detail %}{% endblock %}
{% block translation_map_save %}{% endblock %}
{% block search_result %}{% endblock %}
{% block scan %}{% endblock %}
<footer class=\"footer-distributed\">

    <div class=\"footer-left\">

        <h3>GTS Payment<span>System</span></h3>

        <p class=\"footer-links\">

        </p>

        <p class=\"footer-company-name\">Global Technology Solutiond &copy; 2018</p>
    </div>

    <div class=\"footer-center\">

        <div>
            <i class=\"fa fa-map-marker\"></i>
            <p><span>110 Toktogul Street</span> Bishkek, Kyrgyzstan</p>
        </div>

        <div>
            <i class=\"fa fa-phone\"></i>
            <p>Office: 996 312 881 982</p>
            <p>Mob.  : 996 551 442 488 </p>
        </div>

        <div>
            <i class=\"fa fa-envelope\"></i>
            <p><a href=\"mailto:office@glotechsol.com\">office@glotechsol.com</a></p>
        </div>

    </div>

    <div class=\"footer-right\">

        <p class=\"footer-company-about\">
            <span>About GTS Payment Management Admin Tool</span>
            For World!
        </p>

        <div class=\"footer-icons\">
            <a href=\"#\"><i class=\"fa fa-facebook\"></i></a>
            <a href=\"#\"><i class=\"fa fa-twitter\"></i></a>
            <a href=\"#\"><i class=\"fa fa-linkedin\"></i></a>
            <a href=\"#\"><i class=\"fa fa-github\"></i></a>

        </div>

    </div>

</footer>
</body>
</html>
", "base.html.twig", "/var/www/pat/app/Resources/views/base.html.twig");
    }
}

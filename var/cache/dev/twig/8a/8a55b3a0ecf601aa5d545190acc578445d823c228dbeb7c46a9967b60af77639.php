<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_dd4be7a243c3fddd8dbccc83b9052838be4946ffd57a0335a2eea3df9eba1200 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ed092a190d25e0113bfa9364365796eedb5cc0a9cc65b059b6f55536cb6d31af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ed092a190d25e0113bfa9364365796eedb5cc0a9cc65b059b6f55536cb6d31af->enter($__internal_ed092a190d25e0113bfa9364365796eedb5cc0a9cc65b059b6f55536cb6d31af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_be80f9b87f2a4760608d1bb3742d2ae22296950a0aadd0d29df3a7b8f517df3b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be80f9b87f2a4760608d1bb3742d2ae22296950a0aadd0d29df3a7b8f517df3b->enter($__internal_be80f9b87f2a4760608d1bb3742d2ae22296950a0aadd0d29df3a7b8f517df3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ed092a190d25e0113bfa9364365796eedb5cc0a9cc65b059b6f55536cb6d31af->leave($__internal_ed092a190d25e0113bfa9364365796eedb5cc0a9cc65b059b6f55536cb6d31af_prof);

        
        $__internal_be80f9b87f2a4760608d1bb3742d2ae22296950a0aadd0d29df3a7b8f517df3b->leave($__internal_be80f9b87f2a4760608d1bb3742d2ae22296950a0aadd0d29df3a7b8f517df3b_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_212630bdbcae97fa5263347d869a1c7b4e680938cd96fcae282c7aa5c58195d4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_212630bdbcae97fa5263347d869a1c7b4e680938cd96fcae282c7aa5c58195d4->enter($__internal_212630bdbcae97fa5263347d869a1c7b4e680938cd96fcae282c7aa5c58195d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_5140e96c05ae3ac8091f263fd203e3bd229dbcb513d1e7bd82bf0be7a5860235 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5140e96c05ae3ac8091f263fd203e3bd229dbcb513d1e7bd82bf0be7a5860235->enter($__internal_5140e96c05ae3ac8091f263fd203e3bd229dbcb513d1e7bd82bf0be7a5860235_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_5140e96c05ae3ac8091f263fd203e3bd229dbcb513d1e7bd82bf0be7a5860235->leave($__internal_5140e96c05ae3ac8091f263fd203e3bd229dbcb513d1e7bd82bf0be7a5860235_prof);

        
        $__internal_212630bdbcae97fa5263347d869a1c7b4e680938cd96fcae282c7aa5c58195d4->leave($__internal_212630bdbcae97fa5263347d869a1c7b4e680938cd96fcae282c7aa5c58195d4_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_9ad01ba504d1a9f0e2a31c105f1ff4e8309c34c0368e81263600ef8844e8008c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ad01ba504d1a9f0e2a31c105f1ff4e8309c34c0368e81263600ef8844e8008c->enter($__internal_9ad01ba504d1a9f0e2a31c105f1ff4e8309c34c0368e81263600ef8844e8008c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_33670114a5f5adedc84e6c006079125d62d2c7fd4818451c2ca79b206c2929b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33670114a5f5adedc84e6c006079125d62d2c7fd4818451c2ca79b206c2929b7->enter($__internal_33670114a5f5adedc84e6c006079125d62d2c7fd4818451c2ca79b206c2929b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo (($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if ($this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_33670114a5f5adedc84e6c006079125d62d2c7fd4818451c2ca79b206c2929b7->leave($__internal_33670114a5f5adedc84e6c006079125d62d2c7fd4818451c2ca79b206c2929b7_prof);

        
        $__internal_9ad01ba504d1a9f0e2a31c105f1ff4e8309c34c0368e81263600ef8844e8008c->leave($__internal_9ad01ba504d1a9f0e2a31c105f1ff4e8309c34c0368e81263600ef8844e8008c_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_d3e76e516163e6cd76b2e2131f38c5cfa7937f530494c905b551eb6362f469e1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d3e76e516163e6cd76b2e2131f38c5cfa7937f530494c905b551eb6362f469e1->enter($__internal_d3e76e516163e6cd76b2e2131f38c5cfa7937f530494c905b551eb6362f469e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_7c3fc42c5fbeeade23d4bbc929f7ae0e652089ec1ff530d9f1b53906f9c600d8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c3fc42c5fbeeade23d4bbc929f7ae0e652089ec1ff530d9f1b53906f9c600d8->enter($__internal_7c3fc42c5fbeeade23d4bbc929f7ae0e652089ec1ff530d9f1b53906f9c600d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !$this->getAttribute(($context["collector"] ?? $this->getContext($context, "collector")), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => ($context["token"] ?? $this->getContext($context, "token")))));
            echo "
        </div>
    ";
        }
        
        $__internal_7c3fc42c5fbeeade23d4bbc929f7ae0e652089ec1ff530d9f1b53906f9c600d8->leave($__internal_7c3fc42c5fbeeade23d4bbc929f7ae0e652089ec1ff530d9f1b53906f9c600d8_prof);

        
        $__internal_d3e76e516163e6cd76b2e2131f38c5cfa7937f530494c905b551eb6362f469e1->leave($__internal_d3e76e516163e6cd76b2e2131f38c5cfa7937f530494c905b551eb6362f469e1_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/var/www/pat/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}

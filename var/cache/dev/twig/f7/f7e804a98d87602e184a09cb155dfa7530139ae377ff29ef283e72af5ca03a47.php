<?php

/* progress_bar.html.twig */
class __TwigTemplate_04b94222cc2b729cac00bd9d2869e6a0caa180c4a9f9ea36f15f75f93a64efbe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_44e2b295597176e397ff9def77f3997d90e2b72ff98694ecd3b8558c8b37fc9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44e2b295597176e397ff9def77f3997d90e2b72ff98694ecd3b8558c8b37fc9e->enter($__internal_44e2b295597176e397ff9def77f3997d90e2b72ff98694ecd3b8558c8b37fc9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "progress_bar.html.twig"));

        $__internal_5501c754e9ff91158cd2401aa62c7362c4f20c0d7aa9cf494d892e83d1600590 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5501c754e9ff91158cd2401aa62c7362c4f20c0d7aa9cf494d892e83d1600590->enter($__internal_5501c754e9ff91158cd2401aa62c7362c4f20c0d7aa9cf494d892e83d1600590_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "progress_bar.html.twig"));

        // line 1
        echo " <div class=\"progress\" syle=\"display:none;\">
  <div class=\"progress-bar progress-bar-striped active\" role=\"progressbar\"
  aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:100%\">
    
  </div>
</div>

<div class=\"ajax_loader_main\">
<div class=\"ajax_loader\"></div>
</div>
<style>
html, body
{
    height: 100%;
}
.ajax_loader_main {
  background-color:#222;
  width: 100%;
  height:auto;
  min-height:100%;
  position:absolute;
  opacity:0.5;
  display:none;
  z-index: 10000;
}
.ajax_loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 80px;
  height: 80px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
  position:absolute;
  top:50%;
  left:45%;

}
</style>
 
";
        
        $__internal_44e2b295597176e397ff9def77f3997d90e2b72ff98694ecd3b8558c8b37fc9e->leave($__internal_44e2b295597176e397ff9def77f3997d90e2b72ff98694ecd3b8558c8b37fc9e_prof);

        
        $__internal_5501c754e9ff91158cd2401aa62c7362c4f20c0d7aa9cf494d892e83d1600590->leave($__internal_5501c754e9ff91158cd2401aa62c7362c4f20c0d7aa9cf494d892e83d1600590_prof);

    }

    public function getTemplateName()
    {
        return "progress_bar.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source(" <div class=\"progress\" syle=\"display:none;\">
  <div class=\"progress-bar progress-bar-striped active\" role=\"progressbar\"
  aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:100%\">
    
  </div>
</div>

<div class=\"ajax_loader_main\">
<div class=\"ajax_loader\"></div>
</div>
<style>
html, body
{
    height: 100%;
}
.ajax_loader_main {
  background-color:#222;
  width: 100%;
  height:auto;
  min-height:100%;
  position:absolute;
  opacity:0.5;
  display:none;
  z-index: 10000;
}
.ajax_loader {
  border: 16px solid #f3f3f3;
  border-radius: 50%;
  border-top: 16px solid #3498db;
  width: 80px;
  height: 80px;
  -webkit-animation: spin 2s linear infinite; /* Safari */
  animation: spin 2s linear infinite;
  position:absolute;
  top:50%;
  left:45%;

}
</style>
 
", "progress_bar.html.twig", "/var/www/pat/app/Resources/views/progress_bar.html.twig");
    }
}

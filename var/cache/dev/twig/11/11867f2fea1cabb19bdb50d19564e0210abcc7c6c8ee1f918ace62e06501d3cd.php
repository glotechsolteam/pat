<?php

/* security/login.html.twig */
class __TwigTemplate_def8112e0fe3984799e78944c86794f1461a6db9076a4d89d04e011ceb9b1141 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "security/login.html.twig", 1);
        $this->blocks = array(
            'login' => array($this, 'block_login'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_11f642df80a7a3a72df0ff8112f09c8b8b8a13199c7f0d474038f129a0ba1fc9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_11f642df80a7a3a72df0ff8112f09c8b8b8a13199c7f0d474038f129a0ba1fc9->enter($__internal_11f642df80a7a3a72df0ff8112f09c8b8b8a13199c7f0d474038f129a0ba1fc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $__internal_67419cacd50190a84899dbd305e5199c691fb4aea6cc1aa22b867f1a16a01716 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_67419cacd50190a84899dbd305e5199c691fb4aea6cc1aa22b867f1a16a01716->enter($__internal_67419cacd50190a84899dbd305e5199c691fb4aea6cc1aa22b867f1a16a01716_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_11f642df80a7a3a72df0ff8112f09c8b8b8a13199c7f0d474038f129a0ba1fc9->leave($__internal_11f642df80a7a3a72df0ff8112f09c8b8b8a13199c7f0d474038f129a0ba1fc9_prof);

        
        $__internal_67419cacd50190a84899dbd305e5199c691fb4aea6cc1aa22b867f1a16a01716->leave($__internal_67419cacd50190a84899dbd305e5199c691fb4aea6cc1aa22b867f1a16a01716_prof);

    }

    // line 3
    public function block_login($context, array $blocks = array())
    {
        $__internal_9a8212d69991d6d1d3276be61020116f3be7b9f253c338eea721af79ecd78c05 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9a8212d69991d6d1d3276be61020116f3be7b9f253c338eea721af79ecd78c05->enter($__internal_9a8212d69991d6d1d3276be61020116f3be7b9f253c338eea721af79ecd78c05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "login"));

        $__internal_4eb1d3e22c364f9ff99159f8272f1c1083a19842560e086811c36b318a04a889 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4eb1d3e22c364f9ff99159f8272f1c1083a19842560e086811c36b318a04a889->enter($__internal_4eb1d3e22c364f9ff99159f8272f1c1083a19842560e086811c36b318a04a889_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "login"));

        // line 4
        echo "    <script type=\"text/javascript\" src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/js/delete.js"), "html", null, true);
        echo "\"></script>

    <div class=\"container\">
        <div class=\"card card-container\" style=\"margin:100px auto;\">
            <!-- <img class=\"profile-img-card\" src=\"//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120\" alt=\"\" /> -->
            <img id=\"profile-img\" class=\"profile-img-card\" src=\"//ssl.gstatic.com/accounts/ui/avatar_2x.png\" />
            <p id=\"profile-name\" class=\"profile-name-card\"></p>
            <form class=\"form-signin\" action=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">
                <span id=\"reauth-email\" class=\"reauth-email\"></span>
                <input id=\"username\" name=\"_username\" class=\"form-control\" placeholder=\"User or Email address\" required autofocus>
                <input type=\"password\" id=\"password\" name=\"_password\" class=\"form-control\" placeholder=\"Password\" required>
                <div id=\"remember\" class=\"checkbox\">
                    <label>
                        <input type=\"checkbox\" value=\"remember-me\"> Remember me
                    </label>
                </div>
                <button class=\"btn btn-lg btn-primary btn-block btn-signin\" type=\"submit\">Sign in</button>
            </form><!-- /form -->
            <a href=\"#\" class=\"forgot-password\">
                Forgot the password?
            </a><br/>
            ";
        // line 25
        if (($context["error"] ?? $this->getContext($context, "error"))) {
            // line 26
            echo "                <span class=\"text-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute(($context["error"] ?? $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</span>
            ";
        }
        // line 28
        echo "        </div><!-- /card-container -->
    </div><!-- /container -->

";
        
        $__internal_4eb1d3e22c364f9ff99159f8272f1c1083a19842560e086811c36b318a04a889->leave($__internal_4eb1d3e22c364f9ff99159f8272f1c1083a19842560e086811c36b318a04a889_prof);

        
        $__internal_9a8212d69991d6d1d3276be61020116f3be7b9f253c338eea721af79ecd78c05->leave($__internal_9a8212d69991d6d1d3276be61020116f3be7b9f253c338eea721af79ecd78c05_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 28,  79 => 26,  77 => 25,  60 => 11,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block login %}
    <script type=\"text/javascript\" src=\"{{ asset('bundles/js/delete.js') }}\"></script>

    <div class=\"container\">
        <div class=\"card card-container\" style=\"margin:100px auto;\">
            <!-- <img class=\"profile-img-card\" src=\"//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120\" alt=\"\" /> -->
            <img id=\"profile-img\" class=\"profile-img-card\" src=\"//ssl.gstatic.com/accounts/ui/avatar_2x.png\" />
            <p id=\"profile-name\" class=\"profile-name-card\"></p>
            <form class=\"form-signin\" action=\"{{ path('login') }}\" method=\"post\">
                <span id=\"reauth-email\" class=\"reauth-email\"></span>
                <input id=\"username\" name=\"_username\" class=\"form-control\" placeholder=\"User or Email address\" required autofocus>
                <input type=\"password\" id=\"password\" name=\"_password\" class=\"form-control\" placeholder=\"Password\" required>
                <div id=\"remember\" class=\"checkbox\">
                    <label>
                        <input type=\"checkbox\" value=\"remember-me\"> Remember me
                    </label>
                </div>
                <button class=\"btn btn-lg btn-primary btn-block btn-signin\" type=\"submit\">Sign in</button>
            </form><!-- /form -->
            <a href=\"#\" class=\"forgot-password\">
                Forgot the password?
            </a><br/>
            {% if error %}
                <span class=\"text-danger\">{{ error.messageKey|trans(error.messageData, 'security') }}</span>
            {% endif %}
        </div><!-- /card-container -->
    </div><!-- /container -->

{% endblock %}
", "security/login.html.twig", "/var/www/pat/app/Resources/views/security/login.html.twig");
    }
}

<?php

/* payment_type/scan.html.twig */
class __TwigTemplate_bf995ced2a754b1b726807d8df891434d75fa2457b53b0b86dd8e212c4e98938 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "payment_type/scan.html.twig", 1);
        $this->blocks = array(
            'scan' => array($this, 'block_scan'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_42d4563e2802f67612a12d17806674b9b09efccaa38ca3b1397a413c7e222091 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_42d4563e2802f67612a12d17806674b9b09efccaa38ca3b1397a413c7e222091->enter($__internal_42d4563e2802f67612a12d17806674b9b09efccaa38ca3b1397a413c7e222091_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "payment_type/scan.html.twig"));

        $__internal_6037cde0c53d8a756ebc5dd9c48c91a2f83dfd803278898257710ba6c5a3a369 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6037cde0c53d8a756ebc5dd9c48c91a2f83dfd803278898257710ba6c5a3a369->enter($__internal_6037cde0c53d8a756ebc5dd9c48c91a2f83dfd803278898257710ba6c5a3a369_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "payment_type/scan.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_42d4563e2802f67612a12d17806674b9b09efccaa38ca3b1397a413c7e222091->leave($__internal_42d4563e2802f67612a12d17806674b9b09efccaa38ca3b1397a413c7e222091_prof);

        
        $__internal_6037cde0c53d8a756ebc5dd9c48c91a2f83dfd803278898257710ba6c5a3a369->leave($__internal_6037cde0c53d8a756ebc5dd9c48c91a2f83dfd803278898257710ba6c5a3a369_prof);

    }

    // line 3
    public function block_scan($context, array $blocks = array())
    {
        $__internal_1af7697bd7dbed2aea7d2c75c33a45180849f3e3f507718abe384291e875498b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1af7697bd7dbed2aea7d2c75c33a45180849f3e3f507718abe384291e875498b->enter($__internal_1af7697bd7dbed2aea7d2c75c33a45180849f3e3f507718abe384291e875498b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "scan"));

        $__internal_c3ead24b3eb08da8b8d1d2f922f6e4ddf0fb0a3bd41ae71cee28c7265beb8622 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c3ead24b3eb08da8b8d1d2f922f6e4ddf0fb0a3bd41ae71cee28c7265beb8622->enter($__internal_c3ead24b3eb08da8b8d1d2f922f6e4ddf0fb0a3bd41ae71cee28c7265beb8622_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "scan"));

        // line 4
        echo "

    <div style=\"min-height: 300px; padding:50px;\">
        ";
        // line 7
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start', array("attr" => array("id" => "form_payment_report")));
        echo "
           <!-- <div id=\"reportView\" class=\"col-xs-12\"></div> -->
        <label for=\"formGroupExampleInput\">Sacan QR Code</label>
        <input type=\"text\" class=\"form-control\" name=\"account_number\" id=\"account_number\" autofocus />
        ";
        // line 11
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
        <h1>Participant Name: ";
        // line 12
        echo twig_escape_filter($this->env, ($context["firstname"] ?? $this->getContext($context, "firstname")), "html", null, true);
        echo "</h1>
        <h1>Account Number: ";
        // line 13
        echo twig_escape_filter($this->env, ($context["account_number"] ?? $this->getContext($context, "account_number")), "html", null, true);
        echo "</h1>
        ";
        // line 14
        if ((($context["scan_count"] ?? $this->getContext($context, "scan_count")) == 0)) {
            // line 15
            echo "            <h1>Message: <font size=\"9\" color=\"red\">";
            echo twig_escape_filter($this->env, ($context["message"] ?? $this->getContext($context, "message")), "html", null, true);
            echo "</font></h1>
        ";
        } else {
            // line 17
            echo "            <h1>Message: ";
            echo twig_escape_filter($this->env, ($context["message"] ?? $this->getContext($context, "message")), "html", null, true);
            echo "</h1>
        ";
        }
        // line 19
        echo "        <h1>Scan Count: ";
        echo twig_escape_filter($this->env, ($context["scan_count"] ?? $this->getContext($context, "scan_count")), "html", null, true);
        echo "</h1>
    </div>



";
        
        $__internal_c3ead24b3eb08da8b8d1d2f922f6e4ddf0fb0a3bd41ae71cee28c7265beb8622->leave($__internal_c3ead24b3eb08da8b8d1d2f922f6e4ddf0fb0a3bd41ae71cee28c7265beb8622_prof);

        
        $__internal_1af7697bd7dbed2aea7d2c75c33a45180849f3e3f507718abe384291e875498b->leave($__internal_1af7697bd7dbed2aea7d2c75c33a45180849f3e3f507718abe384291e875498b_prof);

    }

    public function getTemplateName()
    {
        return "payment_type/scan.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 19,  81 => 17,  75 => 15,  73 => 14,  69 => 13,  65 => 12,  61 => 11,  54 => 7,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block scan %}


    <div style=\"min-height: 300px; padding:50px;\">
        {{ form_start(form, {'attr': {'id':'form_payment_report'} }) }}
           <!-- <div id=\"reportView\" class=\"col-xs-12\"></div> -->
        <label for=\"formGroupExampleInput\">Sacan QR Code</label>
        <input type=\"text\" class=\"form-control\" name=\"account_number\" id=\"account_number\" autofocus />
        {{ form_end(form) }}
        <h1>Participant Name: {{ firstname }}</h1>
        <h1>Account Number: {{ account_number }}</h1>
        {% if scan_count == 0 %}
            <h1>Message: <font size=\"9\" color=\"red\">{{ message }}</font></h1>
        {% else %}
            <h1>Message: {{ message }}</h1>
        {% endif %}
        <h1>Scan Count: {{ scan_count }}</h1>
    </div>



{% endblock %}", "payment_type/scan.html.twig", "/var/www/pat/app/Resources/views/payment_type/scan.html.twig");
    }
}

<?php

/* search/result.html.twig */
class __TwigTemplate_69bd037bdfc1beca56887a179a5dcdaaac5145ac4caa7ffb9aadd987f0207110 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "search/result.html.twig", 1);
        $this->blocks = array(
            'search_result' => array($this, 'block_search_result'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca7982910edf2f11b77864a99f801082cb6ccecb4a505673ecab2770a40beeb5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ca7982910edf2f11b77864a99f801082cb6ccecb4a505673ecab2770a40beeb5->enter($__internal_ca7982910edf2f11b77864a99f801082cb6ccecb4a505673ecab2770a40beeb5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "search/result.html.twig"));

        $__internal_10fcc0a8100458d3989349efa06d8d219d65efe951e9263b2ac9e17925b65a37 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10fcc0a8100458d3989349efa06d8d219d65efe951e9263b2ac9e17925b65a37->enter($__internal_10fcc0a8100458d3989349efa06d8d219d65efe951e9263b2ac9e17925b65a37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "search/result.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ca7982910edf2f11b77864a99f801082cb6ccecb4a505673ecab2770a40beeb5->leave($__internal_ca7982910edf2f11b77864a99f801082cb6ccecb4a505673ecab2770a40beeb5_prof);

        
        $__internal_10fcc0a8100458d3989349efa06d8d219d65efe951e9263b2ac9e17925b65a37->leave($__internal_10fcc0a8100458d3989349efa06d8d219d65efe951e9263b2ac9e17925b65a37_prof);

    }

    // line 3
    public function block_search_result($context, array $blocks = array())
    {
        $__internal_1bf05147423680dacf865a128fc13171ddcbc870c0a8c9f18d4d759d48618cc5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1bf05147423680dacf865a128fc13171ddcbc870c0a8c9f18d4d759d48618cc5->enter($__internal_1bf05147423680dacf865a128fc13171ddcbc870c0a8c9f18d4d759d48618cc5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "search_result"));

        $__internal_5b917eed67f0cac64fc0bd31e9b14dc37790da6c369cf81fbfe3044e5ae39937 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5b917eed67f0cac64fc0bd31e9b14dc37790da6c369cf81fbfe3044e5ae39937->enter($__internal_5b917eed67f0cac64fc0bd31e9b14dc37790da6c369cf81fbfe3044e5ae39937_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "search_result"));

        // line 4
        echo "    <style>
        .paginator {
            margin-left: 10px;
        }
    </style>
    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" rel=\"stylesheet\"/>
    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css\" rel=\"stylesheet\"/>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1>Account Number: ";
        // line 13
        echo twig_escape_filter($this->env, ($context["account_number"] ?? $this->getContext($context, "account_number")), "html", null, true);
        echo "</h1>
            ";
        // line 14
        if ((($context["scan_count"] ?? $this->getContext($context, "scan_count")) == 0)) {
            // line 15
            echo "                <h1>Message: <font size=\"9\" color=\"red\">";
            echo twig_escape_filter($this->env, ($context["message"] ?? $this->getContext($context, "message")), "html", null, true);
            echo "</font></h1>
            ";
        } else {
            // line 17
            echo "                <h1>Message: ";
            echo twig_escape_filter($this->env, ($context["message"] ?? $this->getContext($context, "message")), "html", null, true);
            echo "</h1>
            ";
        }
        // line 19
        echo "            <h1>Scan Count: ";
        echo twig_escape_filter($this->env, ($context["scan_count"] ?? $this->getContext($context, "scan_count")), "html", null, true);
        echo "</h1>
        </div>
    </div>
";
        
        $__internal_5b917eed67f0cac64fc0bd31e9b14dc37790da6c369cf81fbfe3044e5ae39937->leave($__internal_5b917eed67f0cac64fc0bd31e9b14dc37790da6c369cf81fbfe3044e5ae39937_prof);

        
        $__internal_1bf05147423680dacf865a128fc13171ddcbc870c0a8c9f18d4d759d48618cc5->leave($__internal_1bf05147423680dacf865a128fc13171ddcbc870c0a8c9f18d4d759d48618cc5_prof);

    }

    public function getTemplateName()
    {
        return "search/result.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 19,  72 => 17,  66 => 15,  64 => 14,  60 => 13,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block search_result %}
    <style>
        .paginator {
            margin-left: 10px;
        }
    </style>
    <link href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" rel=\"stylesheet\"/>
    <link href=\"https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css\" rel=\"stylesheet\"/>
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1>Account Number: {{ account_number }}</h1>
            {% if scan_count == 0 %}
                <h1>Message: <font size=\"9\" color=\"red\">{{ message }}</font></h1>
            {% else %}
                <h1>Message: {{ message }}</h1>
            {% endif %}
            <h1>Scan Count: {{ scan_count }}</h1>
        </div>
    </div>
{% endblock %}


", "search/result.html.twig", "/var/www/pat/app/Resources/views/search/result.html.twig");
    }
}

<?php

/* service_transaction/save.html.twig */
class __TwigTemplate_182d357237b508381a3c0d5a21cddc75cea9578d9b8e342f9d6b3ff90b00957e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "service_transaction/save.html.twig", 1);
        $this->blocks = array(
            'service_transaction_save' => array($this, 'block_service_transaction_save'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ff6ae9614cfd78be8480d3be1f0e8624140c98751be89896ceec3b4082ef2397 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ff6ae9614cfd78be8480d3be1f0e8624140c98751be89896ceec3b4082ef2397->enter($__internal_ff6ae9614cfd78be8480d3be1f0e8624140c98751be89896ceec3b4082ef2397_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "service_transaction/save.html.twig"));

        $__internal_15e6d8742328e3e60d05b4215f53b9fc04a3220f268872377b6158fff80d392f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_15e6d8742328e3e60d05b4215f53b9fc04a3220f268872377b6158fff80d392f->enter($__internal_15e6d8742328e3e60d05b4215f53b9fc04a3220f268872377b6158fff80d392f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "service_transaction/save.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ff6ae9614cfd78be8480d3be1f0e8624140c98751be89896ceec3b4082ef2397->leave($__internal_ff6ae9614cfd78be8480d3be1f0e8624140c98751be89896ceec3b4082ef2397_prof);

        
        $__internal_15e6d8742328e3e60d05b4215f53b9fc04a3220f268872377b6158fff80d392f->leave($__internal_15e6d8742328e3e60d05b4215f53b9fc04a3220f268872377b6158fff80d392f_prof);

    }

    // line 3
    public function block_service_transaction_save($context, array $blocks = array())
    {
        $__internal_cf9d82649686eeec772b4469f7abaa063f7b853bbe22e4e72807f6fdb39ae9cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf9d82649686eeec772b4469f7abaa063f7b853bbe22e4e72807f6fdb39ae9cf->enter($__internal_cf9d82649686eeec772b4469f7abaa063f7b853bbe22e4e72807f6fdb39ae9cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_transaction_save"));

        $__internal_2147a5494551fefe4610b27a36e1e0086e987f04248351bbc51d215cade58dd5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2147a5494551fefe4610b27a36e1e0086e987f04248351bbc51d215cade58dd5->enter($__internal_2147a5494551fefe4610b27a36e1e0086e987f04248351bbc51d215cade58dd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "service_transaction_save"));

        // line 4
        echo "
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1>Create New Service Transaction</h1>
            ";
        // line 8
        echo twig_include($this->env, $context, "result_message.html.twig");
        echo "
            ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_start');
        echo "
            <table class=\"table\">
                <tr>
                    <th>Account Number</th>
                    <th>Paid</th>
                    <th>Status</th>
                    <th>Paid Date</th>
                    <th>Payment Type</th>
                    <th>Service</th>
                    <th>user</th>

                    <th>Option</th>
                </tr>
                <tr>
                    <td>";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "account_number", array()), 'widget');
        echo "
                    <td>";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "paid", array()), 'widget');
        echo "
                    <td>";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "status", array()), 'widget');
        echo "
                    <td>";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "paid_date", array()), 'widget');
        echo "
                    <td>";
        // line 27
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "payment_type", array()), 'widget');
        echo "
                    <td>";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "service", array()), 'widget');
        echo "
                    <td>";
        // line 29
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "user", array()), 'widget');
        echo "
                    <td>";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form"] ?? $this->getContext($context, "form")), "save", array()), 'widget', array("attr" => array("class" => "btn btn-success")));
        echo "
                </tr>
            </table>
            ";
        // line 33
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? $this->getContext($context, "form")), 'form_end');
        echo "
        </div>
    </div> 
";
        
        $__internal_2147a5494551fefe4610b27a36e1e0086e987f04248351bbc51d215cade58dd5->leave($__internal_2147a5494551fefe4610b27a36e1e0086e987f04248351bbc51d215cade58dd5_prof);

        
        $__internal_cf9d82649686eeec772b4469f7abaa063f7b853bbe22e4e72807f6fdb39ae9cf->leave($__internal_cf9d82649686eeec772b4469f7abaa063f7b853bbe22e4e72807f6fdb39ae9cf_prof);

    }

    public function getTemplateName()
    {
        return "service_transaction/save.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 33,  104 => 30,  100 => 29,  96 => 28,  92 => 27,  88 => 26,  84 => 25,  80 => 24,  76 => 23,  59 => 9,  55 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block service_transaction_save %}

    <div class=\"row\">
        <div class=\"col-md-12\">
            <h1>Create New Service Transaction</h1>
            {{ include('result_message.html.twig') }}
            {{ form_start(form) }}
            <table class=\"table\">
                <tr>
                    <th>Account Number</th>
                    <th>Paid</th>
                    <th>Status</th>
                    <th>Paid Date</th>
                    <th>Payment Type</th>
                    <th>Service</th>
                    <th>user</th>

                    <th>Option</th>
                </tr>
                <tr>
                    <td>{{ form_widget(form.account_number) }}
                    <td>{{ form_widget(form.paid) }}
                    <td>{{ form_widget(form.status) }}
                    <td>{{ form_widget(form.paid_date) }}
                    <td>{{ form_widget(form.payment_type) }}
                    <td>{{ form_widget(form.service) }}
                    <td>{{ form_widget(form.user) }}
                    <td>{{ form_widget(form.save, { 'attr' : { 'class': 'btn btn-success' }}) }}
                </tr>
            </table>
            {{ form_end(form) }}
        </div>
    </div> 
{% endblock %}
", "service_transaction/save.html.twig", "/var/www/pat/app/Resources/views/service_transaction/save.html.twig");
    }
}

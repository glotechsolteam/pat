<?php

/* login/editUser.html.twig */
class __TwigTemplate_3b41fa185a49c37962ef1674d81c6e3bfddb7460452b41b466af867311313843 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "login/editUser.html.twig", 1);
        $this->blocks = array(
            'user_edit' => array($this, 'block_user_edit'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_829c6fa05fd8a4e4a0b39155c00469eb5e03a48eef79a36d49ef3a6da150b217 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_829c6fa05fd8a4e4a0b39155c00469eb5e03a48eef79a36d49ef3a6da150b217->enter($__internal_829c6fa05fd8a4e4a0b39155c00469eb5e03a48eef79a36d49ef3a6da150b217_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "login/editUser.html.twig"));

        $__internal_1fabeee688ce8b98e607ce26d2147868b70fa763cef9cb6b5d10227c8ab527e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1fabeee688ce8b98e607ce26d2147868b70fa763cef9cb6b5d10227c8ab527e4->enter($__internal_1fabeee688ce8b98e607ce26d2147868b70fa763cef9cb6b5d10227c8ab527e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "login/editUser.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_829c6fa05fd8a4e4a0b39155c00469eb5e03a48eef79a36d49ef3a6da150b217->leave($__internal_829c6fa05fd8a4e4a0b39155c00469eb5e03a48eef79a36d49ef3a6da150b217_prof);

        
        $__internal_1fabeee688ce8b98e607ce26d2147868b70fa763cef9cb6b5d10227c8ab527e4->leave($__internal_1fabeee688ce8b98e607ce26d2147868b70fa763cef9cb6b5d10227c8ab527e4_prof);

    }

    // line 3
    public function block_user_edit($context, array $blocks = array())
    {
        $__internal_668c49cd0f26cd2d708be5b9263373a3a9d86e9940209e43c03fc68673392fbc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_668c49cd0f26cd2d708be5b9263373a3a9d86e9940209e43c03fc68673392fbc->enter($__internal_668c49cd0f26cd2d708be5b9263373a3a9d86e9940209e43c03fc68673392fbc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_edit"));

        $__internal_a08c17e70316a7fb6869fe7984d04d70ce17fd34fadbeea8c29084621552a49d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a08c17e70316a7fb6869fe7984d04d70ce17fd34fadbeea8c29084621552a49d->enter($__internal_a08c17e70316a7fb6869fe7984d04d70ce17fd34fadbeea8c29084621552a49d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "user_edit"));

        // line 4
        echo "<div class=\"container\" style=\"margin-top: 100px;\">
    <div class=\"row centered-form\">
        <div class=\"col-xs-12 col-sm-6\" >
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Edit Profile ";
        // line 9
        echo twig_escape_filter($this->env, $this->getAttribute(($context["user"] ?? $this->getContext($context, "user")), "username", array()), "html", null, true);
        echo "</h3>
                </div>
                <div id=\"exTab1\">
                    <ul  class=\"nav nav-tabs\">
                        <li class=\"active\">
                            <a  href=\"#1a\" data-toggle=\"tab\">General</a>
                        </li>
                        <li><a href=\"#2a\" data-toggle=\"tab\">Change Password</a>
                        </li>
                    </ul>

                    <div class=\"tab-content clearfix\">
                        <div class=\"tab-pane active\" id=\"1a\">
                            <div class=\"panel-body\">
                                ";
        // line 23
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form_profile_noPass"] ?? $this->getContext($context, "form_profile_noPass")), 'form_start');
        echo "
                                <div class=\"form-group\">
                                    <label>First Name</label>
                                    ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form_profile_noPass"] ?? $this->getContext($context, "form_profile_noPass")), "firstname", array()), 'widget', array("attr" => array("class" => "form-control input-sm")));
        echo "
                                </div>
                                <div class=\"form-group\">
                                    <label>Last Name</label>
                                    ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form_profile_noPass"] ?? $this->getContext($context, "form_profile_noPass")), "lastname", array()), 'widget', array("attr" => array("class" => "form-control input-sm")));
        echo "
                                </div>
                                <div class=\"form-group\">
                                    <label>Email</label>
                                    ";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form_profile_noPass"] ?? $this->getContext($context, "form_profile_noPass")), "email", array()), 'widget', array("attr" => array("class" => "form-control input-sm")));
        echo "
                                </div>
                                <div class=\"form-group\">
                                    <label>Phone Number</label>
                                    ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form_profile_noPass"] ?? $this->getContext($context, "form_profile_noPass")), "phone", array()), 'widget', array("attr" => array("class" => "form-control input-sm")));
        echo "
                                </div>
                                <div class=\"form-group\">
                                    <label>User Name</label>
                                    ";
        // line 42
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form_profile_noPass"] ?? $this->getContext($context, "form_profile_noPass")), "username", array()), 'widget', array("attr" => array("class" => "form-control input-sm")));
        echo "
                                </div>


                                <div class=\"row\">
                                    <div class=\"col-xs-6 col-sm-6 col-md-6\">
                                        <div class=\"form-group\">
                                            <label>Enable</label>
                                            ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form_profile_noPass"] ?? $this->getContext($context, "form_profile_noPass")), "isactive", array()), 'widget', array("attr" => array("class" => "")));
        echo "
                                        </div>
                                    </div>
                                    <div class=\"col-xs-6 col-sm-6 col-md-6\">
                                        <div class=\"form-group\">
                                            <label>Roles</label>
                                            ";
        // line 56
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form_profile_noPass"] ?? $this->getContext($context, "form_profile_noPass")), "roles", array()), 'widget', array("attr" => array("class" => "")));
        echo "
                                        </div>
                                    </div>
                                </div>

                                <div class=\"form-group\">
                                    <img class=\"img-circle img-bordered-primary\" src=\"";
        // line 62
        echo twig_escape_filter($this->env, ($context["user_image_path"] ?? $this->getContext($context, "user_image_path")), "html", null, true);
        echo twig_escape_filter($this->env, ($context["img_link"] ?? $this->getContext($context, "img_link")), "html", null, true);
        echo "\" width=\"120\" />
                                    ";
        // line 63
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form_profile_noPass"] ?? $this->getContext($context, "form_profile_noPass")), "file", array()), 'widget', array("attr" => array("class" => "")));
        echo "
                                </div>


                                ";
        // line 67
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form_profile_noPass"] ?? $this->getContext($context, "form_profile_noPass")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info btn-block")));
        echo "
                                ";
        // line 68
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form_profile_noPass"] ?? $this->getContext($context, "form_profile_noPass")), 'form_end');
        echo "

                            </div>
                        </div>
                        <div class=\"tab-pane\" id=\"2a\">
                            <div class=\"panel-body\">
                                ";
        // line 74
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form_profile_yesPass"] ?? $this->getContext($context, "form_profile_yesPass")), 'form_start');
        echo "
                                <div class=\"form-group\">
                                    <label>New Password</label>
                                    ";
        // line 77
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form_profile_yesPass"] ?? $this->getContext($context, "form_profile_yesPass")), "password", array()), "first", array()), 'widget', array("attr" => array("class" => "form-control input-sm")));
        echo "
                                </div>
                                <div class=\"form-group\">
                                    <label>Repeat password</label>
                                    ";
        // line 81
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute($this->getAttribute(($context["form_profile_yesPass"] ?? $this->getContext($context, "form_profile_yesPass")), "password", array()), "second", array()), 'widget', array("attr" => array("class" => "form-control input-sm")));
        echo "
                                </div>
                                ";
        // line 83
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($this->getAttribute(($context["form_profile_yesPass"] ?? $this->getContext($context, "form_profile_yesPass")), "submit", array()), 'widget', array("attr" => array("class" => "btn btn-info btn-block")));
        echo "
                                ";
        // line 84
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form_profile_yesPass"] ?? $this->getContext($context, "form_profile_yesPass")), 'form_end');
        echo "
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
    </div>
    </div>

";
        
        $__internal_a08c17e70316a7fb6869fe7984d04d70ce17fd34fadbeea8c29084621552a49d->leave($__internal_a08c17e70316a7fb6869fe7984d04d70ce17fd34fadbeea8c29084621552a49d_prof);

        
        $__internal_668c49cd0f26cd2d708be5b9263373a3a9d86e9940209e43c03fc68673392fbc->leave($__internal_668c49cd0f26cd2d708be5b9263373a3a9d86e9940209e43c03fc68673392fbc_prof);

    }

    public function getTemplateName()
    {
        return "login/editUser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  183 => 84,  179 => 83,  174 => 81,  167 => 77,  161 => 74,  152 => 68,  148 => 67,  141 => 63,  136 => 62,  127 => 56,  118 => 50,  107 => 42,  100 => 38,  93 => 34,  86 => 30,  79 => 26,  73 => 23,  56 => 9,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block user_edit %}
<div class=\"container\" style=\"margin-top: 100px;\">
    <div class=\"row centered-form\">
        <div class=\"col-xs-12 col-sm-6\" >
            <div class=\"panel panel-default\">
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Edit Profile {{user.username}}</h3>
                </div>
                <div id=\"exTab1\">
                    <ul  class=\"nav nav-tabs\">
                        <li class=\"active\">
                            <a  href=\"#1a\" data-toggle=\"tab\">General</a>
                        </li>
                        <li><a href=\"#2a\" data-toggle=\"tab\">Change Password</a>
                        </li>
                    </ul>

                    <div class=\"tab-content clearfix\">
                        <div class=\"tab-pane active\" id=\"1a\">
                            <div class=\"panel-body\">
                                {{ form_start(form_profile_noPass) }}
                                <div class=\"form-group\">
                                    <label>First Name</label>
                                    {{ form_widget(form_profile_noPass.firstname, {'attr': {'class':'form-control input-sm'} }) }}
                                </div>
                                <div class=\"form-group\">
                                    <label>Last Name</label>
                                    {{ form_widget(form_profile_noPass.lastname, {'attr': {'class':'form-control input-sm'} }) }}
                                </div>
                                <div class=\"form-group\">
                                    <label>Email</label>
                                    {{ form_widget(form_profile_noPass.email, {'attr': {'class':'form-control input-sm'} }) }}
                                </div>
                                <div class=\"form-group\">
                                    <label>Phone Number</label>
                                    {{ form_widget(form_profile_noPass.phone, {'attr': {'class':'form-control input-sm'} }) }}
                                </div>
                                <div class=\"form-group\">
                                    <label>User Name</label>
                                    {{ form_widget(form_profile_noPass.username, {'attr': {'class':'form-control input-sm'} }) }}
                                </div>


                                <div class=\"row\">
                                    <div class=\"col-xs-6 col-sm-6 col-md-6\">
                                        <div class=\"form-group\">
                                            <label>Enable</label>
                                            {{ form_widget(form_profile_noPass.isactive, {'attr': {'class':''} }) }}
                                        </div>
                                    </div>
                                    <div class=\"col-xs-6 col-sm-6 col-md-6\">
                                        <div class=\"form-group\">
                                            <label>Roles</label>
                                            {{ form_widget(form_profile_noPass.roles, {'attr': {'class':''} }) }}
                                        </div>
                                    </div>
                                </div>

                                <div class=\"form-group\">
                                    <img class=\"img-circle img-bordered-primary\" src=\"{{user_image_path}}{{ img_link }}\" width=\"120\" />
                                    {{ form_widget(form_profile_noPass.file, {'attr': {'class':''} }) }}
                                </div>


                                {{ form_widget(form_profile_noPass.submit, {'attr': {'class':'btn btn-info btn-block'} }) }}
                                {{ form_end(form_profile_noPass)}}

                            </div>
                        </div>
                        <div class=\"tab-pane\" id=\"2a\">
                            <div class=\"panel-body\">
                                {{ form_start(form_profile_yesPass) }}
                                <div class=\"form-group\">
                                    <label>New Password</label>
                                    {{ form_widget(form_profile_yesPass.password.first, {'attr': {'class':'form-control input-sm'} }) }}
                                </div>
                                <div class=\"form-group\">
                                    <label>Repeat password</label>
                                    {{ form_widget(form_profile_yesPass.password.second, {'attr': {'class':'form-control input-sm'} }) }}
                                </div>
                                {{ form_widget(form_profile_yesPass.submit, {'attr': {'class':'btn btn-info btn-block'} }) }}
                                {{ form_end(form_profile_yesPass)}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
    </div>
    </div>

{% endblock %}
", "login/editUser.html.twig", "/var/www/pat/app/Resources/views/login/editUser.html.twig");
    }
}

<?php

/* @WebProfiler/Profiler/header.html.twig */
class __TwigTemplate_15eddc954afe8b1a5701f36b017299cffefff806815c131a09a5e54aa5d1cbdd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_63f40797ff2f3a0f78198e12df94113c6c214b064bf6a3d6fbac71ca6d5f7575 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_63f40797ff2f3a0f78198e12df94113c6c214b064bf6a3d6fbac71ca6d5f7575->enter($__internal_63f40797ff2f3a0f78198e12df94113c6c214b064bf6a3d6fbac71ca6d5f7575_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/header.html.twig"));

        $__internal_73b9a322d418aa9e0ad30866d41629c160bdfc6228750ba3084adfe0bacd750b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_73b9a322d418aa9e0ad30866d41629c160bdfc6228750ba3084adfe0bacd750b->enter($__internal_73b9a322d418aa9e0ad30866d41629c160bdfc6228750ba3084adfe0bacd750b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/header.html.twig"));

        // line 1
        echo "<div id=\"header\">
    <div class=\"container\">
        <h1>";
        // line 3
        echo twig_include($this->env, $context, "@WebProfiler/Icon/symfony.svg");
        echo " Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
";
        
        $__internal_63f40797ff2f3a0f78198e12df94113c6c214b064bf6a3d6fbac71ca6d5f7575->leave($__internal_63f40797ff2f3a0f78198e12df94113c6c214b064bf6a3d6fbac71ca6d5f7575_prof);

        
        $__internal_73b9a322d418aa9e0ad30866d41629c160bdfc6228750ba3084adfe0bacd750b->leave($__internal_73b9a322d418aa9e0ad30866d41629c160bdfc6228750ba3084adfe0bacd750b_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div id=\"header\">
    <div class=\"container\">
        <h1>{{ include('@WebProfiler/Icon/symfony.svg') }} Symfony <span>Profiler</span></h1>

        <div class=\"search\">
            <form method=\"get\" action=\"https://symfony.com/search\" target=\"_blank\">
                <div class=\"form-row\">
                    <input name=\"q\" id=\"search-id\" type=\"search\" placeholder=\"search on symfony.com\">
                    <button type=\"submit\" class=\"btn\">Search</button>
                </div>
           </form>
        </div>
    </div>
</div>
", "@WebProfiler/Profiler/header.html.twig", "/var/www/pat/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/header.html.twig");
    }
}

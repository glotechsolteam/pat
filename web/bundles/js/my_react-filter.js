var CLASS_ID = 0;
var SEMESTER_ID = 0;

var App = React.createClass({

    getInitialState: function() {

        return {
            text: '',
            AssignSubjects: [],
            classes: [],
            subjects: [],
            semester: []

        }

    },
    render: function () {
        return(
            <div>
                <form onSubmit={this.onSubmit} name="form" method="post" action="/subject/assign?class_id=CLASS_ID&semester_id=SEMESTER_ID" id="form-assign">
                    <ClassesList classes={this.state.classes} onTodoAdd={this.handleTodoAdd} onLoadClass={this.handleClassesAdd} />
                    <SemesterList CLASS_ID={this.state.CLASS_ID} semester={this.state.semester} onTodoAdd={this.handleTodoAdd} onLoadSemester={this.handleSemesterAdd} onUpdateSearch={this.handleUpdateSearch} />
                    <TodoForm onTodoAdd={this.handleTodoAdd} handleClassChange={this.handleTodoAdd} onLoadSubject={this.handleSubjectAdd} subjects={this.state.subjects} onUpdateSearch={this.handleUpdateSearch}                             />
                    <AssignSubjectList AssignSubjects={this.state.AssignSubjects} deleteTodo={this.handleTodoDelete}
                                       CLASS_ID={this.state.CLASS_ID} semester={this.state.semester} onTodoAdd={this.handleTodoAdd} onLoadSemester={this.handleSemesterAdd}/>
                    <input id="form__token" name="form[_token]" value="gGuFR_44cfq91ap_ZwVbtPRKQM05NWqFg0q6MxXQzds" type="hidden"/>
                    <button type="submit" id="form_save" name="form[save]">Assign Subject</button>
                </form>
            </div>
        )
    },
    handleUpdateSearch: function (text) {
       this.setState({search: text});
    },
    handleTodoAdd: function (id, text) {

        var newTdo = {
            id: id,
            name: text
        }

        this.setState({AssignSubjects: this.state.AssignSubjects.concat(newTdo)});


    },
    handleClassesAdd: function (id, text) {
        var newClassesTodo = {
            id: id,
            name: text
        }
        this.setState({classes: this.state.classes.concat(newClassesTodo)});
    },
    handleSemesterAdd: function (id, text) {
        var newSemesterTodo = {
            id: id,
            name: text
        }
        this.setState({semester: this.state.semester.concat(newSemesterTodo)});

    },
    handleSubjectAdd: function (id, text) {
        this.forceUpdate();
        var newSubjectsTodo = {
            id: id,
            name: text
        }
        this.setState({subjects: this.state.subjects.concat(newSubjectsTodo)});
    },
    handleTodoDelete: function (todo) {
        var AssignSubjects = this.state.AssignSubjects;
        for(var i = 0; i < AssignSubjects.length; i++) {
            if(AssignSubjects[i].id == todo.id) {
                AssignSubjects.splice(i, 1);
            }
        }
        this.setState({AssignSubjects: AssignSubjects});
    }
});

var TodoForm = React.createClass({
    getInitialState:function(){
        return {selectValue:''};
    },
    render: function () {
        return(
            <div>
                    <div className="form-group">
                        <select  ref="key" onChange={this.onChange} >
                            <option value="0">Choose Subject</option>
                            {
                                this.props.subjects.map(subjectlist => {
                                    return (
                                        <option subjectlist={subjectlist} key={subjectlist.id} value={subjectlist.id} >{subjectlist.name}</option>
                                    )
                                })
                            }
                        </select>
                        <a className="collection-add btn btn-default" title="Add fruit" onClick={this.onSubmit} ><span className="glyphicon glyphicon-plus-sign"></span></a>
                    </div>
            </div>
        )
    },

    componentDidMount() {
        var id = 0;
        var form = $('#form-assign');
        var url = form.attr('action').replace('CLASS_ID', id);
        var data = form.serialize();
        var props = this.props;
        $.post(url, data, function(result) {
            for(var i=0; i< result.subject.length; i++){
                props.onLoadSubject(result.subject[i].id,result.subject[i].name);
            }
            // load Assign Subject list
            for (var i = 0; i < result.subject_assign.length; i++) {
                props.onTodoAdd(result.subject_assign[i].id, result.subject_assign[i].name);
            }
        });
    },
    onChange: function (e) {
        var index = e.target.selectedIndex;
        this.setState({selectValue:e.target[index].text});

    },

    onSubmit: function(e) {
        e.preventDefault();
        var id = this.refs.key.value;
        var text = this.state.selectValue;
        if(!text){
            alert('Please enter a todo! ');
        }
        else {
            this.props.onTodoAdd(id, text);
        }

    }
});

var AssignSubjectList = React.createClass({
    getInitialState: function() {

        return {
            search: 1
        }
    },
    updateSearch(event) {
        //this.setState({search: event.target.value.substr(0,20)});
        this.setState({search: event.target.value});
    },
        render: function () {

        let filteredAssignSubjects = this.props.AssignSubjects.filter((assSub) => {
                return assSub.id === parseInt(this.state.search);
            }

        );
              //  alert(this.state.search);
        return(

            <table className="table table-striped table-hover" id="assign_subjects">
                <tr>
                    <th>Subjects</th>
                </tr>
                <tr><td><input type="text" value={this.state.search} onCklick={this.updateSearch.bind(this)} />
                    <select onChange={this.updateSearch.bind(this)} ref="id" id="form_semester"
                            name="form[semester]" required="required">
                        <option value="">Choose Semester</option>
                        {
                            this.props.semester.map(semesterlist => {
                                return (
                                    <option semesterlist={semesterlist} key={semesterlist.id}
                                            value={semesterlist.id}>{semesterlist.name}</option>
                                )
                            })
                        }

                    </select>
                </td></tr>
                {
                    this.props.AssignSubjects.map(todo => {
                            return (
                                <tr todo={todo} key={todo.id} ><td>
                                    <input type="text"  value={todo.name} data-id={todo.id} readOnly />
                                    <input type="hidden" value={todo.id} name="form[subject_id][]" />
                                    <a className="collection-remove btn btn-default" title="Delete" href="#" onClick={this.onDelete.bind(this, todo)}>
                                        <span className="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td></tr>)

                        })
                }
            </table>
        )
    },
    //immediately after a component is mounted.
    componentDidMount() {
        var props = this.props;
        var form = $('#form-assign');
        var url = form.attr('action').replace('CLASS_ID',  CLASS_ID);
        var data = form.serialize();
        $.post(url, data, function (result) {
            for (var i = 0; i < result.semester.length; i++) {
                props.onLoadSemester(result.semester[i].id, result.semester[i].name);
            }
        });
    },
    handleSemesterChange: function (e) {
        e.preventDefault();
        var id = this.refs.id.value;
        SEMESTER_ID = id;

        var form = $('#form-assign');
        var url = form.attr('action').replace('CLASS_ID', CLASS_ID);
        url = url.replace('SEMESTER_ID', SEMESTER_ID);
        var data = form.serialize();
        var props = this.props;
    },
    onDelete: function (todo) {
        this.props.deleteTodo(todo);

    }

});

var ClassesList = React.createClass({
    render: function () {
        return(
            <select onChange={this.handleClassChange} ref="id" id="form_class_id" name="form[class_id]" required="required" >
                <option value="">Choose Class</option>
                {
                    this.props.classes.map(classlist => {
                        return (
                            <option classlist={classlist} key={classlist.id} value={classlist.id} >{classlist.name}</option>
                               )
                    })
                }

            </select>

        )
    },
    componentDidMount() {
        var id = 0;
        var form = $('#form-assign');
        var url = form.attr('action').replace('CLASS_ID', id);
        var data = form.serialize();
        var props = this.props;
        $.post(url, data, function(result) {
            for(var i=0; i< result.classes.length; i++){
                props.onLoadClass(result.classes[i].id,result.classes[i].name);
            }
        });
    },
    handleClassChange: function(e) {
        e.preventDefault();
        var id = this.refs.id.value;
        CLASS_ID = id;
        var form = $('#form-assign');
        var url = form.attr('action').replace('CLASS_ID',  CLASS_ID);
        url = url.replace('SEMESTER_ID', SEMESTER_ID);
        var data = form.serialize();
        var props = this.props;
        $.post(url, data, function(result) {
            for(var i=0; i< result.subject_assign.length; i++){
                props.onTodoAdd(result.subject_assign[i].id, result.subject_assign[i].name);
            }
        });


    }
});

var SemesterList = React.createClass({
    render: function () {

        return (
            <select onChange={this.handleSemesterChange.bind(this)} ref="id" id="form_semester"
                    name="form[semester]" required="required">
                <option value="">Choose Semester</option>
                {
                    this.props.semester.map(semesterlist => {
                        return (
                            <option semesterlist={semesterlist} key={semesterlist.id}
                                    value={semesterlist.id}>{semesterlist.name}</option>
                        )
                    })
                }

            </select>

        )
    },

    //immediately after a component is mounted.
    componentDidMount() {
        var props = this.props;
        var form = $('#form-assign');
        var url = form.attr('action').replace('CLASS_ID',  CLASS_ID);
        var data = form.serialize();
        $.post(url, data, function (result) {
            for (var i = 0; i < result.semester.length; i++) {
                props.onLoadSemester(result.semester[i].id, result.semester[i].name);
            }
        });
    },
    handleSemesterChange: function (e) {
        e.preventDefault();
        var id = this.refs.id.value;
        SEMESTER_ID = id;
     
        var form = $('#form-assign');
        var url = form.attr('action').replace('CLASS_ID',  CLASS_ID);
        url = url.replace('SEMESTER_ID', SEMESTER_ID);
        var data = form.serialize();
        var props = this.props;
        /*
        $.post(url, data, function (result) {
            for (var i = 0; i < result.subject_assign.length; i++) {

                //props.onTodoAdd(result.subject_assign[i].id, result.subject_assign[i].name);
            }
        });
        */
       // var index = e.target.selectedIndex;
       // props.onUpdateSearch(e.target[index].text);


    }
});
ReactDOM.render(
    <App/>,
    document.getElementById('app')
);
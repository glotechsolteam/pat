<?php

namespace AppBundle\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Http\Authorization\AccessDeniedHandlerInterface;

class AccessDeniedHandler implements AccessDeniedHandlerInterface {

    public function handle(Request $request, AccessDeniedException $accessDeniedException) {
         $request->getSession()->getFlashBag()->clear();
        $request->getSession()
            ->getFlashBag()
            ->add('fail', 'No right to access to creating a new user');
        
        return new RedirectResponse('/');
    }



}

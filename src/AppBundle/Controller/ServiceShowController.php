<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Service;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays the list of services. Ex) seminar, school system, etc.
 */
class ServiceShowController extends Controller {

    /**
     * @Route("/service/list", name="service_list")
     */
    public function showAction(Request $request) {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $service = $em->getRepository(Service::class)->find($request->query->getInt('id'));
            $res = $this->deleteService($em, $service);
            return new Response(
                    json_encode(array('removed' => $res)), 200, array('Content-Type' => 'application/json')
            );
        }
        $services = $this->getDoctrine()
                ->getRepository(Service::class)
                ->findAllServices();

        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
                $services, $request->query->getInt('page', 1), $request->query->getInt('limit', 10),  
                array('defaultSortFieldName' => 'name', 'defaultSortDirection' => 'asc')
        );

        $deleteFormAjax = $this->createCustomForm('ID', 'DELETE', 'service_list');
        return $this->render('service/list.html.twig', array(
                    'services' => $result,
                    'delete_form_ajax' => $deleteFormAjax->createView(),
        ));
    }

    public function deleteService($em, $service) {
        $em->remove($service);
        $em->flush();
        $removed = 1;
        return $removed;
    }

    public function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl($route, array('id' => $id)))
                        ->setMethod($method)
                        ->getForm();
    }

}

<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LogoutController extends Controller {

    /**
     * @Route("/logout", name="logout")
     */
    public function indexAction(Request $request) {
        return $this->redirect('/');
    }

}

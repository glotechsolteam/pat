<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserType;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Provides a new user registration page. 
 */
class UserRegisterController extends Controller {

    /**
     * @Route("/register", name="user_registration")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder) {

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $em = $this->getDoctrine()->getManager();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $userNameChk = $em->getRepository(User::class)->findBy(array(
                'username' => $form['username']->getData(),
            ));
            $userEmailChk = $em->getRepository(User::class)->findBy(array(
                'email' => $form['email']->getData(),
            ));
            if ($userNameChk) {
                return $this->render(
                                'registration/register.html.twig', array(
                            'form' => $form->createView(),
                            'error' => 'This username is exist',
                                )
                );
            } else if ($userEmailChk) {
                return $this->render(
                                'registration/register.html.twig', array(
                            'form' => $form->createView(),
                            'error' => 'This email is exist',
                                )
                );
            }

            try {
                $user->upload();
                // 3) Encode the password (you could also do this via Doctrine listener)
                $password = $passwordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($password);
                $user->replaceImageFileNameWithUsername();
                // 4) save the User!      
                $em->getConnection()->beginTransaction();

                $em->persist($user);
                $em->flush();
                $em->getConnection()->commit();
            } catch (UniqueConstraintViolationException $e) {
                $em->getConnection()->rollBack();
                throw $e;
                return $this->redirect('/register');
            }

            return $this->redirect('/login');
        }
        //Display the view now
        return $this->render(
                        'registration/register.html.twig', array(
                    'form' => $form->createView(),
                    'error' => $form->getErrors(true),
                        )
        );
    }

}

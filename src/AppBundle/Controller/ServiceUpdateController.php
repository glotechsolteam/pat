<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Service;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/**
 * Edits an existing service.
 */
class ServiceUpdateController extends Controller {

    /**
     * @Route("/service/update", name="service_update")
     */
    public function updateAction(Request $request) {
        $serviceId = $request->query->getInt('id');
        $em = $this->getDoctrine()->getManager();
        $service = $em->getRepository(Service::class)->find($serviceId);
        $service->setName($service->getName());
        $form = $this->createFormBuilder($service)
                ->add('name', TextType::class)
                ->add('description', TextType::class)
                ->add('price', TextType::class)
                ->add('location', TextType::class)
                ->add('startDate', DateType::class)
                ->add('file', FileType::class, array(
                    'required' => false,
                    'label' => 'ServiceImage'
                ))
                ->add('category', EntityType::class, array(
                    'class' => 'AppBundle:Category',
                    'choice_label' => 'name',
                    'required' => true))
                ->add('service_frequency', EntityType::class, array(
                    'class' => 'AppBundle:Service_Frequency',
                    'choice_label' => 'name',
                    'required' => true))
                ->add('save', SubmitType::class, array('label' => 'Update Service'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $service = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $service->upload();
            $em->persist($service);
            $em->flush();

            return $this->redirect('/service/update?id=' . $service->getId());
            // return $this->redirectToRoute('product_update');
        }
        return $this->render('service/detail.html.twig', array('img_link' => $service->getServiceImage(),
                    'form' => $form->createView(),
        ));
    }

}

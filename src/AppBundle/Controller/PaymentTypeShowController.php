<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Payment_Type;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays the list of payment types.  
 */
class PaymentTypeShowController extends Controller {

    /** @Route("/paymenttype/list", name="payment_type_list")
     */
    public function showAction(Request $request) {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $paymentType = $em->getRepository(Payment_Type::class)->find($request->query->getInt('id'));
            $res = $this->deleteService($em, $paymentType);
            return new Response(
                    json_encode(array('removed' => $res)), 200, array('Content-Type' => 'application/json')
            );
        }
        $paymentTypes = $this->getDoctrine()
                ->getRepository(Payment_Type::class)
                ->findAllPaymentTypes();

        /**
         * @var $paginator \Knp\Component\Page\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
                $paymentTypes, $request->query->getInt('page', 1), $request->query->getInt('limit', 10),
                array('defaultSortFieldName' => 'name', 'defaultSortDirection' => 'desc')
        );

        $deleteFormAjax = $this->createCustomForm('ID', 'DELETE', 'payment_type_list');
        return $this->render('payment_type/list.html.twig', array(
                    'paymenttypes' => $result,
                    'delete_form_ajax' => $deleteFormAjax->createView(),
        ));
    }

    public function deleteService($em, $paymentType) {
        $em->remove($paymentType);
        $em->flush();
        $removed = 1;
        return $removed;
    }

    public function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl($route, array('id' => $id)))
                        ->setMethod($method)
                        ->getForm();
    }

}

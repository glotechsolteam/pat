<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Service;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Service_Transaction;
use AppBundle\Entity\Miscellaneous;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

use Doctrine\DBAL\Connection;
use GenerateToken;
use ResponseAPI;


use PdfAttachment;
/**
 * Displays the list of service transactions.
 */
class ServiceTransactionShowController extends Controller {

    /**
     * @Route("/service_transaction/list", name="service_transaction_list")
     */
    public function showAction(Request $request) {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $serviceTransaction = $em->getRepository(Service_Transaction::class)->find($request->query->getInt('id'));
            $res = $this->deleteService($em, $serviceTransaction);
            return new Response(
                    json_encode(array('removed' => $res)), 200, array('Content-Type' => 'application/json')
            );
        }
        $serviceTran = new Service_Transaction();
        $servicesTransactions = null;
        $form = $this->createFormBuilder()
                ->add('dateStart', DateType::class, array(
                    'placeholder' => '', 'required' => false
                ))
                ->add('dateEnd', DateType::class, array(
                    'placeholder' => '', 'required' => false
                ))
                ->add('service', EntityType::class, array(
                    'class' => 'AppBundle:Service',
                    'placeholder' => ' ',
                    'choice_label' => 'name',
                    'required' => false))
                ->add('submit', SubmitType::class, array('label' => 'Submit'))
                ->setMethod('GET')
                ->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $servicesTransactions = $this->getDoctrine()
                    ->getRepository(Service_Transaction::class)
                    ->findServiceTransactionsByDate($form['dateStart']->getData(), 
                            $form['dateEnd']->getData(), $form['service']->getData());
            $serviceTran = $this->getDoctrine()
                    ->getRepository(Miscellaneous::class)
                    ->findServiceTotal($form['dateStart']->getData(), 
                            $form['dateEnd']->getData(), $form['service']->getData());
        } else {
            $servicesTransactions = $this->getDoctrine()
                    ->getRepository(Service_Transaction::class)
                    ->findAllServiceTransactions();
            $serviceTran = $this->getDoctrine()
                    ->getRepository(Miscellaneous::class)
                    ->findAllServiceTotal();
        }
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
                $servicesTransactions, $request->query->getInt('page', 1), 
                $request->query->getInt('limit', 5), 
                array('defaultSortFieldName' => 'paidDate', 'defaultSortDirection' => 'desc')
        );

        $deleteFormAjax = $this->createCustomForm('ID', 'DELETE', 'service_transaction_list');
        return $this->render('service_transaction/list.html.twig', array(
                    'service_transaction' => $result,
                    'total_service_transaction' => $serviceTran,
                    'form' => $form->createView(),
                    'delete_form_ajax' => $deleteFormAjax->createView(),
        ));
    }

    public function deleteService($em, $service) {
        $em->remove($service);
        $em->flush();
        $removed = 1;
        return $removed;
    }

    public function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl($route, array('id' => $id)))
                        ->setMethod($method)
                        ->getForm();
    }

    /**** Sending Mail To Customer ***/


    /**
     * @Route("/send_mail", name="send_mail")
     */
    public function sendMail(Request $request, \Swift_Mailer $mailer) {
        try {
            $em = $this->getDoctrine()->getManager();
            $service = $em->getRepository(Service::class)->findOneBy(array('id' =>$request->query->getInt('service_id')));


            $user = $em->getRepository(User::class)->findOneBy(array(
                'id' => $request->query->getInt('user_id'),
            ));
            $service_truncation = $em->getRepository(Service_Transaction::class)->findOneBy(array(
                'accountNumber' =>$request->query->get('account_number')
                ));
            /**** Call PDF Generate Class ***/
           // $pdf = new PdfAttachment\PdfAttachment();
            $code = $request->query->get('account_number');
            $price = $service->getPrice();
            $paid = $service_truncation->getPaid();
            $address = $service->getLocation();
            $image_link = $service->getServiceImage();
            $date = $service->getStartDate();
            $start_date = $date->format('Y-m-d');
            $serviceName = $service->getName();
            $FullName = $user->getFirstname().' '.$user->getLastname();
            $userName = $user->getUsername();
         //   $pdf->pdfAction($code, $serviceName, $price, $address,$start_date, $image_link, $userName, $FullName );
            /**** End Call PDF Generate Class ***/

            /**** Mail sending code ***/
            $ticket_html = $this->getHTMLTicketInfo($code, $serviceName, $price, $address, $start_date, $FullName, $paid);
            $message = (new \Swift_Message($serviceName))

                ->setFrom('office@glotechsol.com')
                ->setTo([$user->getEmail(), 'zubeyde@glotechsol.com'=> $user->getFirstname()])
                ->setBody(
                    $user->getFirstName().
                    ' Ticket for '.$serviceName

                )
                ->addPart($ticket_html, 'text/html');

              // ->attach(\Swift_Attachment::fromPath($this->get('kernel')->getRootDir().'/../app/files/ticket-'.$code.'('.$userName.').pdf'));

            $mailer->send($message);
        }
        catch (Exception $e){
            $this->addFlash(
                'notice',
                'Your message did not sended!'
            );

        }


        return $this->redirect('/service_transaction/list');
    }
    public function getHTMLTicketInfo($code, $serviceName, $price, $address, $start_date, $FullName, $paid) {
        $booket_date = date("Y-m-d");
        $html ='<div class="container">';

        $textForPriceCondition = 'Стоимость билета на семинар составляет '.$price.' сомов';
        $priceInfo = '<tr><td>Цена:'.$price.' сомов</td></tr>';
        if((int)$paid < 1) {
            $textForPriceCondition = 'Участие на семинаре - платное, но Вы обладаете билетом предоставленным по гранту';
            $priceInfo = '';
        }

        $html.='        
        <h1>'.$serviceName.'</h1>
        <br/><br/>
        <table cellspacing="0" cellpadding="2" border="0" width="650">        
            <tr>
                <td>Номер вашего билета <strong>:'.$code.' </strong></td>
                 <td rowspan="8"> <img src="https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl='.$code.'&choe=UTF-8"></td>
            </tr>        
            <tr><td>Телефон для справок: <strong>+996 312 881 982 </strong></td></tr>
            <tr><td>Адрес проведения семинара: '.$address.'</td></tr>
            <tr><td>Дата начала: '.$start_date.'</td></tr>
            '.$priceInfo.'
            <tr><td>Имя клиента: '.$FullName.'</td></tr>
            <tr><td>Дата регистрации: '.$booket_date.'</td></tr> 
            
        </table>
       
        <br/><strong>Условия билета</strong><br/>
		<ul>
		    <li>Билет предоставляет доступ к участию в '.$serviceName.'</li>     
		    <li>'.$textForPriceCondition.'</li>
		    <li>Лица без билета на семинар допускаться не будут </li>
		    <li>При утренней ежедневной регистрации помимо билета обязательно наличие удостоверения личности.</li>
		</ul>
        <strong>Желаем Вам плодотворного участия на семинаре!</strong><br/>
     
        <div style="width:100%; font-size:9px; padding:15px 5px 5px 5px">
		<strong>Служба поддержки клиентов</strong><br/>
		Тел: +996 312 881 982 , E-mail: office@glotechsol.com <br/>
		Мы всегда рады Вам помочь в будние дни с 9:00 до 17:00. Пятница, Суббота,Воскресенье - выходной. <br/>
		<i>Кыргызская Республика, г. Бишкек 720040, Токтогула 110</i>
		</div>
		<style type="text/css">
            ul {
                
                background-color: #b8d1f3;
                padding:10px; 
                font-size:12px; 
                font-weight:normal;		
            }
            
        </style>';
        return $html;
    }

}

<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Service;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class ServiceCreateController extends Controller {

    /**
     * @Route("/service/save", name="service_save")
     */
    public function createAction(Request $request) {
        $result_message = ['Service Success Saved', ''];
        $service = new Service();

        $form = $this->createFormBuilder($service)
                ->add('name', TextType::class)
                ->add('description', TextType::class)
                ->add('price', TextType::class)
                ->add('location', TextType::class)
                ->add('startDate', DateType::class)
                ->add('file', FileType::class, array(
                    'required' => false,
                    'label' => 'ServiceImage'
                ))
                ->add('category', EntityType::class, array(
                    'class' => 'AppBundle:Category',
                    'choice_label' => 'name',
                    'required' => true))
                ->add('service_frequency', EntityType::class, array(
                    'class' => 'AppBundle:Service_Frequency',
                    'choice_label' => 'name',
                    'required' => true))
                ->add('save', SubmitType::class, array('label' => 'Create Service'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $service = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $service->upload();
            $em->persist($service);
            $em->flush();

            return $this->redirectToRoute('service_save');
        }
        return $this->render('service/save.html.twig', array(
                    'form' => $form->createView(),
                    'result' => $result_message[1],
        ));
    }

}

<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Service_Frequency;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays the list of service frequency.
 */
class ServiceFrequencyShowController extends Controller {

    /**
     * @Route("/service_frequency/list", name="service_frequency_list")
     */
    public function showAction(Request $request) {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $serviceFrequency = $em->getRepository(Service_Frequency::class)->find($request->query->getInt('id'));
            $res = $this->deleteService($em, $serviceFrequency);
            return new Response(
                    json_encode(array('removed' => $res)), 200, array('Content-Type' => 'application/json')
            );
        }
        $servicefrequencys = $this->getDoctrine()
                ->getRepository(Service_Frequency::class)
                ->findAllServiceFrequencys();

        /**
         * @var $paginator \Knp\Component\Page\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
                $servicefrequencys, $request->query->getInt('page', 1), $request->query->getInt('limit', 10),
                array('defaultSortFieldName' => 'name', 'defaultSortDirection' => 'asc')
        );

        $deleteFormAjax = $this->createCustomForm('ID', 'DELETE', 'service_frequency_list');
        return $this->render('service_frequency/list.html.twig', array(
                    'servicefrequencys' => $result,
                    'delete_form_ajax' => $deleteFormAjax->createView(),
        ));
    }

    public function deleteService($em, $serviceFrequency) {
        $em->remove($serviceFrequency);
        $em->flush();
        $removed = 1;
        return $removed;
    }

    public function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl($route, array('id' => $id)))
                        ->setMethod($method)
                        ->getForm();
    }

}

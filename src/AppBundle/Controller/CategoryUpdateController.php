<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/**
 * Edits an existing category. 
 */
class CategoryUpdateController extends Controller {

    /**
     * @Route("/category/update", name="category_update")
     */
    public function updateAction(Request $request) {
        $categoryID = $request->query->getInt('id');
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository(Category::class)->find($categoryID);
        $category->setName($category->getName());
        $form = $this->createFormBuilder($category)
                ->add('name', TextType::class)
                ->add('description', TextType::class)
                ->add('file', FileType::class, array(
                    'required' => false,
                    'label' => 'CategoryImage'
                ))
                ->add('save', SubmitType::class, array('label' => 'Update Category'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $category->upload();
            $em->persist($category);
            $em->flush();

            return $this->redirect('/category/update?id=' . $category->getId());
        }
        return $this->render('category/detail.html.twig', array('img_link' => $category->getCategoryImage(),
                    'form' => $form->createView(),
        ));
    }

}

<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Service_Transaction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Edits an existing service transaction. 
 */
class ServiceTransactionUpdateController extends Controller {

    /**
     * @Route("/service_transaction/update", name="service_transaction_update")
     */
    public function updateAction(Request $request) {
        $serviceId = $request->query->getInt('id');
        $em = $this->getDoctrine()->getManager();
        $serviceTransaction = $em->getRepository(Service_Transaction::class)->find($serviceId);
        $form = $this->createFormBuilder($serviceTransaction)
                ->add('account_number', TextType::class)
                ->add('paid', TextType::class)
                ->add('status', ChoiceType::class, array(
                'choices' => array("true" => '1', "false" => '0'),
            ))
                ->add('paid_date', DateType::class)
                ->add('payment_type', EntityType::class, array(
                    'class' => 'AppBundle:Payment_Type',
                    'choice_label' => 'name',
                    'required' => true))
                ->add('service', EntityType::class, array(
                    'class' => 'AppBundle:Service',
                    'choice_label' => 'name',
                    'required' => true))
                ->add('user', EntityType::class, array(
                    'class' => 'AppBundle:User',
                    'choice_label' => 'username',
                    'required' => true))
                ->add('save', SubmitType::class, array('label' => 'Update Transaction'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $serviceTransaction = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($serviceTransaction);
            $em->flush();

            return $this->redirect('/service_transaction/update?id=' . $serviceTransaction->getId());
        }
        return $this->render('service_transaction/detail.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}

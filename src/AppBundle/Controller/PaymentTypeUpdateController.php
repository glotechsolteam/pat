<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Payment_Type;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Process;

/**
 * Edits payment type info.
 */
class PaymentTypeUpdateController extends Controller {

    /**
     * @Route("/paymenttype/update", name="payment_type_update")
     */
    public function updateAction(Request $request) {
        $paymentTypeId = $request->query->getInt('id');
        $em = $this->getDoctrine()->getManager();
        $paymentType = $em->getRepository(Payment_Type::class)->find($paymentTypeId);
    //    $paymentT = new Payment_Type();
        $paymentType->setName($paymentType->getName());
    //    $paymentT->setPublicKeyTwo($paymentType->getPublicKeyTwo());
        $form = $this->createFormBuilder($paymentType)
                ->add('name', TextType::class)
                ->add('public_key', TextType::class)
                ->add('public_key_two', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Update Service'))
                ->getForm();

        $form->handleRequest($request);
      /*  $processPay = new Process();
        $publicKey = '';
        $secretKey = '';
        $processPay->setIntial($publicKey, $secretKey);
        $processPay->publicKey;
       */
        if ($form->isSubmitted() && $form->isValid()) {
            $paymentType = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($paymentType);
            $em->flush();
            /*$token = $_POST['stripeToken'];
            $email = $_POST['stripeEmail'];
            $processPay->charge($token, $email, $paymentT->getPublicKeyTwo());
             */
            return $this->redirect('/paymenttype/update?id=' . $paymentType->getId());
        }
      /*  return $this->render('payment_type/detail.html.twig', array(
                    'form' => $form->createView(), 'payment_type' => $paymentT, 'process' => $processPay,
        ));*/
        return $this->render('payment_type/detail.html.twig', array(
                    'form' => $form->createView(), 
        ));
    }

}

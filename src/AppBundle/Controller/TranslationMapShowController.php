<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Translation_Map;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays the list of translation_map.  
 */
class TranslationMapShowController extends Controller {

    /** @Route("/translation_map/list", name="translation_map_list")
     */
    public function showAction(Request $request) {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $translationMap = $em->getRepository(Translation_Map::class)->find($request->query->getInt('id'));
            $res = $this->deleteService($em, $translationMap);
            return new Response(
                    json_encode(array('removed' => $res)), 200, array('Content-Type' => 'application/json')
            );
        }
        $translationMap = $this->getDoctrine()
                ->getRepository(Translation_Map::class)
                ->findAllTranslationMap();

        /**
         * @var $paginator \Knp\Component\Page\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
                $translationMap, $request->query->getInt('page', 1), $request->query->getInt('limit', 10),
                array('defaultSortFieldName' => 'entity', 'defaultSortDirection' => 'desc')
        );

        $deleteFormAjax = $this->createCustomForm('ID', 'DELETE', 'translation_map_list');
        return $this->render('translation_map/list.html.twig', array(
                    'translationmap' => $result,
                    'delete_form_ajax' => $deleteFormAjax->createView(),
        ));
    }

    public function deleteService($em, $translationMap) {
        $em->remove($translationMap);
        $em->flush();
        $removed = 1;
        return $removed;
    }

    public function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl($route, array('id' => $id)))
                        ->setMethod($method)
                        ->getForm();
    }

}

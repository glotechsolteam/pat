<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Payment_Type;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Creates a new payment type reference data.
 */
class PaymentTypeCreateController extends Controller {

    /** @Route("/paymenttype/save", name="payment_type_save")
     */
    public function createAction(Request $request) {
        $result_message = ['Payment Type Success Saved', ''];
        $paymentType = new Payment_Type();

        $form = $this->createFormBuilder($paymentType)
                ->add('name', TextType::class)
                ->add('public_key', TextType::class)
                ->add('public_key_two', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Create Payment Type'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $paymentType = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($paymentType);
            $em->flush();

            return $this->redirectToRoute('payment_type_save');
        }
        return $this->render('payment_type/save.html.twig', array(
                    'form' => $form->createView(),
                    'result' => $result_message[1],
        ));
    }

}

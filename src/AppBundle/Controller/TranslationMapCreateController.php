<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Translation_Map;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Creates a new translation_map reference data.
 */
class TranslationMapCreateController extends Controller {

    /** @Route("/translation_map/save", name="translation_map_save")
     */
    public function createAction(Request $request) {
        $result_message = ['Translation Map Success Saved', ''];
        $translationMap = new Translation_Map();

        $form = $this->createFormBuilder($translationMap)
                ->add('entity', TextType::class)
                ->add('entity_id', TextType::class)
                ->add('field_name', TextType::class)
                ->add('en', TextType::class)
                ->add('ru', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Create Translation Map'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $translationMap = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($translationMap);
            $em->flush();

            return $this->redirectToRoute('translation_map_save');
        }
        return $this->render('translation_map/save.html.twig', array(
                    'form' => $form->createView(),
                    'result' => $result_message[1],
        ));
    }

}

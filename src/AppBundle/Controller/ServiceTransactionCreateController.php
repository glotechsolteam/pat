<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Service_Transaction;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Creates a service transaction, which is the record of a user paid for a service.
 */
class ServiceTransactionCreateController extends Controller {

    /**
     * @Route("/service_transaction/save", name="service_transaction_save")
     */
    public function createAction(Request $request) {
        $result_message = ['Service Transaction Success Saved', ''];
        $serviceTransaction = new Service_Transaction();

        $form = $this->createFormBuilder($serviceTransaction)
                ->add('account_number', TextType::class)
                ->add('paid', TextType::class)
                ->add('status', ChoiceType::class, array(
                'choices' => array("true" => '1', "false" => '0'),
            ))
                ->add('paid_date', DateType::class)
                ->add('payment_type', EntityType::class, array(
                    'class' => 'AppBundle:Payment_Type',
                    'choice_label' =>
                    function($choice) {
                        return $choice->getName() . ':' . $choice->getPublicKey() . ':' . $choice->getPublicKeyTwo();
                    },
                    'required' => true))
                ->add('service', EntityType::class, array(
                    'class' => 'AppBundle:Service',
                    'choice_label' =>
                    function($choice) {
                        return $choice->getName() . ':' . $choice->getServiceFrequency()->getName() . ':$' . $choice->getPrice();
                    },
                    'required' => true))
                ->add('user', EntityType::class, array(
                    'class' => 'AppBundle:User',
                    'choice_label' => 'username',
                    'required' => true))
                ->add('save', SubmitType::class, array('label' => 'Create Transaction'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $serviceTransaction = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($serviceTransaction);
            $em->flush();

            return $this->redirectToRoute('service_save');
        }
        return $this->render('service_transaction/save.html.twig', array(
                    'form' => $form->createView(),
                    'result' => $result_message[1],
        ));
    }

}

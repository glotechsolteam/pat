<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/**
 * Creates service frequency. Ex) one time, or monthly.
 */
class CategoryCreateController extends Controller {

    /**
     * @Route("/category/save", name="category_save")
     */
    public function createAction(Request $request) {
        $result_message = ['Category Success Saved', ''];
        $category = new Category();

        $form = $this->createFormBuilder($category)
                ->add('name', TextType::class)
                ->add('description', TextType::class)
                ->add('file', FileType::class, array(
                    'required' => false,
                    'label' => 'CategoryImage'
                ))
                ->add('save', SubmitType::class, array('label' => 'Create Category'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('category_save');
        }
        return $this->render('category/save.html.twig', array(
                    'form' => $form->createView(),
                    'result' => $result_message[1],
        ));
    }

}

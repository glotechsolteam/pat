<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Service_Frequency;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

/**
 * Creates service frequency. Ex) one time, or monthly.
 */
class ServiceFrequencyCreateController extends Controller {

    /**
     * @Route("/service_frequency/save", name="service_frequency_save")
     */
    public function createAction(Request $request) {
        $result_message = ['Payment Type Success Saved', ''];
        $serviceFrequency = new Service_Frequency();

        $form = $this->createFormBuilder($serviceFrequency)
                ->add('name', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Create Service Frequency'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $serviceFrequency = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($serviceFrequency);
            $em->flush();

            return $this->redirectToRoute('service_frequency_save');
        }
        return $this->render('service_frequency/save.html.twig', array(
                    'form' => $form->createView(),
                    'result' => $result_message[1],
        ));
    }

}

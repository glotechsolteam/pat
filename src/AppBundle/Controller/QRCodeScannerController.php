<?php
/**
 * Created by PhpStorm.
 * User: kufa
 * Date: 15.05.18
 * Time: 16:22
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Service_Transaction;
use AppBundle\Entity\Payment_Type;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Process;


class QRCodeScannerController extends Controller
{

    /**
     * @Route("/search", name="search")
     */
    public function searchAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('search'))
            ->getForm();
        $FirstName = 'No Found';
        $message = 'No matching account number!';
        $scanCount = 0;
        $accountnumber = 0;
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $accountnumber = $request->request->get('account_number');
            $serviceTransaction = $this->getDoctrine()
                ->getRepository(Service_Transaction::class)
                ->findOneBy(array(
                    'accountNumber' => $accountnumber
                ));

            if (is_null($serviceTransaction)) {
                $message = "No matching account number!";
            } else {
                $scanCount = $serviceTransaction->getScanCount();
                if ($scanCount >= 1) {
                    $message = "Already scanned!";
                    $scanCount = $scanCount + 1;
                    $serviceTransaction->setScanCount($scanCount);
                } else {
                    $message = "Scanned successfully!";
                    $scanCount = 1;
                    $serviceTransaction->setScanCount($scanCount);
                }
                $em = $this->getDoctrine()->getManager();
                $em->persist($serviceTransaction);
                $em->flush();
                // $FirstName = $serviceTransaction->getUser()->getFirsname();
                $FirstName = $serviceTransaction->getUser()->getFirstname();
            }

        }
        return $this->render('payment_type/scan.html.twig', [
            'form' => $form->createView(),
            'firstname' => $FirstName,
            'account_number' => $accountnumber,
            'message' => $message,
            'scan_count' => $scanCount

        ]);
    }


}
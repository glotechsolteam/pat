<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Service_Frequency;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Edits an existing service frequency. 
 */
class ServiceFrequencyUpdateController extends Controller {

    /**
     * @Route("/service_frequency/update", name="service_frequency_update")
     */
    public function updateAction(Request $request) {
        $serviceFrequencyID = $request->query->getInt('id');
        $em = $this->getDoctrine()->getManager();
        $serviceFrequency = $em->getRepository(Service_Frequency::class)->find($serviceFrequencyID);
        $serviceFrequency->setName($serviceFrequency->getName());
        $form = $this->createFormBuilder($serviceFrequency)
                ->add('name', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Update Service Frequency'))
                ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $serviceFrequency = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($serviceFrequency);
            $em->flush();

            return $this->redirect('/service_frequency/update?id=' . $serviceFrequency->getId());
        }
        return $this->render('service_frequency/detail.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

}

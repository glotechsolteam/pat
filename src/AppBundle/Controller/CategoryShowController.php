<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Category;
use Symfony\Component\HttpFoundation\Request;

/**
 * Displays the list of category.
 */
class CategoryShowController extends Controller {

    /**
     * @Route("/category/list", name="category_list")
     */
    public function showAction(Request $request) {
        if ($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            $serviceFrequency = $em->getRepository(Category::class)->find($request->query->getInt('id'));
            $res = $this->deleteService($em, $serviceFrequency);
            return new Response(
                    json_encode(array('removed' => $res)), 200, array('Content-Type' => 'application/json')
            );
        }
        $categorys = $this->getDoctrine()
                ->getRepository(Category::class)
                ->findAllCategorys();

        /**
         * @var $paginator \Knp\Component\Page\Paginator
         */
        $paginator = $this->get('knp_paginator');
        $result = $paginator->paginate(
                $categorys, $request->query->getInt('page', 1), $request->query->getInt('limit', 10),
                 array('defaultSortFieldName' => 'name', 'defaultSortDirection' => 'asc')
        );

        $deleteFormAjax = $this->createCustomForm('ID', 'DELETE', 'category_list');
        return $this->render('category/list.html.twig', array(
                    'categorys' => $result,
                    'delete_form_ajax' => $deleteFormAjax->createView(),
        ));
    }

    public function deleteService($em, $categorys) {
        $em->remove($categorys);
        $em->flush();
        $removed = 1;
        return $removed;
    }

    public function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl($route, array('id' => $id)))
                        ->setMethod($method)
                        ->getForm();
    }

}

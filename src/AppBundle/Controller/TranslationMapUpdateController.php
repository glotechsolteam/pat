<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Translation_Map;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Process;

/**
 * Edits Translation Map info.
 */
class TranslationMapUpdateController extends Controller {

    /**
     * @Route("/translation_map/update", name="translation_map_update")
     */
    public function updateAction(Request $request) {
        $translationMapId = $request->query->getInt('id');
        $em = $this->getDoctrine()->getManager();
        $translationMap = $em->getRepository(Translation_Map::class)->find($translationMapId);
    //    $paymentT = new Payment_Type();
        $translationMap->setEntity($translationMap->getEntity());
    //    $paymentT->setPublicKeyTwo($paymentType->getPublicKeyTwo());
        $form = $this->createFormBuilder($translationMap)
                ->add('entity', TextType::class)
                ->add('entity_id', TextType::class)
                ->add('field_name', TextType::class)
                ->add('en', TextType::class)
                ->add('ru', TextType::class)
                ->add('save', SubmitType::class, array('label' => 'Update Translation Map'))
                ->getForm();

        $form->handleRequest($request);
      /*  $processPay = new Process();
        $publicKey = '';
        $secretKey = '';
        $processPay->setIntial($publicKey, $secretKey);
        $processPay->publicKey;
       */
        if ($form->isSubmitted() && $form->isValid()) {
            $translationMap = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($translationMap);
            $em->flush();
            /*$token = $_POST['stripeToken'];
            $email = $_POST['stripeEmail'];
            $processPay->charge($token, $email, $paymentT->getPublicKeyTwo());
             */
            return $this->redirect('/translation_map/update?id=' . $translationMap->getId());
        }
      /*  return $this->render('payment_type/detail.html.twig', array(
                    'form' => $form->createView(), 'payment_type' => $paymentT, 'process' => $processPay,
        ));*/
        return $this->render('translation_map/detail.html.twig', array(
                    'form' => $form->createView(), 
        ));
    }

}

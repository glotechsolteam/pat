<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use AppBundle\Entity\Service_Transaction;

/**
 * Displays the list of services. Ex) seminar, school system, etc.
 */
class SearchController extends Controller {

    /**
     * @Route("/search/{accountnumber}")
     */
    public function accountnumberAction($accountnumber) {

        $serviceTransaction = $this->getDoctrine()
            ->getRepository(Service_Transaction::class)
            ->getServiceTransactionByAccountNumber($accountnumber);

        $message = null;
        $scanCount = 0;
        if (is_null($serviceTransaction)) {
            $message = "No matching account number!";
        } else {
            $scanCount = $serviceTransaction->getScanCount();
            if ($scanCount >= 1) {
                $message = "Already scanned!";
                $scanCount = $scanCount+1;
                $serviceTransaction->setScanCount($scanCount);
            } else {
                $message = "Scanned successfully!";
                $scanCount = 1;
                $serviceTransaction->setScanCount($scanCount);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($serviceTransaction);
            $em->flush();
        }
        return $this->render(
            'search/result.html.twig',
            array('account_number' => $accountnumber,
                'message' => $message, 'scan_count' => $scanCount)
        );
    }

}

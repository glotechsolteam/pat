<?php

namespace AppBundle\Repository;

/**
 * Miscellaneous
 *
 */
class MiscellaneousRepository extends \Doctrine\ORM\EntityRepository {

    
    public function findServiceTotal($dateStart, $dateEnd, $service) {
        $searchterm = '%';

        $em = $this->getEntityManager();
        $query = "";
        if (is_null($dateStart) && !is_null($dateEnd) && is_null($service)) {
            $query = $em->createQuery("SELECT sum(st.paid) as totalAmount  
            FROM AppBundle:Service_Transaction st 
        INNER JOIN st.service s 
        INNER JOIN st.paymentType pt
        INNER JOIN st.user u
        WHERE st.id like :id 
        and st.paidDate <= :dateLastDay  ")
                    ->setParameter('id', $searchterm)
                    ->setParameter('dateLastDay', $dateEnd->format('Y-m-d H:i'));
        } else if (!is_null($dateStart) && is_null($dateEnd) && is_null($service)) {
            $query = $em->createQuery("SELECT sum(st.paid) as totalAmount  
            FROM AppBundle:Service_Transaction st 
        INNER JOIN st.service s 
        INNER JOIN st.paymentType pt
        INNER JOIN st.user u
        WHERE st.id like :id 
        and st.paidDate >= :dateFirstDay  ")
                    ->setParameter('id', $searchterm)
                    ->setParameter('dateFirstDay', $dateStart->format('Y-m-d H:i'));
        } else if (is_null($dateStart) && !is_null($dateEnd) && !is_null($service)) {
            $query = $em->createQuery("SELECT sum(st.paid) as totalAmount  
            FROM AppBundle:Service_Transaction st 
        INNER JOIN st.service s 
        INNER JOIN st.paymentType pt
        INNER JOIN st.user u
        WHERE st.id like :id 
        and s.name like :sname 
        and st.paidDate <= :dateLastDay  ")
                    ->setParameter('id', $searchterm)
                    ->setParameter('sname', $service->getName())
                    ->setParameter('dateLastDay', $dateEnd->format('Y-m-d H:i'));
        } else if (!is_null($dateStart) && is_null($dateEnd) && !is_null($service)) {
            $query = $em->createQuery("SELECT sum(st.paid) as totalAmount   
            FROM AppBundle:Service_Transaction st 
        INNER JOIN st.service s 
        INNER JOIN st.paymentType pt
        INNER JOIN st.user u
        WHERE st.id like :id 
        and s.name like :sname 
        and st.paidDate >= :dateFirstDay  ")
                    ->setParameter('id', $searchterm)
                    ->setParameter('sname', $service->getName())
                    ->setParameter('dateFirstDay', $dateStart->format('Y-m-d H:i'));
        } else if (is_null($dateStart) && is_null($dateEnd) && !is_null($service)) {
            $query = $em->createQuery("SELECT sum(st.paid) as totalAmount   
            FROM AppBundle:Service_Transaction st 
        INNER JOIN st.service s 
        INNER JOIN st.paymentType pt
        INNER JOIN st.user u
        WHERE st.id like :id 
        and s.name like :sname  ")
                    ->setParameter('id', $searchterm)
                    ->setParameter('sname', $service->getName());
        } else if (is_null($dateStart) && is_null($dateEnd) && is_null($service)) {
            $query = $em->createQuery("SELECT sum(st.paid) as totalAmount  
            FROM AppBundle:Service_Transaction st 
        INNER JOIN st.service s 
        INNER JOIN st.paymentType pt
        INNER JOIN st.user u
        WHERE st.id like :id")
                    ->setParameter('id', $searchterm);
        } else if (!is_null($dateStart) && !is_null($dateEnd) && is_null($service)) {
            $query = $em->createQuery("SELECT sum(st.paid) as totalAmount  
            FROM AppBundle:Service_Transaction st 
        INNER JOIN st.service s 
        INNER JOIN st.paymentType pt
        INNER JOIN st.user u
        WHERE st.id like :id 
        and st.paidDate >= :dateFirstDay and st.paidDate <= :dateLastDay")
                    ->setParameter('id', $searchterm)
                    ->setParameter('dateFirstDay', $dateStart->format('Y-m-d H:i'))
                    ->setParameter('dateLastDay', $dateEnd->format('Y-m-d H:i'));
        } else {
            $query = $em->createQuery("SELECT sum(st.paid) as totalAmount  
            FROM AppBundle:Service_Transaction st 
        INNER JOIN st.service s 
        INNER JOIN st.paymentType pt
        INNER JOIN st.user u
        WHERE st.id like :id 
        and s.name like :sname 
        and st.paidDate >= :dateFirstDay and st.paidDate <= :dateLastDay  ")
                    ->setParameter('id', $searchterm)
                    ->setParameter('sname', $service->getName())
                    ->setParameter('dateFirstDay', $dateStart->format('Y-m-d H:i'))
                    ->setParameter('dateLastDay', $dateEnd->format('Y-m-d H:i'));
        }

        try {
            $result = $query->getSingleResult();
            return $result;
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
    
    
    public function findAllServiceTotal() {
       
        $searchterm = '%';

        $em = $this->getEntityManager();

        $queryTotal = $em->createQuery("SELECT  sum(st.paid) as totalAmount  
            FROM AppBundle:Service_Transaction st 
        INNER JOIN st.service s 
        INNER JOIN st.paymentType pt
        INNER JOIN st.user u
        WHERE st.id like :id ")
                ->setParameter('id', $searchterm);

        try {
            $resultTotal = $queryTotal->getSingleResult();
            return $resultTotal;
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        } 
    }
    /**
     * Checks if the service transaction exists for the given parameters.
     * 
     * @param type $accountNumb
     * @param type $paymentType
     * @param type $serviceName
     * @return service transactions
     */
    public function checkTransactionExist($accountNumb, $paymentType, $serviceName) {


        $em = $this->getEntityManager();

        $query = $em->createQuery("SELECT st, s, pt, u  
            FROM AppBundle:Service_Transaction st 
        INNER JOIN st.service s 
        INNER JOIN st.paymentType pt
        INNER JOIN st.user u
        WHERE 
        st.accountNumber like :account_number
        and s.name like :service_name
        and pt.name like :payment_type
       "
                )
                ->setParameter('account_number', $accountNumb)
                ->setParameter('payment_type', $paymentType)
                ->setParameter('service_name', $serviceName);
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}

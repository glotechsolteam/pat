<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Miscellaneous
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MiscellaneousRepository")
 *
 */
class Miscellaneous {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    private $totalAmount;

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    public function setTotalAmount($totalAmount) {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    public function getTotalAmount() {
        return $this->totalAmount;
    }

}

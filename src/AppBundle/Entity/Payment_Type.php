<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Payment_Type
 *
 * @ORM\Table(name="payment__type")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Payment_TypeRepository")
 */
class Payment_Type {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="public_key_one", type="string", length=255)
     */
    private $publicKey;

    /**
     * @var string
     *
     * @ORM\Column(name="public_key_two", type="string", length=255)
     */
    private $publicKeyTwo;

    /**
     * @ORM\OneToMany(targetEntity="Service_Transaction", mappedBy="paymentTypes")
     */
    protected $paymentTypes;

    public function __construct() {
        $this->paymentTypes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Payment_Type
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set publicKey
     *
     * @param string $publicKey
     *
     * @return Payment_Type
     */
    public function setPublicKey($publicKey) {
        $this->publicKey = $publicKey;

        return $this;
    }

    /**
     * Get publicKey
     *
     * @return string
     */
    public function getPublicKey() {
        return $this->publicKey;
    }

    /**
     * Set publicKey
     *
     * @param string $publicKey
     *
     * @return Payment_Type
     */
    public function setPublicKeyTwo($publicKey) {
        $this->publicKeyTwo = $publicKey;

        return $this;
    }

    /**
     * Get publicKey
     *
     * @return string
     */
    public function getPublicKeyTwo() {
        return $this->publicKeyTwo;
    }

}

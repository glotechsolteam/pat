<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service_Transaction
 *
 * @ORM\Table(name="service__transaction")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Service_TransactionRepository")
 *
 * @ORM\Table(name="service__transaction", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="payment_type_id_services_id_account_number", columns={"payment_type_id", "services_id","account_number"})
 * })
 */
class Service_Transaction
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="account_number", type="string", length=255)
     */
    private $accountNumber;

    /**
     * @var bool
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status;

    /**
     * @var float
     *
     * @ORM\Column(name="paid", type="float")
     */
    private $paid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="paid_date", type="date", nullable=true)
     */
    private $paidDate;

    /**
     * @var int
     *
     * @ORM\Column(name="scan_count", type="integer", nullable=true,  options={"default" : 0})
     */
    private $scanCount;

    /**
     * @ORM\ManyToOne(targetEntity="Payment_Type", inversedBy="paymentTypes")
     * @ORM\JoinColumn(name="payment_type_id", referencedColumnName="id")
     */
    private $paymentType;

    /**
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="serviceTransactions")
     * @ORM\JoinColumn(name="services_id", referencedColumnName="id")
     */
    private $service;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="users")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;
    
    private $totalAmount;

     /**
     * Set paid
     *
     * @param float $paid
     *
     * @return Service_Transaction
     */
    public function setTotalAmount($totalAmount)
    {
        $this->totalAmount = $totalAmount;

        return $this;
    }

    /**
     * Get paid
     *
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }

    /**
     * Set scanCount
     *
     * @param integer $scanCount
     *
     * @return Service_Transaction
     */
    public function setScanCount($scanCount) {
        $this->scanCount = $scanCount;

        return $this;
    }

    /**
     * Get scanCount
     *
     * @return Integer
     */
    public function getScanCount()
    {
        return $this->scanCount;
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountNumber
     *
     * @param string $accountNumber
     *
     * @return Service_Transaction
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set paid
     *
     * @param float $paid
     *
     * @return Service_Transaction
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;

        return $this;
    }

    /**
     * Get paid
     *
     * @return float
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Service_Transaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return bool
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * Set paidDate
     *
     * @param \DateTime $paidDate
     *
     * @return Service_Transaction
     */
    public function setPaidDate($paidDate)
    {
        $this->paidDate = $paidDate;

        return $this;
    }

    /**
     * Get paidDate
     *
     * @return \DateTime
     */
    public function getPaidDate()
    {
        return $this->paidDate;
    }

    /**
     * Set a service
     *
     * @param object $service
     *
     * @return Service
     */
    public function setService($service)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get a service
     *
     * @return object
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * Set user
     *
     * @param object $user
     *
     * @return User
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return object
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set paymentType
     *
     * @param object $paymentType
     *
     * @return paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * Get paymentType
     *
     * @return object
     */
    public function getPaymentType()
    {
        return $this->paymentType;
    }
}
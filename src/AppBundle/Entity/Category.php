<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category
 *
 * @ORM\Table(name="category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;
 
    
    /**
     * @ORM\OneToMany(targetEntity="Service", mappedBy="services")
     */
    protected $services;

    public function __construct() {
        $this->services = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Category
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
     /**
     * Set description
     *
     * @param string $description
     *
     * @return Category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    /**
     * @var string
     *
     * @ORM\Column(name="category_image", type="string", length=255, nullable= true)
     */
    public $category_image;
    private $file;

    public function getFile() {
        return $this->file;
    }

    public function setFile($file) {
        return $this->file = $file;
    }

    /**
     * Set category_image
     *
     * @param string $category_image
     *
     * @return Service
     */
    public function setCategoryImage($category_image) {
        $this->category_image = $category_image;
        return $this;
    }

    /**
     * Get category_image
     *
     * @param string $category_image
     *
     * @return Category
     */
    public function getCategoryImage() {
        return $this->category_image;
    }

    public function getUploadDir() {
        return 'upload/images';
    }

    public function getAbsolutRoot() {
        return $this->getUploadRoot();
    }

    public function getWebPath() {
        return $this->getUploadDir() . '/' . $this->category_image;
    }

    public function getUploadRoot() {
        return __DIR__ . '/../../../web/' . $this->getUploadDir() . '/';
    }

    public function upload() {
        if ($this->file === null) {
            return;
        }

        $this->category_image = $this->file->getClientOriginalName();
        if (!is_dir($this->getUploadRoot())) {

        }

        $this->file->move($this->getUploadRoot(), $this->category_image);
        unset($this->file);
    }
}


<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Service
 * 
 * @ORM\Table(name="services", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="service_frequency_id_name", columns={"service_frequency_id", "name"})
 * })
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServiceRepository")
 */
class Service {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, unique=false)
     */
    private $location;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, unique=false, nullable=true)
     */
    private $description;
    
    /**
     * @var datetime
     *
     * @ORM\Column(name="startDate", type="datetime")
     */
    private $startDate;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity="Service_Frequency", inversedBy="services")
     * @ORM\JoinColumn(name="service_frequency_id", referencedColumnName="id")
     */
    private $serviceFrequency;
    
     /**
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="services")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="Service_Transaction", mappedBy="serviceTransactions")
     */
    protected $serviceTransactions;

    public function __construct() {
        $this->serviceTransactions = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Service
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Service
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return Service
     */
    public function setLocation($location) {
        $this->location = $location;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getLocation() {
        return $this->location;
    }
    
    /**
     * Set startDate
     *
     * @param datetime $startDate
     *
     * @return Service
     */
    public function setStartDate($startDate) {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return datetime
     */
    public function getStartDate() {
        return $this->startDate;
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Service
     */
    public function setPrice($price) {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return int
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * Set serviceFrequency
     *
     * @param object $serviceFrequency
     *
     * @return Service
     */
    public function setServiceFrequency($serviceFrequency) {
        $this->serviceFrequency = $serviceFrequency;

        return $this;
    }

    /**
     * Get serviceFrequency
     *
     * @return object
     */
    public function getServiceFrequency() {
        return $this->serviceFrequency;
    }

    /**
     * Set category
     *
     * @param object $category
     *
     * @return Service
     */
    public function setCategory($category) {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return object
     */
    public function getCategory() {
        return $this->category;
    }
    
    /**
     * @var string
     *
     * @ORM\Column(name="service_image", type="string", length=255, nullable= true)
     */
    public $service_image;
    private $file;

    public function getFile() {
        return $this->file;
    }

    public function setFile($file) {
        return $this->file = $file;
    }

    /**
     * Set service_image
     *
     * @param string $service_image
     *
     * @return Service
     */
    public function setServiceImage($service_image) {
        $this->service_image = $service_image;
        return $this;
    }

    /**
     * Get service_image
     *
     * @param string $service_image
     *
     * @return Service
     */
    public function getServiceImage() {
        return $this->service_image;
    }

    public function getUploadDir() {
        return 'upload/images';
    }

    public function getAbsolutRoot() {
        return $this->getUploadRoot();
    }

    public function getWebPath() {
        return $this->getUploadDir() . '/' . $this->service_image;
    }

    public function getUploadRoot() {
        return __DIR__ . '/../../../web/' . $this->getUploadDir() . '/';
    }

    public function upload() {
        if ($this->file === null) {
            return;
        }

        $this->service_image = $this->file->getClientOriginalName();
        if (!is_dir($this->getUploadRoot())) {

            //mkdir($this->getUploadRoot(), '0777', true);
        }

        $this->file->move($this->getUploadRoot(), $this->service_image);
        unset($this->file);
    }

}

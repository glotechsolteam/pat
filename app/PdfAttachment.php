<?php

namespace PdfAttachment;

class PdfAttachment
{
    public function pdfAction($code, $serviceName, $price, $address, $start_date, $image_link, $userName, $FullName)
    {
        $booket_date = date("Y-m-d");
        $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', true);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        // set default header data
       // $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

        // add a page
        $pdf->AddPage();

        // set style for barcode
        $style = array(
            'border' => 1,
            'vpadding' => 'auto',
            'hpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255)
            'module_width' => 1, // width of a single module in points
            'module_height' => 1 // height of a single module in points
        );

        $html='<style type="text/css">
			.container {
				
				background-color: #b8d1f3;
				padding:10px; 
				font-size:12px; 
				font-weight:normal;		
			}
			
		</style>';
        $html.='<div class="container">';

        // QRCODE,M : QR-CODE Medium error correction
        $pdf->SetFont('dejavusans', 'N', 14);
        $NameTitle = '<br/><div><strong>'.$serviceName.'</strong></div>';
        $pdf->writeHTML($NameTitle, true, false, true, false, '');
        //$pdf->Text(20, 52	, '');
        $textForPriceCondition = 'Стоимость билета на семинар составляет '.$price.' сомов';
        $priceInfo = '<tr><td>Цена:'.$price.' сомов</td></tr>';
        if((int)$price < 1) {
            $textForPriceCondition = 'Участие на семинаре - платное, но Вы обладаете билетом предоставленным по гранту';
            $priceInfo = '';
        }

        $pdf->SetFont('dejavusans', 'N', 10);
        $tbl = <<<EOD
        <br/><br/>
        <table cellspacing="0" cellpadding="2" border="0" width="650">
            <tr><td>Номер вашего билета <strong>:$code </strong></td></tr>        
            <tr><td>Телефон для справок: <strong>+996 312 881 982 </strong></td></tr>
            <tr><td>Адрес проведения семинара: $address</td></tr>
            <tr><td>Дата начала: $start_date</td></tr>
            $priceInfo
            <tr><td>Имя клиента: $FullName</td></tr>
            <tr><td>Дата регистрации: $booket_date</td></tr> 
        </table>
EOD;

        $pdf->writeHTML($tbl, true, false, false, false, '');

        $pdf->write2DBarcode($code, 'QRCODE,M', 150, 30, 50, 50, $style, 'N');

        // PDF417 (ISO/IEC 15438:2006)
        // new style
        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'cellfitalign' => '',
            'border' => true,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
        $pdf->write1DBarcode($code, 'S25', 10,75,0, 18, 0.4, $style, 'N');
        // Условие билета
        $pdf->SetFont('dejavusans', 'N', 14);
        $html.='<br/><strong>Условия билета</strong><br/>';

        $html.= '
		<ul>
		    <li>Билет предоставляет доступ к участию в '.$serviceName.'</li>     
		    <li>'.$textForPriceCondition.'</li>
		    <li>Лица без билета на семинар допускаться не будут </li>
		    <li>При утренней ежедневной регистрации помимо билета обязательно наличие удостоверения личности.</li>
		</ul>';

        $pdf->SetFont('dejavusans', 'N', 13);
        $html.='</div>';
        $html.='<strong>Желаем Вам плодотворного участия на семинаре!</strong><br/>';
        $pdf->SetFont('dejavusans', 'N', 12);
        $pdf->writeHTML($html, true, false, true, false, '');

        // Footer
        $footer ='<div style="width:100%; font-size:9px; padding:15px 5px 5px 5px">
		<strong>Служба поддержки клиентов</strong><br/>
		Тел: +996 312 881 982 , E-mail: office@glotechsol.com <br/>
		Мы всегда рады Вам помочь в будние дни с 9:00 до 17:00. Пятница, Суббота,Воскресенье - выходной. <br/>
		<i>Кыргызская Республика, г. Бишкек 720040, Токтогула 110</i>
		</div>';
        $pdf->writeHTML($footer, true, false, true, false, '');



        // Close and output PDF document

        // Auto save pdf file to folder
        $ticketName = "ticket-".$code."(".$userName.").pdf";

        return $pdf->Output(__DIR__.'/files/'.$ticketName, 'F');



    }

}
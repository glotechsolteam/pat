<?php

class Process {
    public $publicKey;
    public function setIntial($publicKey, $secretKey) {
        $stripe = array(
            "secret_key" => $secretKey,
            "publishable_key" => $publicKey
        );

        \Stripe\Stripe::setApiKey($stripe['secret_key']);
    }

    public function charge($token, $email, $price) {
        $customer = \Stripe\Customer::create(array(
                    'email' => $email,
                    'source' => $token
        ));

        $charge = \Stripe\Charge::create(array(
                    'customer' => $customer->id,
                    'amount' => $price,
                    'currency' => 'usd'
        ));
    }

}
